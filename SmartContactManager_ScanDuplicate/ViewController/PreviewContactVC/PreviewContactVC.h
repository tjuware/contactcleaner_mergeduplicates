//
//  PreviewContactVC.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 05/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterVC.h"
@interface PreviewContactVC : MasterVC
@property (nonatomic, strong) NSString * filename;

@end
