//
//  PreviewContactVC.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 05/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "PreviewContactVC.h"
#import "HeaderView.h"
#import "TOPasscodeViewController.h"
@interface PreviewContactVC ()<HeaderViewDelegate, TOPasscodeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource,HttpHelperDelegate>
{
    NSArray *arr_contact;
    NSString *password;
}
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (nonatomic, assign) TOPasscodeViewStyle style;
@property (nonatomic, assign) TOPasscodeType type;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) TOPasscodeViewController *passcodeViewController;
@end

@implementation PreviewContactVC
-(void)setBackApiResponse:(NSMutableDictionary *)response api_number:(int)api_number
{
    if([[response objectForKey:@"success"]boolValue])
    {
        if (api_number == savePasswordAPI)
        {
           [self dismissViewControllerAnimated:YES completion:nil];
            [self.view makeToast:@"Save Passowrd Successfully.Enter Once Again "];
            NSDictionary *dict = [[response objectForKey:@"user_details"] firstObject];
            NSLog(@"user_details Dict In PreviewContactVC %@",dict);
            password= [dict objectForKey:@"password"];
            [self updatePlistDictioryByKey:@"password" andValue:password];
            [self performSelector:@selector(showButtonTapped) withObject:nil afterDelay:2.5];
        }
    }
}
- (void)responseError:(NSError *)error api_number:(int)api_number{
    [self showErrorWithStatus:@"Something Went Wrong, Check Internet"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    [self.view_header setDelegate:self];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"back_arrow"]];
    [self.view_header setTitle:@"Preview Contact"];
    [self performSelector:@selector(showButtonTapped) withObject:nil afterDelay:0.4];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    if([[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] boolValue]){
        NSDictionary *namevaluePairs = [[PlistHandler sharedObject]getPlistData];
        email = [namevaluePairs objectForKey:@"registered_email"];
        password = [namevaluePairs objectForKey:@"password"];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    arr_contact = [self downloadVCFandDisplay:self.filename];
    NSLog(@"arr_contcat %@", arr_contact);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)showButtonTapped
{
    _passcodeViewController = [[TOPasscodeViewController alloc] initWithStyle:self.style passcodeType:self.type];
    _passcodeViewController.delegate = self;
    [self presentViewController:_passcodeViewController animated:YES completion:nil];
}

- (void)didTapCancelInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self performSelector:@selector(btnLeftClicked) withObject:nil afterDelay:0.2];
}

-(void)passcodeViewControllerwithNoReturn:(TOPasscodeViewController *)passcodeViewController isCorrectCode:(NSString *)code WithCompletionHandler:(nonnull void (^)(NSMutableDictionary * _Nonnull, BOOL))completionHandler
{
    if(password.length == 0)
    {
    NSDictionary *valuepairs = [[NSDictionary alloc]initWithObjectsAndKeys:email, @"registered_email", code, @"password", nil];
        httpHelper=[[HttpHelper alloc]initWithMUrl:savePasswordURL Dictionary:valuepairs isloading:YES api_number:savePasswordAPI message:@"Sending..."];
        httpHelper.delegate = self;
        
    }
    else
    {
    
    [self updateCheckPasswordAPI:code WithCompletionHandler:^(NSMutableDictionary *dict, BOOL completed) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(dict, completed);
        });
    }];
        
}
}

-(void)btnLeftClicked
{
    [app.navController popViewControllerAnimated:YES];
}

#pragma mark - tableviewDelegate
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==1)
    {
        return arr_contact.count;
    }
    return 1;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = @"cellId";
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    cell.textLabel.textColor=[UIColor colorWithRed:7.0/255 green:56.0/255 blue:86.0/255 alpha:1];
    switch (indexPath.section) {
        case 0:
        {
            if(arr_contact.count>0){
                cell.textLabel.text = [NSString stringWithFormat:@"Add All %d contacts",(int)arr_contact.count];
                cell.textLabel.textAlignment = NSTextAlignmentCenter;
            }
        }
            break;
        case 1:
        {
            NSString *string = [arr_contact objectAtIndex:indexPath.row];//+1 because firt row has \n
            NSString *name = [self stringBetweenString:@"FN:" andString:@"\n" targetString:string];
            name = [self handleNullValueOfString:name];
            cell.textLabel.text = name;
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
            break;
        default:
            break;
    }
    return cell;
}

-(NSString*)stringBetweenString:(NSString*)start andString:(NSString*)end targetString:(NSString*) targetString {
    NSRange startRange = [targetString rangeOfString:start];
    if (startRange.location != NSNotFound) {
        NSRange targetRange;
        targetRange.location = startRange.location + startRange.length;
        targetRange.length = [targetString length] - targetRange.location;
        NSRange endRange = [targetString rangeOfString:end options:0 range:targetRange];
        if (endRange.location != NSNotFound) {
            targetRange.length = endRange.location - targetRange.location;
            return [targetString substringWithRange:targetRange];
        }
    }
    return nil;
}

-(NSString*)stringByTrimmingLeadingWhitespace :(NSString *) str {
    NSInteger i = 0;
    while ((i < [str length])
           && [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[str characterAtIndex:i]]) {
        i++;
    }
    return [str substringFromIndex:i];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        [self addcontact];
    } else {
        NSLog(@"clicked row number %li",(long)indexPath.row
              );
        NSString *vCardString = [arr_contact objectAtIndex:indexPath.row+1];//+1 becoz first row has ,
        vCardString = [NSString stringWithFormat:@"%@\nEND:VCARD",vCardString];
        [self addSingleContact:indexPath.row];
    }
}

-(void)addcontact
{
dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        float total = self->arr_contact.count;
        
        for(int i =1; i < self->arr_contact.count; i++){
            [self addSingleContact:i];
            float per = i/total;
            NSString *msg =[NSString stringWithFormat:@"Contact saved: %i%% ",(int)(per*100)];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showProgress:per status:msg];
                [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, 120)];
                [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
                
                if(i == self->arr_contact.count-1){
                    [SVProgressHUD dismiss];
                }
            });
        }
    });
}

-(void)addSingleContact:(NSInteger)index
{
    NSString * _vCardString = [self->arr_contact objectAtIndex:index];
    _vCardString = [NSString stringWithFormat:@"%@\n\nEND:VCARD",_vCardString];
    [[ContactAction sharedContacts]addSingleVCFContact:_vCardString];
}
@end
