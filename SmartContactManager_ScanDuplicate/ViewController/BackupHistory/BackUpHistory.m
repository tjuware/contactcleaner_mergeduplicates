//
//  BackUpHistory.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 30/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "BackUpHistory.h"
#import "HeaderView.h"
//#import "ContactCell.h"
#import "BackUpCell.h"
#import "iCarousel.h"
@interface BackUpHistory ()<HttpHelperDelegate, HeaderViewDelegate, UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate, iCarouselDelegate, iCarouselDataSource>
{
    NSMutableArray *array_history;
    BOOL isHint;
    NSIndexPath *selectedIndexPath;
}

@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UILabel *lbl_warning;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_tableViewHeight;
@property (strong, nonatomic) IBOutlet iCarousel *icarousal;
@end

@implementation BackUpHistory
-(void)setBackApiResponse:(NSMutableDictionary *)response api_number:(int)api_number
{
    if([[response objectForKey:@"success"] boolValue]){
        if(api_number == getBackupAPI)
        {
            NSLog(@"response %@", response);
            array_history = [[response objectForKey:@"file_backup"] mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->_tableview reloadData];
                [self warningTakeBackup];
                self.cnst_tableViewHeight.constant = (self->array_history.count > 0) ? array_history.count * 55 : 80;
                [self.icarousal reloadData];
            });
        }else if (api_number == deleteBackupAPI)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showSuccessWithStatus:@"Deleted Successfully"];
            });
            
            [array_history removeObjectAtIndex:selectedIndexPath.row];
            [[PlistHandler sharedObject] setBackupCount: array_history.count];
            if(array_history.count >0){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableview deleteRowsAtIndexPaths:@[self->selectedIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                });
            }else{
                [self warningTakeBackup];
            }
        }
        else if (api_number == sendEmailAPI)
        {
            NSString *message;
            NSString *email_id=[response objectForKey:@"email_id"];
            if(email_id != nil || ![email_id isEqualToString:@""])
            {
                message  = [NSString stringWithFormat:@"Email has been sent to %@",email_id];
            }
            else
            {
                message  = [NSString stringWithFormat:@"Login first with email ID"];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showSuccessWithStatus:message];
            });
            
        }
    }
}
- (void)responseError:(NSError *)error api_number:(int)api_number{
    [self showErrorWithStatus:@"Something Went Wrong, Check Internet"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
  
//    [_view_header changeBackgroundColor:Bg_color];
    [self.view_header setTitle:@"Backup History"];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"icon_menu"]];
    _view_header.delegate = self;
//    [self.tableview registerNib:[UINib nibWithNibName:@"ContactCell" bundle:nil] forCellReuseIdentifier:CONTACTCELL_ID];
    [self.tableview registerNib:[UINib nibWithNibName:@"BackUpCell" bundle:nil] forCellReuseIdentifier:@"BackUpCellID"];

    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    isHint = YES;
    if([[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] boolValue]){
        NSDictionary *namevaluePairs = [[NSDictionary alloc]initWithObjectsAndKeys:[[PlistHandler sharedObject]getValueFromPlistByKey:@"user_id"],@"user_id", nil];
        httpHelper = [[HttpHelper alloc]initWithMUrl:getBackupURL Dictionary:namevaluePairs isloading:YES api_number:getBackupAPI message:@"Loading all backup"];
        httpHelper.delegate = self;
    }else{
        [self.tableview setHidden:YES];
        [_lbl_warning setText:@"Please login to check backup history"];
        [self.cnst_tableViewHeight setConstant:80.0];
        [self.icarousal reloadData];
    }
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    self.icarousal.delegate = self;
    self.icarousal.dataSource = self;
    self.interstitial = [self createAndLoadInterstitiala];
    self.interstitial.delegate = self;
//    [self performSelector:@selector(doSomethingInterstitial) withObject:nil afterDelay:4.0];

//    [self createAndLoadInterstitial:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.icarousal.type = iCarouselTypeCoverFlow2;
    //  _carousel.type=iCarouselTypeCylinder;
    [self.icarousal reloadData];
    [self.icarousal scrollToItemAtIndex:0 animated:true];
}

-(void)viewDidAppear:(BOOL)animated{
    self.icarousal.backgroundColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.475];
}
#pragma mark - HeaderViewDelegate
-(void)btnLeftClicked
{
    [self slideView];
}

#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array_history.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BackUpCell *cell = (BackUpCell *)[tableView dequeueReusableCellWithIdentifier:@"BackUpCellID"];
    if(!cell)
        cell = [[BackUpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BackUpCellID"];
    NSDictionary *dict = [array_history objectAtIndex:indexPath.row];
    NSString *createdDate = [dict objectForKey:@"created_date"];
    NSArray *arrSecond = [createdDate componentsSeparatedByString:@" "];
    NSString *strHow = [arrSecond objectAtIndex:0];
    arrSecond = [strHow componentsSeparatedByString:@"-"];
    NSLog(@"arr %@",arrSecond);
    int monthNumber = [[arrSecond objectAtIndex:1]intValue];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSString *monthName = [[df monthSymbols] objectAtIndex:(monthNumber-1)];
    [cell.lbl_month setText:[NSString stringWithFormat:@"%@ %@",monthName,[arrSecond firstObject]]];
    [cell.lbl_date setText:[arrSecond lastObject]];
//    [cell.lbl_fileName setText:@"Smart Contact Manager - Scan Duplicate"];//name];
    [cell.lbl_fileName setText:@"Smart Contact Manager"];//name];
    
    cell.backgroundColor =  [UIColor clearColor];
    //    [cell.lbl_name setTextColor:[UIColor whiteColor]];
    cell.rightUtilityButtons = [self deleteButtons];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BackUpCell *cell = (BackUpCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell showRightUtilityButtonsAnimated:YES];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BackUpCell *cell = (BackUpCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell hideUtilityButtonsAnimated:YES];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0 && isHint){
        [cell performSelector:@selector(animateCell:) withObject:cell afterDelay:1.0];
        isHint = NO;
    }
}
#pragma mark - SWTableViewCellDelegate
-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [_tableview indexPathForCell:cell];
    selectedIndexPath = indexPath;
    NSDictionary *dict = [array_history objectAtIndex:indexPath.row];

    switch (index) {
        case 0:
        {
            NSDictionary *nameValuePairs = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[dict objectForKey:@"user_id"]], @"user_id",[NSString stringWithFormat:@"%@",[dict objectForKey:@"cb_id"]], @"cb_id", nil];
            httpHelper = [[HttpHelper alloc]initWithMUrl:sendEmailURL Dictionary:nameValuePairs isloading:YES api_number:sendEmailAPI message:@"Sending backup"];
            httpHelper.delegate = self;
        }
            break;
        case 1:
        {
            NSString *filename = [dict objectForKey:@"file_name"];
            [self redirectToPreviewContactVC:filename];
        }
            break;
        case 2:
        {
            NSDictionary *nameValuePairs = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[dict objectForKey:@"cb_id"]],@"cb_id", nil];
            
            httpHelper = [[HttpHelper alloc]initWithMUrl:deleteBackupURL Dictionary:nameValuePairs isloading:YES api_number:deleteBackupAPI message:@"Deleting backup"];
            httpHelper.delegate = self;
        }
            break;
        default:
            break;
    }
}
-(BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}

- (NSArray *)deleteButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor: [UIColor orangeColor] title:@"Resend" Icon:[UIImage imageNamed: @"icon_resend1"]];
    [rightUtilityButtons sw_addUtilityButtonWithColor: [UIColor brownColor] title:@"Download" Icon:[UIImage imageNamed: @"icon_download"]];
    [rightUtilityButtons sw_addUtilityButtonWithColor: [UIColor redColor] title:@"Delete" Icon:[UIImage imageNamed: @"icon_delete"]];

    return rightUtilityButtons;
}

-(void)warningTakeBackup
{
        [self.lbl_warning setText:(array_history.count <= 0) ? @"Please take backup" : @""];
        [self.tableview setHidden:(array_history.count <= 0) ? YES : NO];
}
-(NSMutableArray *)carouselArray
{
    NSMutableArray *items = [NSMutableArray array];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Pedometer+ Walk Weight Loss",@"name", pedoAppURL, @"url", @"PedometerTechApp.png", @"image", nil];
    [items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Notes",@"name", notesAppURL, @"url", @"NotesTechApp.png", @"image", nil];
    [items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Maths Learner",@"name", mathsAppURL, @"url", @"MathsLearnerTechApp.png", @"image", nil];
    [items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Housie Number Picker",@"name", housieAppURL, @"url", @"HousieTechApp.png", @"image", nil];
    [items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Water Tracker",@"name", waterAppURL, @"url", @"WaterTrackerTechApp.png", @"image", nil];
    [items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Ball Hit-Smash",@"name", ballsHitAppURL, @"url", @"BallsHitTechApp.png", @"image", nil];
    [items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Bollywood Movie Quiz", @"name",  @"BMQTechApp.png", @"image", bmqAppURL, @"url",nil];
    [items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Bollywood Songs Quiz", @"name",  @"BSQTechApp.png", @"image", bsqAppURL, @"url",nil];
    [items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Bollywood Celebrities", @"name",  @"HBCelebritiesTechApp.png", @"image", hbCelebrityAppURL, @"url",nil];
    [items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Box Office Quiz", @"name",  @"BoxOfficeTechApp.png", @"image", boxOfficeAppURL, @"url",nil];
    [items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Smart Contact Manager", @"name",  @"SCMTechApp.png", @"image", scmAppURL, @"url",nil];
    [items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Contact Backup + Easy", @"name",  @"scmBackupTechApp.png", @"image", scmBackUpAppURL, @"url",nil];
    [items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Contact Cleaner-Merge & Email", @"name",  @"ScanDuplicateTechApp.png", @"image", scanDuplicateAppURL, @"url",nil];
    [items addObject:dict];
    
    
//    NSMutableArray *items = [NSMutableArray array];
//    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
//    dict=[[NSMutableDictionary alloc]init];
//    [dict setObject:@"Ball Hit-Smash" forKey:@"name"];
//    [dict setObject:@"itms://itunes.apple.com/us/app/apple-store/id1083462882" forKey:@"url"];
//    [dict setObject:@"ball_hit" forKey:@"image"];
//    [items addObject:dict];
//
//
//    dict=[[NSMutableDictionary alloc]init];
//    [dict setObject:@" Balls Hit - Fire" forKey:@"name"];
//    [dict setObject:@"itms://itunes.apple.com/us/app/apple-store/id1375475141" forKey:@"url"];
//    [dict setObject:@"balls_hit_fire" forKey:@"image"];
//    [items addObject:dict];
//
//    dict=[[NSMutableDictionary alloc]init];
//    [dict setObject:@"Bollywood Songs Quiz" forKey:@"name"];
//    [dict setObject:@"Bollywood_Songs_Quiz" forKey:@"image"];
//    [dict setObject:@"itms://itunes.apple.com/us/app/apple-store/id695445842" forKey:@"url"];
//    [items addObject:dict];
//
//    dict=[[NSMutableDictionary alloc]init];
//    [dict setObject:@"Bollywood Celebrities" forKey:@"name"];
//    [dict setObject:@"HBollywood_Celebrities" forKey:@"image"];
//    [dict setObject:@"itms://itunes.apple.com/us/app/apple-store/id680460746" forKey:@"url"];
//    [items addObject:dict];
//
//    dict=[[NSMutableDictionary alloc]init];
//    [dict setObject:@"Bollywood Movies Quiz" forKey:@"name"];
//    [dict setObject:@"BollywoodMoviesQuiz" forKey:@"image"];
//    [dict setObject:@"itms://itunes.apple.com/us/app/apple-store/id708263591" forKey:@"url"];
//    [items addObject:dict];
//
//    dict=[[NSMutableDictionary alloc]init];
//    [dict setObject:@"Birthday & Contact Backup" forKey:@"name"];
//    [dict setObject:@"Birthday_Contact" forKey:@"image"];
//    [dict setObject:@"itms://itunes.apple.com/us/app/apple-store/id827642824" forKey:@"url"];
//    [items addObject:dict];
//
//    dict=[[NSMutableDictionary alloc]init];
//    [dict setObject:@"HBollywood" forKey:@"name"];
//    [dict setObject:@"itms://itunes.apple.com/us/app/apple-store/id673082678" forKey:@"url"];
//    [dict setObject:@"HBollywood" forKey:@"image"];
//    [items addObject:dict];
//
//    dict=[[NSMutableDictionary alloc]init];
//    [dict setObject:@"Hollywood Bollywood Quizup" forKey:@"name"];
//    [dict setObject:@"itms://itunes.apple.com/us/app/apple-store/id1121590226" forKey:@"url"];
//    [dict setObject:@"box_office_quizup" forKey:@"image"];
//    [items addObject:dict];
    return items;
}
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [[self carouselArray] count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    NSMutableArray *items = [self carouselArray];
    UILabel *label = nil;
    UIImageView *imageView=nil;
    //create new view if no view is available for recycling
    if (view == nil)
    {
        
        view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, FRAMEWIDTH-50, _icarousal.frame.size.height-50)];
        //  [view setBackgroundColor:[UIColor colorWithRed:46.0/255 green:25.0/255 blue:164.0/255 alpha:0.1]];
        view.layer.cornerRadius=20;
        view.layer.masksToBounds=true;
        view.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.80];// colorWithRed:90.0/255.0 green:99.0/255.0 blue:97.0/255.0 alpha:0.5];
        //        [view setBackgroundColor:[UIColor lightTextColor]];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, carousel.frame.size.height*0.4, carousel.frame.size.height*0.4)];
        imageView.tag = 2;
        imageView.center=CGPointMake(view.center.x, view.center.y-40);
        imageView.layer.cornerRadius=10;
        imageView.layer.masksToBounds=true;
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, imageView.frame.origin.y+imageView.frame.size.height+10, 200, 50)];
        label.numberOfLines=0;
        label.center=CGPointMake(imageView.center.x, label.center.y);
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:20];
        label.textColor=[UIColor darkGrayColor];
        label.tag = 1;
        [view addSubview:label];
        UIButton *btnInstall=[[UIButton alloc]initWithFrame:CGRectMake(0, 40, carousel.frame.size.width-50, 45)];
        [btnInstall setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnInstall setBackgroundColor:[UIColor colorWithRed:76/255.0f green:180/255.0f blue:64/255.0f alpha:0.8]];
        [btnInstall setTitle:@"Install Now" forState:UIControlStateNormal];
        btnInstall.userInteractionEnabled=false;
        [btnInstall setCenter:CGPointMake(view.center.x, view.frame.size.height-40)];
        [view addSubview:imageView];
        [view addSubview:btnInstall];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
        imageView = (UIImageView *)[view viewWithTag:2];
    }
    
    
    imageView.image = [UIImage imageNamed:[items[index] objectForKey:@"image"]];
    label.text = [items[index] objectForKey:@"name"];
    
    return view;
}
-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSMutableArray *items = [self carouselArray];
    NSString *url=[items[index]objectForKey:@"url"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:^(BOOL success) {
        
    }];
    //  NSLog(@"URL %@",[);
}
#pragma mark - Intertisial Delegate Method
-(void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
//    self.interstitial = [self createAndLoadInterstitiala];
}
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    UIViewController *rootViewController = UIApplication.sharedApplication.keyWindow.rootViewController;
    if ([rootViewController.childViewControllers.lastObject isKindOfClass:[BackUpHistory class]]) {
        [self doSomethingInterstitial];
    }
    
}
@end
