//
//  TutorialVC.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 31/10/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TutorialVC.h"

@interface TutorialVC ()<HeaderViewDelegate>
{
    NSInteger index, previousIndex;
    NSMutableArray *array_GreetingsCards;
    CGFloat previos;
}
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet UIImageView *img_poster;

@end

@implementation TutorialVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view_header setTitleColor:[UIColor darkGrayColor]];
    [self.view_header hideViewLeft];
    [self.view_header changeBackgroundColor:[UIColor whiteColor]];
    [self.view_header changeRightTitle:@"Skip" withColor:[UIColor darkGrayColor]];
    self.view_header.delegate = self;
    [[NSUserDefaults standardUserDefaults] setValue:@"seen" forKey:@"isDemoSeen"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    NSDictionary *dict_wholeContact = [[FileManipulation sharedContacts] readWholePhoneContactDictionaryFromJsonFile:SCMCONTACT];
    [[ContactList sharedContacts] fetchAllContacts:NO WithCompletionHandler:^(int result, NSMutableArray *arr) {
        if(result == 2 && dict_wholeContact.count <= 0){
            [[FileManipulation sharedContacts]writeWholePhoneContactToJsonFile:arr andFileName:SCMCONTACT];
        }
    }];
    
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
}
-(void)btnRigtClicked
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier: @"ViewController"];
   
//    [app.navController presentViewController:viewController animated:YES completion:^{
        self->app.navController = nil;
        self->app.navController=[[UINavigationController alloc]initWithRootViewController:viewController];
        self->app.window.rootViewController = self->app.navController;
//    }];
//        [app.navController pushViewController:viewController animated:YES];
}
@end
