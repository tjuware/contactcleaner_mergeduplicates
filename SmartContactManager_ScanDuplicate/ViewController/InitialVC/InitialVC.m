//
//  InitialVC.m
//  SmartContactManager_ScanDuplicate
//
//  Created by Kush Thakkar on 25/01/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

#import "InitialVC.h"
#import <Contacts/Contacts.h>
#import "TutorialVC.h"
#import "AppDelegate.h"
@interface InitialVC ()

@end

@implementation InitialVC

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated{
    
}
- (void)viewDidAppear:(BOOL)animated{
    _imgViewAppIcon.layer.cornerRadius = _imgViewAppIcon.frame.size.width*0.1;
    _imgViewAppIcon.clipsToBounds = YES;
}
@end
