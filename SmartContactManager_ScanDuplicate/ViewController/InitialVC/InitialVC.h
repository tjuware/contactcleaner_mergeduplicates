//
//  InitialVC.h
//  SmartContactManager_ScanDuplicate
//
//  Created by Kush Thakkar on 25/01/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InitialVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgViewAppIcon;

@end

NS_ASSUME_NONNULL_END
