//
//  DuplicateContact.m
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 13/11/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "DuplicateContact.h"
#import "view_duplicateCell.h"
#import "DuplicateCell.h"
#import "MergePopUp.h"
#import "MergeAlert.h"
@interface DuplicateContact ()<UITableViewDelegate, UITableViewDataSource, HeaderViewDelegate, SWTableViewCellDelegate, LoginPopUpDelegate, MergePopUpDelegate, MergeAlertDelegate, HttpHelperDelegate>
{
    CGFloat nameHeight;
    NSArray *arr_color;
//    LoginPopUpView *popup;
    MergePopUp *mergePop;
    MergeAlert *mergeAlert;

    NSInteger count;
    BOOL ismerge, isHint, shouldRewarded;
    
}
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet UITableView *tbl_duplicate;
@property (strong, nonatomic) IBOutlet UIButton *btn_mergeAll;

@end

@implementation DuplicateContact
- (void)setBackApiResponse:(NSMutableDictionary *)response api_number:(int)api_number{
    [super setBackApiResponse:response api_number:api_number];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isHint = YES;
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"logo_back"]];
//    [self.view_header changeRightIconImage:[UIImage imageNamed:@"icon_refresh"]];
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    [self.view_header setDelegate:self];
    [self.view_header setTitle:@"Home"];
    self.tbl_duplicate.delegate = self;
    self.tbl_duplicate.dataSource = self;
    [self.tbl_duplicate registerNib:[UINib nibWithNibName:@"DuplicateCell" bundle:nil] forCellReuseIdentifier:@"CellID"];
    [[viewModification sharedViews]setCornerRadius:21 andView:_btn_mergeAll];
    
    arr_color = [[NSArray alloc]initWithObjects:[UIColor blackColor], UIColor.blueColor, UIColor.brownColor, UIColor.cyanColor, UIColor.greenColor, UIColor.magentaColor, UIColor.orangeColor,UIColor.purpleColor, UIColor.redColor, UIColor.yellowColor,UIColor.grayColor, nil];
    
    if(self.barItemIndex == 1)
    {
        NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"dup_name" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
        NSLog(@"array %@", [self.arr_duplicate firstObject]);
        self.arr_duplicate = [[self.arr_duplicate sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self setBackgroundGradient:self.btn_mergeAll color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];
    count= [[[PlistHandler sharedObject]getValueFromPlistByKey:@"backup_count"] integerValue];

}
-(void)ismergeDone
{
     ismerge = YES;
}
-(void)addSearchKeyInArray
{
    for(int i =0; i< self.arr_duplicate.count; i++)
    {
        NSMutableDictionary *dict = [[self.arr_duplicate objectAtIndex:i] mutableCopy];
        [dict setObject:@"0" forKey:@"isSelected"];
        [self.arr_duplicate replaceObjectAtIndex:i withObject:dict];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - tableview delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(SWTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0 && isHint)
    {
        [cell performSelector:@selector(animateCell:) withObject:cell afterDelay:1.0];
        isHint = NO;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    return self.arr_duplicate.count;
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    switch (self.barItemIndex) {
    //        case 1:
    //        {
    //            NSDictionary *dict = [self.arr_duplicate objectAtIndex:section];
    //            NSArray *arr = [dict objectForKey:@"name"];
    //            return arr.count;
    //        }
    //            break;
    //        case 2:
    //        {
    //            NSDictionary *dict = [self.arr_duplicate objectAtIndex:section];
    //            NSArray *arr = [dict objectForKey:@"number"];
    //            return arr.count;
    //        }
    //            break;
    //        case 3:
    //        {
    //            NSDictionary *dict = [self.arr_duplicate objectAtIndex:section];
    //            NSArray *arr = [dict objectForKey:@"email"];
    //            return arr.count;
    //        }
    //            break;
    //        default:
    //            break;
    //    }
    //    return 0;
    return self.arr_duplicate.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DuplicateCell  * cell= (DuplicateCell *)[tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if (!cell) {
        cell=[[DuplicateCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lbl_fill.backgroundColor = (UIColor *)[arr_color objectAtIndex:(indexPath.row%11)];
    cell.rightUtilityButtons = [self deleteButtons];
    cell.delegate = self;
    cell.img_arr.hidden = YES;
    
    switch (self.barItemIndex) {
        case 1:
        {
            NSDictionary *dict = [self.arr_duplicate objectAtIndex:indexPath.row];
            NSArray *arr = [dict objectForKey:@"name"];
            NSDictionary *dict_contact = [arr firstObject];
            cell.lbl_name.text = [NSString stringWithFormat:@"%@",[dict_contact objectForKey:@"name"]];
            //            NSArray *arr_phn = (NSArray *)[dict_contact objectForKey:@"phoneNumbers"];
            //            NSArray *arr_email = (NSArray *)[dict_contact objectForKey:@"emailAddresses"];
            //            NSString *tex =(arr_phn.count > 0) ? [arr_phn firstObject] : (arr_email.count > 0) ? [arr_email firstObject] : @"No contact details";;
            cell.lbl_detail.text =[NSString stringWithFormat:@"%lu Contacts", (unsigned long)arr.count];
            cell.lbl_initial.text = [cell.lbl_name.text substringToIndex:1];
            [cell.view_selected setHidden:([[dict objectForKey:@"isSelected"] integerValue] == 0)];
        }
            break;
        case 2:
        {
            //            if(arr_dupNum.count>0)
            //                [ self.NODpContact setHidden:YES];
            
            NSDictionary *dict = [_arr_duplicate objectAtIndex:indexPath.row];
            NSArray *arr = [dict objectForKey:@"number"];
            NSDictionary *data = [arr firstObject];
            cell.lbl_name.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"name"]];
            //            NSString *dupNum = [dict objectForKey:@"dup_num"];
            //            NSString * result = [[data objectForKey:@"phoneNumbers"] componentsJoinedByString:@"\n"];
            //            NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:result];
            //            NSRange boldRange = [result rangeOfString:dupNum];
            //            [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:boldRange];
            //            [cell.lbl_detail setAttributedText: yourAttributedString];
            cell.lbl_detail.text =[NSString stringWithFormat:@"%lu Contacts", (unsigned long)arr.count];
            cell.lbl_initial.text = [cell.lbl_name.text substringToIndex:1];
            nameHeight=[self getMessageSize:cell.lbl_name];
        }
            break;
        case 3:
        {
            
            //            if(arr_dupNum.count>0)
            //                [ self.NODpContact setHidden:YES];
            NSDictionary *dict = [self.arr_duplicate objectAtIndex:indexPath.row];
            NSArray *arr = [dict objectForKey:@"email"];
            NSDictionary *data = [arr firstObject];
            cell.lbl_name.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"name"]];
            //            NSString *dupNum = [dict objectForKey:@"dup_email"];
            //            NSString * result = [[data objectForKey:@"emailAddresses"] componentsJoinedByString:@"\n"];
            //            NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:result];
            //            NSRange boldRange = [result rangeOfString:dupNum];
            //            [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:boldRange];
            //            [cell.lbl_detail setAttributedText: yourAttributedString];
            //            NSLog(@"cell %@", cell.contactName);
            cell.lbl_detail.text =[NSString stringWithFormat:@"%lu Contacts", (unsigned long)arr.count];
            cell.lbl_initial.text = [cell.lbl_name.text substringToIndex:1];
            nameHeight=[self getMessageSize:cell.lbl_name];
        }
            break;
        default:
            break;
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DuplicateCell *cell = (DuplicateCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell showRightUtilityButtonsAnimated:YES];
    
    
    
    //    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
    //
    //        UIImage     * iconImage = [UIImage imageNamed:@"wallpaper1"];
    //        NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
    //
    //        [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
    //
    //        _documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
    //        _documentInteractionController.UTI = @"net.whatsapp.image";
    //        _documentInteractionController.delegate = self;
    //
    //        [_documentInteractionController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
    //    }
    
    //    [self selectCell:indexPath andAuto:NO];
    //    [self.tbl_duplicate reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
}


//-(void)selectCell:(NSIndexPath *)indexPath andAuto:(BOOL)isAuto
//{
//    NSMutableDictionary *dict = [[self.arr_duplicate objectAtIndex:indexPath.row] mutableCopy];
//    [dict setObject:([[dict objectForKey:@"isSelected"] integerValue] == 0 || isAuto) ? @"1" : @"0" forKey:@"isSelected"];
//    [self.arr_duplicate replaceObjectAtIndex:indexPath.row withObject:dict];
//}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if(self.barItemIndex == 2){
    //
    //        NSDictionary *dictName = [self.arr_duplicate objectAtIndex:indexPath.section];
    //        NSArray *arrName = [dictName objectForKey:@"number"];
    //        NSDictionary *dataName = [arrName objectAtIndex:indexPath.row];
    //
    //        NSString *strName=[dataName objectForKey:@"givenName"];
    //        NSArray *split = [strName componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    //        split = [split filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
    //
    //        //        NSLog(@"Name in dupNumber at row %ld---%@",(long)indexPath.row, strName);
    //        NSDictionary *dict = [self.arr_duplicate objectAtIndex:indexPath.section];
    //        NSArray *arr = [dict objectForKey:@"number"];
    //        NSDictionary *data = [arr objectAtIndex:indexPath.row];
    //        NSArray *arr_num = [data objectForKey:@"phoneNumbers"];
    //        CGFloat Height = (16*arr_num.count) +20;
    //        if(split.count==0 &&(arr_num.count>=1))
    //            return (16*arr_num.count) +40;
    //        else if(arr_num.count == 1 && split.count==1)
    //            return 60;
    //        else if (split.count>1 && arr_num.count == 1)
    //            return 30+nameHeight;
    //        else if(split.count==1 && arr_num.count>1)
    //            return nameHeight+Height;
    //        return Height;
    //    }
    //    else
    return 60;
}

-(CGFloat)getMessageSize :(UILabel*)label
{
    label.numberOfLines = 0;
    CGRect rect = [label.text boundingRectWithSize:CGSizeMake(label.frame.size.width, CGFLOAT_MAX)
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{NSFontAttributeName: label.font}
                                           context:nil];
    rect.size.width = ceil(rect.size.width);
    rect.size.height = ceil(rect.size.height);
    return rect.size.height;
}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *contentView=[[UIView alloc]init];
//    [contentView setFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
//    [contentView setBackgroundColor:[[UIColor whiteColor]colorWithAlphaComponent:0.6]];
//    
//    UILabel *lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,0,tableView.frame.size.width, 5)];
//    [lblBorder setBackgroundColor:[UIColor colorWithRed:67.0/255 green:139.0/255 blue:155.0/255 alpha:0.6]];
//    [contentView addSubview:lblBorder];
//    
//    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 5, tableView.frame.size.width, 30)];
//    [view setBackgroundColor:[UIColor colorWithRed:67.0/255 green:139.0/255 blue:155.0/255 alpha:0.3]];
//    UIButton *btnCancelContact=[UIButton buttonWithType:UIButtonTypeCustom];
//    [btnCancelContact setBackgroundImage:[UIImage imageNamed:@"circle_close"] forState:UIControlStateNormal];
//    [btnCancelContact setFrame:CGRectMake(tableView.frame.size.width - 40, 0, 30, 30)];
//    [btnCancelContact setTag:section];
//    //    [btnCancelContact addTarget:self action:@selector(wrongClicked:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [view addSubview:btnCancelContact];
//    
//    UIButton *btnMergeContact=[UIButton buttonWithType:UIButtonTypeCustom];
//    [btnMergeContact addTarget:self action:@selector(rightClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [btnMergeContact setFrame:CGRectMake(btnCancelContact.frame.origin.x- 40, 0, 30, 30)];
//    [btnMergeContact setTag:section];
//    [btnMergeContact setBackgroundImage:[UIImage imageNamed:@"circle_right"] forState:UIControlStateNormal];
//    
//    [view addSubview:btnMergeContact];
//    [contentView addSubview:view];
//    return contentView;
//}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 35;
//}

//    if(![[dict_plist objectForKey:@"registered_email"] isEqualToString:@""]){
//        [self MergeAtIndex:sect.tag andIsDeleteAll:NO];
//    }else{
//        UIAlertController *alert_controller = [UIAlertController alertControllerWithTitle:@"Info" message:@"to Watch BackUp history the user must be register..." preferredStyle:UIAlertControllerStyleAlert];
//
//        [alert_controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }]];
//
//        [alert_controller addAction:[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//            //                    [self performSegueWithIdentifier:@"toRegistration" sender:self];
//            [self goRegistration];
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            [defaults setObject:@"register" forKey:@"pageHide"];
//            [defaults synchronize];
//        }]];
//        [self presentViewController:alert_controller animated:YES completion:nil];
//    }
//}

#pragma mark - headerViewDeleagte

-(void)btnLeftClicked
{
//    if([self.delegate respondsToSelector:@selector(rescanAfterMerge)])
//        [self.delegate rescanAfterMerge];
    if([self.delegate respondsToSelector:@selector(rescanAfterMerge:)])
                [self.delegate rescanAfterMerge:ismerge];
    [app.navController popViewControllerAnimated:YES];
}

#pragma mark - swtableview cell

- (NSArray *)deleteButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor orangeColor] title:@"Merge" Icon:[UIImage imageNamed: @"merge"]];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]                                                title:@"Delete" Icon:[UIImage imageNamed: @"icon_delete"]];
    return rightUtilityButtons;
}

-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [self.tbl_duplicate indexPathForCell:cell];
    switch (index) {
        case 0:
        {
            if([[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] boolValue]){

                NSDictionary *dict = [self.arr_duplicate objectAtIndex:indexPath.row];

                [self showMergeAlert:dict andIndex:indexPath];
                
            }else{
                [self showLoginPopUp];
            }
        }
            break;
        case 1:
        {
            [self.arr_duplicate removeObjectAtIndex:indexPath.row];
            [self.tbl_duplicate reloadData];
        }
            break;
        default:
            break;
    }
}

-(void) btn_optionMerge:(NSIndexPath *)indexpath WithCompletionHandler:(void(^)(CNContact *contacts, BOOL completed))completionHandler
{
    //    NSLog(@"sect tag %ld", (long)sect.tag);
    NSDictionary *dict = [self.arr_duplicate objectAtIndex:indexpath.row];
    switch (self.barItemIndex) {
            
        case 1:{
            [[ContactAction sharedContacts] mergeDuplicateContactByName:dict WithCompletionHandler:^(CNContact *contact, BOOL completed) {
                //                                [self scan];
                
                [UIView animateWithDuration:0.5 animations:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.arr_duplicate removeObjectAtIndex:indexpath.row];
                        [self.tbl_duplicate reloadData];
                    });
                } completion:^(BOOL finished) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD performSelector:@selector(dismiss) withObject:nil afterDelay:1.0];
                    });
                    
                    completionHandler(contact, YES);
                }];
            }];
        }
            break;
        case 2:{
            [[ContactAction sharedContacts] mergeDuplicateContactByNumber:dict WithCompletionHandler:^(BOOL completed) {
                //                [self scan];
                [UIView animateWithDuration:0.5 animations:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.arr_duplicate removeObjectAtIndex:indexpath.row];
                        [self.tbl_duplicate reloadData];
                    });
                } completion:^(BOOL finished) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD performSelector:@selector(dismiss) withObject:nil afterDelay:1.0];
                    });
                    completionHandler(nil, completed);
                }];
            }];
        }
            break;
        case 3:{
            [[ContactAction sharedContacts] mergeDuplicateContactByEmail:dict WithCompletionHandler:^(BOOL completed) {
                //                [self scan];
                [UIView animateWithDuration:0.5 animations:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.arr_duplicate removeObjectAtIndex:indexpath.row];
                        [self.tbl_duplicate reloadData];
                    });
                } completion:^(BOOL finished) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD performSelector:@selector(dismiss) withObject:nil afterDelay:1.0];
                    });
                    completionHandler(nil, completed);
                }];
            }];
        }
            break;
        default:
            break;
    }
}
#pragma mark - IBACtion

- (IBAction)btn_mergeAll:(id)sender {
//    [_btn_mergeAll setUserInteractionEnabled:NO];
    if(_arr_duplicate.count > 0){
    NSString *msg = [NSString stringWithFormat:@"Are you sure want to merge all contact by %@",(_barItemIndex == 1) ? @"name" : (_barItemIndex == 2) ? @"number" : @"email"];
    [[AlertControllerDesign sharedObject]alertController:@"Info" withMessage:msg andButtonOne:@"Yes" andButtontwo:@"No" WithCompletionHandler:^(BOOL completed) {
        if(completed)
        {
            [self showShareView];
            self.shareview.delegate = self;
        }
    }];
    }else{
        [self.view makeToast:@"No Duplicate contact to merge" duration:1.0 position:@"center"];
    }
//    [self mergeAllWithCompletion];
}

-(void)mergeAllWithCompletion
{
    NSInteger totalCount = self.arr_duplicate.count;
    NSInteger __block count = 0;
    
     if([[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] boolValue]){
        if(totalCount == 0){
            [self.view makeToast:@"Don't have contact to merge"];
        }else {
        
        [self.view makeToast:@"Merging"];
        switch (self.barItemIndex) {
            case 1:
            {
                for(NSDictionary *dict in self.arr_duplicate){
//                    [self.view makeToast:@"Merging"];
                    //                    NSInteger ind = [self.arr_duplicate indexOfObject:dict];
                    ++count;
//                    NSString *msg = [NSString stringWithFormat:@"Merging %ld",((count / totalCount)*100)];
//                    UILabel *lbl = (UILabel *)[self.view viewWithTag:7368];
//                    lbl.text = msg;
                    if(count == totalCount)
                    {
//                        [self performSelector:@selector(stopActivityIndicator) withObject:nil afterDelay:1.0];
                        [self showMergePopUp];
                    }
                    [[ContactAction sharedContacts] mergeDuplicateContactByName:dict WithCompletionHandler:^(CNContact *contact, BOOL completed) {
                    }];
                }
                //                [self showMergePopUp];
            }
                break;
            case 2:
            {
                for(NSDictionary *dict in self.arr_duplicate){
                    ++count;
//                    [self.view makeToast:@"Merging"];

//                    NSString *msg = [NSString stringWithFormat:@"Merging %ld",((count / totalCount)*100)];
//                    UILabel *lbl = (UILabel *)[self.view viewWithTag:7368];
//                    lbl.text = msg;
                    if(count == totalCount)
                    {
//                        [self performSelector:@selector(stopActivityIndicator) withObject:nil afterDelay:1.0];
                        [self showMergePopUp];
                    }
                    [[ContactAction sharedContacts] mergeDuplicateContactByNumber:dict WithCompletionHandler:^(BOOL completed) {
                    }];
                }
                //                [self showMergePopUp];
            }
                break;
            case 3:
            {
                for(NSDictionary *dict in self.arr_duplicate){
                    ++count;
//                    [self.view makeToast:@"Merging"];

//                    NSString *msg = [NSString stringWithFormat:@"Merging %ld",((count / totalCount)*100)];
//                    UILabel *lbl = (UILabel *)[self.view viewWithTag:7368];
//                    lbl.text = msg;
                    if(count == totalCount)
                    {
//                        [self performSelector:@selector(stopActivityIndicator) withObject:nil afterDelay:1.0];
                        [self showMergePopUp];
                    }
                    [[ContactAction sharedContacts] mergeDuplicateContactByEmail:dict WithCompletionHandler:^(BOOL completed) {
                    }];
                }
            }
                break;
            default:
                [self.view makeToast:@"please select any option"];
                break;
        }
        }
        [self.tbl_duplicate reloadData];
        [self ismergeDone];
    }else{
        [self showLoginPopUp];
    }
}
#pragma mark - MergePop UPView

-(void)showMergePopUp{
    if(mergePop == nil){
        //    [self.view makeToast:@"Rescaning...."];
        UIView *view1 = [[UIView alloc]initWithFrame:self.view.frame];
        view1.tag = 123;
        view1.alpha = 0.0;
        [view1 setBackgroundColor:[[UIColor blackColor]colorWithAlphaComponent:0.35]];
        CGFloat height = (_arr_duplicate.count > 6) ? 350 : (_arr_duplicate.count * 40)+150;
        mergePop = [[MergePopUp alloc]initWithFrame:CGRectMake(FRAMEWIDTH *0.1, self.view.center.y - 187, FRAMEWIDTH *0.8, height)];
        mergePop.barItemIndex = _barItemIndex;
        //    mergePop.arr_display = self.arr_duplicate;
        //        [mergePop.tableview reloadData];
        [mergePop.layer setCornerRadius:20];
        [mergePop setClipsToBounds:YES];
        [mergePop setDelegate:self];
        [mergePop reloadCell:[NSMutableArray arrayWithArray:[_arr_duplicate copy]]];
        [mergePop setBackgroundColor:[[UIColor blackColor]colorWithAlphaComponent:0.65]];
        [view1 addSubview:mergePop];
        [self.view addSubview:view1];

        [UIView animateWithDuration:0.5 animations:^{
            view1.alpha = 1.0;
            self->mergePop.alpha = 1.0;
        }];
    }
}
#pragma mark - MergeAlert

-(void)showMergeAlert:(NSDictionary *)dict andIndex:(NSIndexPath *)index{
    NSLog(@"dict asdas  %@", dict);
    if(mergeAlert == nil){
        UIView *view = [[UIView alloc]initWithFrame:self.view.frame];
        view.tag = 123;
        view.alpha = 0.0;
        [view setBackgroundColor:[[UIColor blackColor]colorWithAlphaComponent:0.35]];
        mergeAlert = [[MergeAlert alloc]initWithFrame:CGRectMake(FRAMEWIDTH *0.1, self.view.center.y - 125, FRAMEWIDTH *0.8, 250)];
        [mergeAlert.layer setCornerRadius:20];
        [mergeAlert setClipsToBounds:YES];
        [mergeAlert setDelegate:self];
        mergeAlert.key  = (_barItemIndex == 1) ? @"name" : (_barItemIndex == 2)? @"number":@"email";
        mergeAlert.dict_contact = dict;
        [mergeAlert callReloadCell:dict andKey:(_barItemIndex == 1) ? @"name" : (_barItemIndex == 2)? @"number":@"email"];
        [mergeAlert setBackgroundColor:[[UIColor blackColor]colorWithAlphaComponent:0.65]];
        mergeAlert.index = index;
        [view addSubview:mergeAlert];
        [self.view addSubview:view];
        
        [UIView animateWithDuration:0.5 animations:^{
            view.alpha = 1.0;
        }];
    }
}
-(void)hideMergeAlert
{
    [UIView animateWithDuration:0.5 animations:^{
        self->mergeAlert.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self->mergeAlert removeFromSuperview];
        self->mergeAlert = nil;
        UIView *view = [self.view viewWithTag:123];
        [view removeFromSuperview];
    }];
}
-(void)btn_YesAlert:(NSIndexPath *)index
{
    
    [self hideMergeAlert];
    [SVProgressHUD showWithStatus:@"Merging"];
    [self btn_optionMerge:index WithCompletionHandler:^(CNContact *contact, BOOL completed) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        [self ismergeDone];
    }];
}

- (void)btn_CancelAlert
{
    [self hideMergeAlert];
}
#pragma mark - LoginPopUpDelegate
-(void)btn_close:(int)sender{

    [UIView animateWithDuration:0.5 animations:^{
        self->popup.alpha = 0.0;
        self->mergePop.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self->popup removeFromSuperview];
        self->popup = nil;
        [self->mergePop removeFromSuperview];
        self->mergePop = nil;
        UIView *view = [self.view viewWithTag:123];
        [view removeFromSuperview];
        if(sender == 1 && self->shouldRewarded){
            [self addRewardApiCall];
            self->shouldRewarded = NO;
            [self performSelector:@selector(mergeAllWithCompletion) withObject:nil afterDelay:1.0];
//            dispatch_async(dispatch_get_main_queue(), ^{
//               [self performSelector:@selector(mergeAllWithCompletion) withObject:nil afterDelay:1.0];
//            });
        }
    }];
}

-(void)signInClick:(NSString *)txtEmail
{
    if(![[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] boolValue]){
        
        NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:txtEmail,@"email_id",@"",@"password",nil];
        httpHelper = [[HttpHelper alloc]initWithMUrl:registerUserURL Dictionary:[dict copy] isloading:YES api_number:registerUserAPI message:@"Loading all backup"];
        httpHelper.delegate= self;
    }
}

-(void)signInWithPassword:(NSString *)txt_pass withEmail:(NSString *)txtEmail
{
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:txtEmail,@"email_id", txt_pass, @"password", nil];
    httpHelper = [[HttpHelper alloc]initWithMUrl:savePasswordURL Dictionary:[dict copy] isloading:YES api_number:savePasswordAPI message:@"Loading all backup"];
    httpHelper.delegate= self;
}

-(void)btn_skip
{
    [self btn_close:0];
}

//-(void)btn_GoogleLogin
//{
//    [self googleLogin];
//}

-(void)hideMergePopup
{
    [self.arr_duplicate removeAllObjects];
    [self.tbl_duplicate reloadData];
    [self btn_close:0];
}

#pragma mark shareDialogviewDelegate
-(void)shareSuccess
{
    if([SVProgressHUD isVisible])
        [SVProgressHUD dismiss];


    [[AlertControllerDesign sharedObject] alertController:@"Backup" withMessage:@"We strongly recommend you to take backup of contact merge. Would like to take backup ?" andButtonOne:@"Ok" andButtontwo:@"Cancel" WithCompletionHandler:^(BOOL completed) {
        if(completed)
        {
            if([[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] boolValue]){
                dispatch_async(dispatch_get_main_queue(), ^{
                [self addRewardApiCall];
                });
                int numOfBackup = [self getBackUpCount] + 1;
                if(self->count < numOfBackup){
                    [self saveAndUploadContact];
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString * mess = [NSString stringWithFormat:@"User can save only %d back up file. please clear any backup to take more backup.", numOfBackup];
                        [[AlertControllerDesign sharedObject] alertController:mess withMessage:@"" andButtonOne:@"Ok" andButtontwo:@"Cancel" WithCompletionHandler:^(BOOL completed) {
                            if(completed)
                                [self redirectToBackupHistory];
                        }];
                    });
                }
            }else{
                [self showLoginPopUp];
            }
        }else{
            self->shouldRewarded = YES;
        }
        [self mergeAllWithCompletion];
    }];
    
}

-(void)shareFailed
{
    NSLog(@"share failed");
}

-(void)saveAndUploadContact
{
    [[ContactList sharedContacts] fetchAllContacts:YES WithCompletionHandler:^(int result, NSMutableArray *arr) {
        if(result == 2){
            [[ContactList sharedContacts] generateAndSaveVCF:arr WithUSerID:[[PlistHandler sharedObject]getValueFromPlistByKey:@"user_id"] andCompletionHandler:^(NSString *filename, BOOL completed) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([SVProgressHUD isVisible])
                        [SVProgressHUD dismiss];
                    /*
                     [[ContactList sharedContacts]uploadFile:[[PlistHandler sharedObject]getValueFromPlistByKey:@"registered_email"] WithUserID:[[PlistHandler sharedObject]getValueFromPlistByKey:@"user_id"]];
                     [[PlistHandler sharedObject]setBackupCount:++count];
                     */
                    
                    
                    [[ContactList sharedContacts]uploadFile:[[PlistHandler sharedObject]getValueFromPlistByKey:@"registered_email"] WithUserID:[[PlistHandler sharedObject]getValueFromPlistByKey:@"user_id"]];
                    [[PlistHandler sharedObject]setBackupCount:++count];
                    
                });
            }];
        }else{
            
        }
    }];
}
@end
