//
//  DuplicateCell.m
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 13/11/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "DuplicateCell.h"

@implementation DuplicateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.cnst_Height.constant = self.frame.size.height *0.6;
    self.view_initial.layer.cornerRadius = (self.frame.size.height*0.6)/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)retest{
    self.cnst_Height.constant = self.frame.size.height *0.8;
    self.view_initial.layer.cornerRadius = (self.frame.size.height*0.8)/2;
    [self layoutIfNeeded];
}
@end
