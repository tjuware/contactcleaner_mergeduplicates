//
//  DuplicateCell.h
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 13/11/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface DuplicateCell : SWTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_detail;
@property (strong, nonatomic) IBOutlet UIView *view_initial;
@property (strong, nonatomic) IBOutlet UILabel *lbl_initial;
@property (strong, nonatomic) IBOutlet UILabel *lbl_fill;
@property (strong, nonatomic) IBOutlet UIView *view_selected;
@property (strong, nonatomic) IBOutlet UIImageView *img_arr;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_Height;
-(void)retest;
@end
