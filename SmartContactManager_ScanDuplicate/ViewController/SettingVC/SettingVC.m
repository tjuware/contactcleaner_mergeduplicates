//
//  SettingVC.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 10/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "SettingVC.h"
#import "ContactCell.h"
#import "DuplicateCell.h"
#import "TOPasscodeViewController.h"
#import "PopUpVC.h"
#import "PrivacyPolicyFaq.h"
@interface SettingVC ()<HeaderViewDelegate, UITableViewDelegate, UITableViewDataSource, HttpHelperDelegate, SWTableViewCellDelegate, TOPasscodeViewControllerDelegate,MFMailComposeViewControllerDelegate,UIAdaptivePresentationControllerDelegate,PopUpVCDelegate,GADBannerViewDelegate>
{
    NSArray *array_icon, *array_title, *arr_reminder;
    NSMutableArray *arr_backup;
    BOOL isSelected;
    TOPasscodeViewController *passcodeVC;
    NSString *password, *viewName, *oldPass, *newPass, *cnfNewPass;
    NSIndexPath *pathsaved;
}
@property (strong, nonatomic) IBOutlet UIView *emailview;
@property (strong, nonatomic) IBOutlet UILabel *lbl_emailID;
@property (strong, nonatomic) IBOutlet UILabel *lbl_version;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *emailview_height;
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, assign) TOPasscodeViewStyle style;
@property (weak, nonatomic) IBOutlet UIView *adsview;
@property (nonatomic, assign) TOPasscodeType type;
@property (weak, nonatomic) IBOutlet UIView *imfomationView;
@property (weak, nonatomic) IBOutlet UIButton *recoveryButton;
@property (strong, nonatomic) IBOutlet UILabel *lbl_rewardPoint;

@end

@implementation SettingVC
-(void)didSucess:(NSDictionary *)dict{
    [[ContactAction sharedContacts]addNewContactToDevice:dict andSaveDirectly:YES];
    [arr_backup removeObjectAtIndex:pathsaved.row];
    [self.tableview deleteRowsAtIndexPaths:@[pathsaved] withRowAnimation:UITableViewRowAnimationLeft];
    [[FileManipulation sharedContacts]writeRecoverPhoneContactToJsonFile:arr_backup andFileName:RECOVER];
    [self removeAnimate];
}

- (IBAction)brn_close:(id)sender {
    [self removeAnimate];
}

- (IBAction)recovery_btn:(id)sender {
    
    NSDictionary *dict = [arr_backup objectAtIndex:pathsaved.row];
    [[ContactAction sharedContacts]addNewContactToDevice:dict andSaveDirectly:YES];
    [arr_backup removeObjectAtIndex:pathsaved.row];
    [self.tableview deleteRowsAtIndexPaths:@[pathsaved] withRowAnimation:UITableViewRowAnimationLeft];
    [[FileManipulation sharedContacts]writeRecoverPhoneContactToJsonFile:arr_backup andFileName:RECOVER];
    [self removeAnimate];
}

- (void)showAnimate
{
    _imfomationView.transform = CGAffineTransformMakeScale(1.3, 1.3);
    _imfomationView.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        [self.imfomationView setHidden:false];
        self->_imfomationView.alpha = 1;
        self->_imfomationView.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self->_imfomationView.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self->_imfomationView.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.imfomationView setHidden:NO];
        }
    }];
}
- (IBAction)btnClose:(id)sender {
    [self removeAnimate];
}
-(void)setShadowEffecttoView :(UIView *)viewCheck
{
    viewCheck.layer.shadowRadius  = 1.5f;
    viewCheck.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    viewCheck.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    viewCheck.layer.shadowOpacity = 0.9f;
    viewCheck.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(viewCheck.bounds, shadowInsets)];
    viewCheck.layer.shadowPath    = shadowPath.CGPath;
}
-(void)setBackApiResponse:(NSMutableDictionary *)response api_number:(int)api_number
{
    if([[response objectForKey:@"success"] boolValue])
    {
        if(api_number == registerUserAPI)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
            NSDictionary *dict = [[response objectForKey:@"user_details"] firstObject];
            NSLog(@"user_details Dict In SettingVC %@",dict);
            [[PlistHandler sharedObject] savePlistDataWithData:[self removeNullValuefromDictionary:dict]];
            
            _lbl_emailID.text =[NSString stringWithFormat:@"Your email ID: %@", [dict objectForKey:@"registered_email"]];
            NSString *reward = [self handleNullValueOfString:[dict objectForKey:REWARD_POINT]];
            _lbl_rewardPoint.text = [NSString stringWithFormat:@"Your reward points : %@",reward];
            
            NSString *pass = [dict objectForKey:@"password"];
                if(pass.length <= 0 || pass == nil)
                {
                    [popup viewWithPassword];
                }else{
                    [self btn_close:0];
                }
            [self refresh];
        }else if(api_number == changePasswordAPI)
        {
            NSLog(@"response %@", response);
        }else if (api_number == sendPasswordAPI){
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showSuccessWithStatus:[response objectForKey:@"message"]];
            });
            
        }else if(api_number == savePasswordAPI)
        {
            NSLog(@"response %@", response);
            NSLog(@"dict_plist %@", dict_plist);
            NSDictionary *dict = [[response objectForKey:@"user_details"] firstObject];
             NSLog(@"user_details Dict In SettingVC %@",dict);
            NSString *pass = [dict objectForKey:@"password"];
            [self updatePlistDictioryByKey:@"password" andValue:pass];
            [self refresh];
            
        }else if (api_number == claimRewardAPI){
            NSInteger highScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"HighScore"];

            [self updateBackupCount:highScore++];
        }
        else{
            [super setBackApiResponse:response api_number:api_number];
        }
    }
}

-(void)responseError:(NSError *)error api_number:(int)api_number
{
    [self showErrorWithStatus:@"Something Went Wrong, Check Internet"];
    [self refresh];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _lbl_rewardPoint.text = @"";
    _lbl_emailID.text = @"";
    NSString *version = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
    _lbl_version.text =[NSString stringWithFormat:@"Scan Duplicate V.%@",version];
    
    [self setShadowEffecttoView:_imfomationView];
//    [self.view_header changeBackgroundColor:Bg_color];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"icon_menu"]];
    [self.view_header setTitle:(_isForSetting) ? @"SettingVC" : @"Recovery"];
    self.view_header.delegate = self;
    [self.tableview registerNib:[UINib nibWithNibName:@"ContactCell" bundle:nil] forCellReuseIdentifier:CONTACTCELL_ID];
    [self.tableview registerNib:[UINib nibWithNibName:@"DuplicateCell" bundle:nil] forCellReuseIdentifier:@"CellID"];
//    NSString *pass = [self handleNullValueOfString:[[PlistHandler sharedObject] getValueFromPlistByKey:@"password"]];
//    array_title = [[NSArray alloc]initWithObjects:@"Change Password", (pass.length > 0) ? @"Forgot Password" : @"Set Password", @"Reminder", @"Reset", nil];
//    array_icon = [[NSArray alloc]initWithObjects:@"icon_fPass", @"icon_PassF", @"icon_reminder", @"icon_reset", nil];
//    arr_reminder = [[NSArray alloc]initWithObjects:@"5 days", @"10 days", @"15 days", @"30 days", nil];
    if(!self.isForSetting){
        NSDictionary *dict = [[FileManipulation sharedContacts] readWholePhoneContactDictionaryFromJsonFile: RECOVER];
       arr_backup = [[dict objectForKey:@"PhoneContacts"]mutableCopy];
        [self.emailview_height setConstant:0];
        
    }
    else{
        [self.emailview setHidden:NO];
        [self.emailview_height setConstant:50];
        if([[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] boolValue]){
            _lbl_emailID.text =[NSString stringWithFormat:@"Your email ID: %@", [[PlistHandler sharedObject]getValueFromPlistByKey:@"registered_email"]];
            NSString * reward = [self handleNullValueOfString : [[PlistHandler sharedObject]getValueFromPlistByKey:REWARD_POINT] ];
            _lbl_rewardPoint.text = [NSString stringWithFormat:@"Your reward points : %@",(reward.length == 0)? @"0" : reward];
    }
    }
        [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    isSelected = NO;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self returnbannerView:_adsview :self];
//    [self returnbannerView:_adsview :self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    password = [dict_plist objectForKey:@"password"];
    [self refresh];
}
#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  (_isForSetting) ? array_title.count : 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_isForSetting){
        if(isSelected && section == 2)
            return 5;
        return 1;
    }else
        return arr_backup.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!self.isForSetting){
        DuplicateCell *cell = (DuplicateCell *)[tableView dequeueReusableCellWithIdentifier:@"CellID"];
        if(!cell)
            cell = [[DuplicateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
        NSDictionary *dict;
            dict = [arr_backup objectAtIndex:indexPath.row];
            cell.rightUtilityButtons = [self deleteButtons];
            cell.delegate = self;
        NSString *title = [dict objectForKey:@"name"];
        
//        cell.image.image = [UIImage imageNamed:icon];
        cell.lbl_name.text = title;
        NSArray *arr = [cell.lbl_name.text componentsSeparatedByString:@" "];
        cell.lbl_detail.text = @"";
        cell.lbl_fill.backgroundColor = (UIColor *)[arr_color objectAtIndex:(indexPath.row%11)];
        if(arr.count > 1)
        {
            NSString *sec = (NSString *)[arr lastObject];
            NSString *str = [NSString stringWithFormat:@"%@%@",[[arr firstObject] substringToIndex:1], (sec.length > 0) ? [sec substringToIndex:1] : @""];
            cell.lbl_initial.text = str;
        }else{
            cell.lbl_initial.text = [cell.lbl_name.text substringToIndex:1];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }else{
        ContactCell *cell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:CONTACTCELL_ID];
        if(!cell)
            cell = [[ContactCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CONTACTCELL_ID];
        NSString *title = [array_title objectAtIndex:indexPath.section];
        NSString *icon = [array_icon objectAtIndex:indexPath.section];
        
        cell.image.image = [UIImage imageNamed:icon];
        cell.lbl_name.text = title;
        cell.lbl_name.textColor = [UIColor darkGrayColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.cnst_left.constant = 16;
        [cell.img_check setHidden:YES];
        cell.cnst_rightToParentLabelName.priority = 999;
        
        if(indexPath.section == 2 && indexPath.row > 0)
        {
            NSString *subtitle = [arr_reminder objectAtIndex:indexPath.row - 1];
            cell.image.image = [UIImage imageNamed:@"icon_timer"];
            cell.lbl_name.text = subtitle;
            cell.cnst_left.constant = 35;
            if([[[PlistHandler sharedObject]getValueFromPlistByKey:@"reminderSelect"] integerValue] == indexPath.row){
                [cell.img_check setHidden:NO];
                cell.cnst_rightToParentLabelName.priority = 555;
                [cell.img_check setImage:[UIImage imageNamed:@"icon_doubleTick"]];
            }
        }
        return cell;
    }
    
   
  
}
- (NSArray *)deleteButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor: [UIColor orangeColor] title:@"Recover" Icon:[UIImage imageNamed: @"icon_restore"]];
    return rightUtilityButtons;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL isLogin = [[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] boolValue];
    
    
    
    if(_isForSetting){
        switch (indexPath.section) {
            case 0: //:@"Change Password"
            {
                if(isLogin){
//                    [[AlertControllerDesign sharedObject] alertControllerForChangePasswordWithCompletionHandler:^(NSMutableDictionary *dict, BOOL completed) {
//                        [dict setObject:[[PlistHandler sharedObject] getValueFromPlistByKey:@"registered_email"] forKey:@"registered_email"];
//                        self->httpHelper=[[HttpHelper alloc]initWithMUrl:changePasswordURL Dictionary:dict isloading:YES api_number:changePasswordAPI message:@"Creating new pin"];
//                        self->httpHelper.delegate=self;
//                    }];
                    if(password.length > 0){
                        passcodeVC = [[TOPasscodeViewController alloc] initWithStyle:self.style withTitle:@"Old Password" passcodeType:self.type];
                        viewName = @"oldPassword";
                        passcodeVC.delegate = self;
                        [self presentViewController:passcodeVC animated:YES completion:nil];
                    }else{
                        passcodeVC = [[TOPasscodeViewController alloc] initWithStyle:self.style withTitle:@"Set Password" passcodeType:self.type];
                        viewName = @"setPassword";
                        passcodeVC.delegate = self;
                        [self presentViewController:passcodeVC animated:YES completion:nil];
                    }
                }else{
                    [self.view makeToast:@"Please Login"];
//                    [self loginAlertController];
                    [self showLoginPopUp];
                }
            }
                break;
            case 1:{ //"Forgot Password"
                NSString *email_id = [[PlistHandler sharedObject] getValueFromPlistByKey: @"registered_email"];
                NSString *pass = [self handleNullValueOfString:[[PlistHandler sharedObject] getValueFromPlistByKey:@"password"]];
                if(isLogin){
                    if(pass.length > 0){
                        NSDictionary *valuepairs=[NSDictionary dictionaryWithObjectsAndKeys:email_id,@"registered_email",nil];
                        httpHelper=[[HttpHelper alloc]initWithMUrl:sendPasswordURL Dictionary:valuepairs isloading:YES api_number:sendPasswordAPI message:@"Sending..."];
                        httpHelper.delegate=self;
                    }else {
                        [[AlertControllerDesign sharedObject]alertControllerForSavePasswordWithCompletionHandler:^(NSMutableDictionary *dict, BOOL completed) {
                            [dict setObject:[[PlistHandler sharedObject] getValueFromPlistByKey:@"registered_email"] forKey:@"registered_email"];
                            
                            self->httpHelper = [[HttpHelper alloc]initWithMUrl:savePasswordURL Dictionary:[dict copy] isloading:YES api_number:savePasswordAPI message:@"Saving password"];
                            self->httpHelper.delegate = self;
                        }];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                        });
                    }
                }else{
                    [self.view makeToast:@"Please Login"];
//                    [self loginAlertController];
                    [self showLoginPopUp];
                }
            }
                break;
            case 2:{   // @"Reminder"
                if(indexPath.row == 0){
                    isSelected = !isSelected;
                }
                if(indexPath.row > 0){
                    [[PlistHandler sharedObject]setValueFromPlistByvalue:[NSString stringWithFormat:@"%lu",indexPath.row] andKey:@"reminderSelect"];
                    [[LocalNotifications sharedObject] performSelector:@selector(resetLocalNotification) withObject:nil afterDelay:0.5];
                }
                [self.tableview reloadData];
            }
                break;
            case 3:     {
                if([MFMailComposeViewController canSendMail]) {
                    MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
                    mailCont.mailComposeDelegate = self;
                    NSString *url=@"https://itunes.apple.com/us/app/id1444870653";
                    [mailCont setSubject:[NSString stringWithFormat:@"FeedBack - from %@",[[PlistHandler sharedObject]getValueFromPlistByKey:@"registered_email"]]];
                    [mailCont setToRecipients:[NSArray arrayWithObject:@"info@techathalon.com"]];
                    [mailCont setMessageBody:[NSString stringWithFormat:@"The most secure contact backup. Download Smart Contact Manager - Backup app %@", url] isHTML:NO];
                    
                    [app.navController presentViewController:mailCont animated:YES completion:nil];
                }
            }
                break;
            case 4:{
                PrivacyPolicyFaq *privacy = [[PrivacyPolicyFaq alloc]init];
                privacy.urlString = privacyPath ;
                privacy.title = @"Pricacy Policy";
                [app.navController pushViewController:privacy animated:YES];
            }
                break;
            case 5:{ // "Reset"
                [[AlertControllerDesign sharedObject] alertControllerForLogoutWithCompletionHandler:^(BOOL completed) {
                    if(completed){
                        [self.view makeToast:@"Account Reset Successfully"];
                        [self.lbl_emailID setText:@""];
                        [self.lbl_rewardPoint setText:@""];
                        [[PlistHandler sharedObject]removeDataFromPlist];
                        [self refresh];
                        [self redirectToHome];
                    }
                }];
            }
                break;
            case 6: {
                NSString * point = [[PlistHandler sharedObject]getValueFromPlistByKey:REWARD_POINT];
                point = [self handleNullValueOfString:point];
                int rewardPoint = [[self handleNullValueOfString:point] intValue];
                
                NSString * pointUsed = [[PlistHandler sharedObject]getValueFromPlistByKey:REWARD_USED];
                int rewardUsed = [[self handleNullValueOfString:pointUsed] intValue];
                
                NSString *message = [NSString stringWithFormat:@"Want to upgrade your account? After upgrade you can store %d backup file",(rewardUsed == 0) ? 2 : rewardUsed + 2];
                
                if(rewardPoint > 100){
                    [[AlertControllerDesign sharedObject] alertController:@"REWARDS" withMessage:message andButtonOne:@"Yes" andButtontwo:@"No" WithCompletionHandler:^(BOOL completed) {
                        if(completed){
                            [self claimRewardApiCall];
                        }
                    }];
                }else{
                    [[AlertControllerDesign sharedObject] alertController:@"REWARDS" withMessage:@"You need at 100 point to claim reward" andButtonOne:@"Ok" andButtontwo:@"" WithCompletionHandler:^(BOOL completed) {                    }];
                }
            }
                break;
            default:
                break;
        }
    }
    else if (!_isForSetting)
    {
        NSDictionary *dict = [arr_backup objectAtIndex:indexPath.row];
        //        NSLog(@"dict %@",dict)
        PopUpVC *bcakup = [[PopUpVC alloc]init];
        bcakup.popUpDelegate= self;
        bcakup.info_dict =dict;
        bcakup.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        bcakup.modalTransitionStyle = UIModalPresentationPopover;
        [self presentViewController:bcakup animated:YES completion:nil];
        
        
//
//         DuplicateCell *cell = (DuplicateCell *)[tableView cellForRowAtIndexPath:indexPath];
//
//        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:bcakup];
//        nav.modalPresentationStyle = UIModalPresentationPopover;
//        UIPopoverPresentationController *popover = nav.popoverPresentationController;
//       // bcakup.preferredContentSize = CGSizeMake(bcakup.view.frame.size.width,250);
//        popover.delegate = self;
//        popover.sourceView = self.view;
//        popover.sourceRect = CGRectMake(cell.frame.origin.x,cell.frame.size.width / 2, 0, 0);
//        popover.backgroundColor = [UIColor clearColor];
//        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
//        [self presentViewController:nav animated:YES completion:nil];
//
//        [self showAnimate];
//        NSDictionary *dict;
//        dict = [arr_backup objectAtIndex:indexPath.row];
//        NSLog(@"dict %@",dict);
//
//        NSArray *arrayofPhone =[dict objectForKey:@"phoneNumbers"];
//        if (arrayofPhone.count > 0)
//        {
//            ManaDropDownMenu *menu = [[ManaDropDownMenu alloc] initWithFrame:CGRectMake(20, self.recoveryButton.frame.origin.y - 130, self.imfomationView.frame.size.width -40, 40) title:[arrayofPhone objectAtIndex:0]];
//            menu.delegate = self;
//            menu.backgroundColor = [UIColor clearColor];
//            menu.numberOfRows = arrayofPhone.count;
//            menu.textOfRows = arrayofPhone;
//            [self.imfomationView addSubview:menu];
//
//
//
////            _phoneNumber_manaView.numberOfRows =1;
//
//        }
//
//
//
//        if ([[dict objectForKey:@"emailAddresses"]count] > 0)
//
//
//

        pathsaved = indexPath;
    }
//
}
-(UIModalPresentationStyle) adaptivePresentationStyleForPresentationController: (UIPresentationController * ) controller
{
    return UIModalPresentationNone;
}

#pragma mark - SWTableviewDelegate
-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [_tableview indexPathForCell:cell];
    NSDictionary *dict = [arr_backup objectAtIndex:indexPath.row];
    [[ContactAction sharedContacts]addNewContactToDevice:dict andSaveDirectly:YES];
    [arr_backup removeObjectAtIndex:indexPath.row];
    [self.tableview deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    [[FileManipulation sharedContacts]writeRecoverPhoneContactToJsonFile:arr_backup andFileName:RECOVER];
}

#pragma mark - HeaderViewDelegate
-(void)btnLeftClicked
{
    [self slideView];
}

#pragma mark - TOPasscodeViewControllerDelegate
-(void)didTapCancelInPasscodeViewController:(TOPasscodeViewController *)passcodeViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)passcodeViewController:(TOPasscodeViewController *)passcodeViewController isCorrectCode:(NSString *)code
{
    if([viewName isEqualToString:@"setPassword"]){
        NSDictionary *valuepairs = [[NSDictionary alloc]initWithObjectsAndKeys:[[PlistHandler sharedObject] getValueFromPlistByKey:@"registered_email"], @"registered_email", code, @"password", nil];
        httpHelper=[[HttpHelper alloc]initWithMUrl:savePasswordURL Dictionary:valuepairs isloading:YES api_number:savePasswordAPI message:@"Sending..."];
        httpHelper.delegate=self;
        return YES;
    }else if ([viewName isEqualToString:@"oldPassword"])
    {
        oldPass = code;
        passcodeVC = [[TOPasscodeViewController alloc] initWithStyle:self.style withTitle:@"New Password" passcodeType:self.type];
        viewName = @"newPassword";
        passcodeVC.delegate = self;
        [self performSelector:@selector(presentView) withObject:nil afterDelay:0.1];
        return YES;
        
    }else if ([viewName isEqualToString:@"newPassword"])
    {
        passcodeVC = [[TOPasscodeViewController alloc] initWithStyle:self.style withTitle:@"Confirm Password" passcodeType:self.type];
        viewName = @"cnfPassword";
        passcodeVC.delegate = self;
        newPass = code;
        [self performSelector:@selector(presentView) withObject:nil afterDelay:0.1];
        return YES;
    }else if ([viewName isEqualToString:@"cnfPassword"])
    {
        cnfNewPass = code;
        if([newPass integerValue] == [cnfNewPass integerValue])
        {
            NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:email, @"registered_email", oldPass, @"old_pswd", newPass, @"new_pswd", nil];
            self->httpHelper=[[HttpHelper alloc]initWithMUrl:changePasswordURL Dictionary:dict isloading:YES api_number:changePasswordAPI message:@"Creating new pin"];
            self->httpHelper.delegate=self;
            return YES;
        }else{
            return NO;
        }
    }else
        return YES;
}
-(void)presentView
{
    [self presentViewController:passcodeVC animated:YES completion:nil];
}

-(void)refresh
{
    password = [dict_plist objectForKey:@"password"];
    array_title = [[NSMutableArray alloc]initWithObjects:(password.length > 0) ? @"Change Password" : @"Set Password", @"Forgot Password", @"Reminder",@"Feedback", @"Privacy Policy" ,nil];
    if([[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] integerValue] == 1){
        NSMutableArray *arr = [array_title mutableCopy];
        [arr addObject:@"Reset"];
        [arr addObject:@"Claim reward by 100 points"];
        array_title = [arr copy];
    }
//    array_icon = [[NSMutableArray alloc]initWithObjects:@"icon_fPass", @"icon_reminder", @"icon_feedBack", @"icon_privacy", @"icon_reset", @"icon_reward" ,nil];
    array_icon = [[NSMutableArray alloc]initWithObjects:@"icon_fPass",@"icon_login",  @"icon_reminder", @"icon_feedBack", @"icon_privacy", @"icon_reset", @"icon_reward" ,nil];
   
    arr_reminder = [[NSMutableArray alloc]initWithObjects:@"3 days", @"6 days", @"9 days", @"12 days", nil];
    [self.tableview reloadData];
}


-(void)adViewDidReceiveAd:(GADBannerView *)bannerView{
    NSLog(@"hajshh");
}
- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"hajshh");
}
@end
