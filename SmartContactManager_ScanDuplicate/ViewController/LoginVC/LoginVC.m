//
//  LoginVC.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 06/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "LoginVC.h"
#import "HeaderView.h"
#import "ACFloatingTextField.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginVC ()<HeaderViewDelegate, GIDSignInDelegate, GIDSignInUIDelegate, HttpHelperDelegate, UITextFieldDelegate>
{
    BOOL isChecked;
}

@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_password;
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_email;
@property (strong, nonatomic) IBOutlet UIView *view_googlt;
@property (strong, nonatomic) IBOutlet UIView *view_facebool;
@property (strong, nonatomic) IBOutlet UIView *view_iconFB;
@property (strong, nonatomic) IBOutlet UIView *view_iconGP;
@property (strong, nonatomic) IBOutlet UIButton *btn_login;
@property (strong, nonatomic) IBOutlet UIButton *btn_skip;
@property (strong, nonatomic) IBOutlet UIView *view_email;
@property (strong, nonatomic) IBOutlet UIView *view_password;
@property (strong, nonatomic) IBOutlet UIImageView *img_logo;
@property (strong, nonatomic) IBOutlet UIView *view_logo;
@property (strong, nonatomic) IBOutlet UIView *view_google;
@property (strong, nonatomic) IBOutlet UIView *view_facebook;
@property (strong, nonatomic) IBOutlet UIView *view_inGoogle;
@property (strong, nonatomic) IBOutlet UIView *view_infacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;

@end

@implementation LoginVC

#pragma mark - HttpHelperDelegate
-(void)setBackApiResponse:(NSMutableDictionary *)response api_number:(int)api_number
{
    if([[response objectForKey:@"success"] boolValue])
    {
        if(api_number == registerUserAPI)
        {
            NSLog(@"response login : %@", response);
            [SVProgressHUD dismiss];
            [self redirectToHome];
            NSDictionary *dict = [[response objectForKey:@"user_details"] firstObject];
            NSLog(@"user_details Dict In LoginVC %@",dict);
            [[PlistHandler sharedObject] savePlistDataWithData:[self removeNullValuefromDictionary:dict]];
            [self performSelector:@selector(resp) withObject:nil afterDelay:1.0];
        }
    }
}
-(void)responseError:(NSError *)error api_number:(int)api_number
{
    [self showErrorWithStatus:@"Something Went Wrong, Check Internet"];
   
}
#pragma mark - life cycle
- (void)resp{
   NSLog(@"%@",[[PlistHandler sharedObject]getPlistData]);
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.view_header changeBackgroundColor:Bg_color];
    isChecked = YES;
    [self setCheckBox:isChecked];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"icon_menu"]];
    [self.view_header setTitle:@"Login"];
    self.view_header.delegate = self;
//    []
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.txt_password.inputAccessoryView = numberToolbar;
    self.txt_password.delegate = self;
    self.txt_email.delegate = self;
    [GIDSignInButton class];
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    [[viewModification sharedViews] changeViewtoCornerRadiusWithShadow:5 AndBorder:0.5 andBorderColor:[UIColor whiteColor] toView:self.view_googlt andShadowRadius:2.0 andShadowOpacity:0.8];
    [[viewModification sharedViews] changeViewtoCornerRadiusWithShadow:5 AndBorder:0.5 andBorderColor:[UIColor whiteColor] toView:self.view_facebool andShadowRadius:2.0 andShadowOpacity:0.8];
    [[viewModification sharedViews] changeViewtoCornerRadius:3 AndBorder:0 andBorderColor:[UIColor whiteColor] toView:self.view_iconFB];
    [[viewModification sharedViews] changeViewtoCornerRadius:3 AndBorder:0 andBorderColor:[UIColor whiteColor] toView:self.view_iconGP];
    [self setEmail];
    
//    [self.txt_password showErrorWithText:@"Password should not be empty"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[viewModification sharedViews] changeViewtoCornerRadiusInArray:[[NSArray alloc]initWithObjects:@"23",@"15",@"23",@"23",@"23",@"23",@"5", nil] AndBorder:0.5 andBorderColor:[UIColor darkGrayColor] toView:[[NSArray alloc]initWithObjects:self.btn_login,self.btn_skip,self.view_email,self.txt_email,self.view_password, self.txt_password,self.view_logo, nil]];

    [[viewModification sharedViews] custom:_view_facebook];
    [[viewModification sharedViews]custom:_view_google];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self setBackgroundGradient:self.btn_login color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];
    
    [self setBackgroundGradient:self.btn_skip color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];
    
    [self setBackgroundGradient:self.view_email color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];
    [self setBackgroundGradient:self.view_password color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];
    [self setBackgroundGradient:self.view_logo color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)btn_skip:(id)sender {
    [self redirectToHome];
}
- (IBAction)btnActionCheckBox:(UIButton *)sender {
    isChecked = !isChecked;
    [self setCheckBox:isChecked];
}

-(void)setCheckBox:(BOOL) ischeck{
    [self.btnCheckBox setImage:[self ipMaskedImageNamed:[UIImage imageNamed:(isChecked) ? @"checked" : @"unchecked"] color:UIColor.blackColor] forState:normal];
}

#pragma mark - Helper Method
-(void)setEmail{
    NSString *email = [[NSUserDefaults standardUserDefaults] stringForKey:@"rememberEmail"];
    self.txt_email.text = email;
}

#pragma mark - headerViewDeleagte
-(void)btnLeftClicked
{
    [self slideView];
}
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
}
#pragma mark - GIDSignInDelegate

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error) {
        NSLog(@"Status: Authentication error: %@", error);
        return;
    }else
    {
        NSDictionary *nameValuePairs=[NSDictionary dictionaryWithObjectsAndKeys:user.profile.email,@"email_id",@"",@"phoneNO",@"",@"gender",user.profile.givenName,@"u_firstname",user.profile.familyName,@"u_lastname", nil];
        httpHelper = [[HttpHelper alloc]initWithMUrl:registerUserURL Dictionary:nameValuePairs isloading:YES api_number:registerUserAPI message:@"Loading all backup"];
        httpHelper.delegate = self;
    }
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error) {
         NSLog(@"Status: Failed to disconnect: %@", error);
    } else {
         NSLog(@"Status: Disconnected");
    }
}

- (void)presentSignInViewController:(UIViewController *)viewController {
    [app.navController pushViewController:viewController animated:YES];
}
#pragma mark - IBACtion

- (IBAction)btnActionRememberMe:(UIButton *)sender {
    __weak typeof(self) weakSelf = self;
    [self.txt_email setTextWithEmailValidation:(ACFloatingTextField *)self.txt_email WithCompletionHandler:^(BOOL completed) {
        if(completed)
            if(isChecked){
                [[NSUserDefaults standardUserDefaults] setValue:weakSelf.txt_email.text forKey:@"rememberEmail"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
    }];
}

- (IBAction)custonFBLoginClick:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {

        if (error)
        {
            NSLog(@"Unexpected login error: %@", error);
            NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
            NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops";
            [[AlertControllerDesign sharedObject]alertController:alertTitle withMessage:alertMessage andButtonOne:@"OK" andButtontwo:@"" WithCompletionHandler:^(BOOL completed) {        }];
        }
        else
        {
            if(result.token)   // This means if There is current access token.
            {
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                                   parameters:@{@"fields": @"picture, name, email"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id userinfo, NSError *error) {
                     if (!error) {

                         dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                         dispatch_async(queue, ^(void) {

                             dispatch_async(dispatch_get_main_queue(), ^{
                                 NSLog(@"user info: %@", userinfo);
                                 NSLog(@"%@",userinfo[@"email"]);
                             });
                         });
                     }
                     else{
                         NSLog(@"%@", [error localizedDescription]);
                     }
                 }];
            }
            NSLog(@"Login Cancel");
        }
    }];
}
- (IBAction)btn_loginClicked:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self.txt_email setTextWithEmailValidation:(ACFloatingTextField *)self.txt_email WithCompletionHandler:^(BOOL completed) {
        if(completed)
            if(isChecked){
                [[NSUserDefaults standardUserDefaults] setValue:weakSelf.txt_email.text forKey:@"rememberEmail"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            [weakSelf registerApi:weakSelf.txt_email.text];
    }];
}

- (IBAction)btn_gmailLoginClicked:(id)sender {
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];
}

-(void)registerApi:(NSString *)email
{
    NSDictionary *nameValuePairs=[NSDictionary dictionaryWithObjectsAndKeys: email, @"email_id", @"", @"phoneNO", @"", @"gender", nil];
    httpHelper = [[HttpHelper alloc]initWithMUrl:registerUserURL Dictionary:nameValuePairs isloading:YES api_number:registerUserAPI message:@"Loading all backup"];
    httpHelper.delegate = self;
}
#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.txt_password){
        if(range.length + range.location > textField.text.length)
            return NO;
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 25;
    }
    return YES;
}
@end
