//
//  MoreAppVC.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 04/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "MoreAppVC.h"
#import "iCarousel.h"
#import "HeaderView.h"

@interface MoreAppVC ()<iCarouselDataSource, iCarouselDelegate, HeaderViewDelegate>
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (nonatomic, strong) NSMutableArray *items;
@property (strong, nonatomic) IBOutlet HeaderView *view_header;

@end

@implementation MoreAppVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.view_header changeBackgroundColor:Bg_color];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"icon_menu"]];
    _view_header.delegate = self;
    [_view_header setTitle:@"More App"];
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    [self setBackgroundGradient:self.carousel color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];

    self.items = [NSMutableArray array];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Pedometer+ Walk Weight Loss",@"name", pedoAppURL, @"url", @"PedometerTechApp.png", @"image", nil];
    [_items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Notes",@"name", notesAppURL, @"url", @"NotesTechApp.png", @"image", nil];
    [_items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Maths Learner",@"name", mathsAppURL, @"url", @"MathsLearnerTechApp.png", @"image", nil];
    [_items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Housie Number Picker",@"name", housieAppURL, @"url", @"HousieTechApp.png", @"image", nil];
    [_items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Water Tracker",@"name", waterAppURL, @"url", @"WaterTrackerTechApp.png", @"image", nil];
    [_items addObject:dict];
    
    dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Ball Hit-Smash",@"name", ballsHitAppURL, @"url", @"BallsHitTechApp.png", @"image", nil];
    [_items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Bollywood Movie Quiz", @"name",  @"BMQTechApp.png", @"image", bmqAppURL, @"url",nil];
    [_items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Bollywood Songs Quiz", @"name",  @"BSQTechApp.png", @"image", bsqAppURL, @"url",nil];
    [_items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Bollywood Celebrities", @"name",  @"HBCelebritiesTechApp.png", @"image", hbCelebrityAppURL, @"url",nil];
    [_items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Box Office Quiz", @"name",  @"BoxOfficeTechApp.png", @"image", boxOfficeAppURL, @"url",nil];
    [_items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Smart Contact Manager", @"name",  @"SCMTechApp.png", @"image", scmAppURL, @"url",nil];
    [_items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Contact Backup + Easy", @"name",  @"scmBackupTechApp.png", @"image", scmBackUpAppURL, @"url",nil];
    [_items addObject:dict];
    
    dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Contact Cleaner-Merge & Email", @"name",  @"ScanDuplicateTechApp.png", @"image", scanDuplicateAppURL, @"url",nil];
    [_items addObject:dict];
    // Do any additional setup after loading the view from its nib.
    _carousel.delegate = self;
    _carousel.dataSource = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    _carousel.type = iCarouselTypeCoverFlow2;
    //  _carousel.type=iCarouselTypeCylinder;
    [_carousel reloadData];
    [_carousel scrollToItemAtIndex:0 animated:true];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [_items count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    UIImageView *imageView=nil;
    //create new view if no view is available for recycling
    if (view == nil)
    {
        
        view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, FRAMEWIDTH-50, carousel.frame.size.height-35)];
        //  [view setBackgroundColor:[UIColor colorWithRed:46.0/255 green:25.0/255 blue:164.0/255 alpha:0.1]];
        view.layer.cornerRadius=20;
        view.layer.masksToBounds=true;
        view.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0.75 ];// colorWithRed:90.0/255.0 green:99.0/255.0 blue:97.0/255.0 alpha:0.5];
        //        [view setBackgroundColor:[UIColor lightTextColor]];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, carousel.frame.size.height*0.4, carousel.frame.size.height*0.4)];
        imageView.tag = 2;
        imageView.center=CGPointMake(view.center.x, view.center.y-40);
        imageView.layer.cornerRadius=10;
        imageView.layer.masksToBounds=true;
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, imageView.frame.origin.y+imageView.frame.size.height+10, 200, 50)];
        label.numberOfLines=0;
        label.center=CGPointMake(imageView.center.x, label.center.y);
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:20];
        label.textColor=[UIColor darkGrayColor];
        label.tag = 1;
        [view addSubview:label];
        UIButton *btnInstall=[[UIButton alloc]initWithFrame:CGRectMake(0, 40, carousel.frame.size.width-50, 45)];
        [btnInstall setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnInstall setBackgroundColor:[UIColor colorWithRed:76/255.0f green:180/255.0f blue:64/255.0f alpha:0.8]];
        [btnInstall setTitle:@"Install Now" forState:UIControlStateNormal];
        btnInstall.userInteractionEnabled=false;
        [btnInstall setCenter:CGPointMake(view.center.x, view.frame.size.height-40)];
        [view addSubview:imageView];
        [view addSubview:btnInstall];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
        imageView = (UIImageView *)[view viewWithTag:2];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    imageView.image = [UIImage imageNamed:[_items[index] objectForKey:@"image"]];
    label.text = [_items[index] objectForKey:@"name"];
    
    return view;
}
-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSString *url=[_items[index]objectForKey:@"url"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:^(BOOL success) {
        
    }];
    //  NSLog(@"URL %@",[);
}

-(void)btnLeftClicked
{
    [self slideView];
}

@end
