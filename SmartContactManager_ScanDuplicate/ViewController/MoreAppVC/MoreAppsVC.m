//
//  MoreAppsVC.m
//  Pedometer_Walk
//
//  Created by Ios_Team on 22/02/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

#import "MoreAppsVC.h"
#import "HeaderView.h"

@interface MoreAppsVC ()<FSPagerViewDataSource,FSPagerViewDelegate,HeaderViewDelegate>
{
    NSInteger seletedAppIndex;
    NSArray *appImagesArr;
    NSArray *appNamesArr;
    NSArray *appLinksArr;
}
//@property (strong, nonatomic) NSArray<NSString *> *imageNames;
//@property (strong, nonatomic) NSArray<NSString *> *appStoreLinks;
@property (assign, nonatomic) NSInteger typeIndex;
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@end

@implementation MoreAppsVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"icon_menu"]];
    _view_header.delegate = self;
    [_view_header setTitle:@"More App"];
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    [self setPagerViews];
}
-(void)setPagerViews{
    appNamesArr = [[NSArray alloc ] initWithObjects:@"Drink Water Reminder Tracker", @"Math Learner", @"Sticky Notes & To Do List", @"Housie Number Picker", @"Smart Contact Manager", @"Contact Backup + Transfer Easy", @"Contact Cleaner-Merge",
                   @"Bollywood Hollywood Celebrities",
                   @"Box Office Quiz",
                                   @"Bollywood Songs Quiz",@"Bollywood Movie Quiz",@"Birthday & Contact",@"Balls Hit", nil];
//    appNamesArr = @[@"Drink Water Reminder Tracker", @"Math Learner", @"Sticky Notes & To Do List", @"Housie Number Picker", @"Smart Contact Manager", @"Contact Backup + Transfer Easy", @"Contact Cleaner-Merge",
//    @"Bollywood Hollywood Celebrities",
//    @"Box Office Quiz",
//                    @"Bollywood Songs Quiz",@"Bollywood Movie Quiz",@"Birthday & Contact",@"Balls Hit"];
//     appImagesArr = @[@"App_WaterIcon.png", @"App_MathsIcon.png", @"App_NotesAppIcon.png", @"App_HNPIcon.png", @"App_SCMManagerIcon.png", @"App_SCMBackUpIcon.png", @"App_SCMDuplicateIcon.png",
//     @"App_HBCeleIcon.png",
//     @"App_BoxOfficeIcon.png",
//     @"App_BSQIcon.png",@"App_BMQIcon.png",@"App_BithdayIcon.png",@"App_BallsHitIcon.png"];
    appImagesArr = [[NSArray alloc] initWithObjects:@"App_WaterIcon.png", @"App_MathsIcon.png", @"App_NotesAppIcon.png", @"App_HNPIcon.png", @"App_SCMManagerIcon.png", @"App_SCMBackUpIcon.png", @"App_SCMDuplicateIcon.png",
    @"App_HBCeleIcon.png",
    @"App_BoxOfficeIcon.png",
    @"App_BSQIcon.png",@"App_BMQIcon.png",@"App_BithdayIcon.png",@"App_BallsHitIcon.png", nil];
    
//    appLinksArr = @[@"https://apps.apple.com/in/app/drink-water-reminders-tracker/id1453280308",
//                           @"https://apps.apple.com/in/app/math-learner-learning-maths/id1484812590",
//                           @"https://apps.apple.com/in/app/sticky-notes-to-do-list/id1534095481",
//                           @"https://apps.apple.com/in/app/housie-tambola-number-picker/id1448156725",
//                           @"https://apps.apple.com/in/app/contacts-backup-transfer-easy/id1032947867",
//                           @"https://apps.apple.com/in/app/smart-contact-manager/id1443756011",
//                           @"https://apps.apple.com/in/app/contact-cleaner-merge-email/id1444870653",
//                           @"https://apps.apple.com/in/app/bollywood-hollywood-star-quiz/id680460746",
//                           @"App_BoxOfficeIcon.png",
//                           @"https://apps.apple.com/in/app/bollywood-songs-quiz/id695445842",
//                           @"https://apps.apple.com/in/app/bollywood-movies-quiz-guess-so/id708263591",
//                           @"https://apps.apple.com/in/app/birthday-contact-backup/id827642824",
//                           @"https://apps.apple.com/in/app/balls-hit-smash/id1083462882g"];
    
    appLinksArr = [[NSArray alloc ] initWithObjects:@"https://apps.apple.com/in/app/drink-water-reminders-tracker/id1453280308",
                   @"https://apps.apple.com/in/app/math-learner-learning-maths/id1484812590",
                   @"https://apps.apple.com/in/app/sticky-notes-to-do-list/id1534095481",
                   @"https://apps.apple.com/in/app/housie-tambola-number-picker/id1448156725",
                   @"https://apps.apple.com/in/app/contacts-backup-transfer-easy/id1032947867",
                   @"https://apps.apple.com/in/app/smart-contact-manager/id1443756011",
                   @"https://apps.apple.com/in/app/contact-cleaner-merge-email/id1444870653",
                   @"https://apps.apple.com/in/app/bollywood-hollywood-star-quiz/id680460746",
                   @"App_BoxOfficeIcon.png",
                   @"https://apps.apple.com/in/app/bollywood-songs-quiz/id695445842",
                   @"https://apps.apple.com/in/app/bollywood-movies-quiz-guess-so/id708263591",
                   @"https://apps.apple.com/in/app/birthday-contact-backup/id827642824",
                   @"https://apps.apple.com/in/app/balls-hit-smash/id1083462882g", nil];
    
    self.pagerView.transformer = [[FSPagerViewTransformer alloc] initWithType:FSPagerViewTransformerTypeCoverFlow];
//    self.pagerView.itemSize = CGSizeMake(120, 120);
    self.pagerView.itemSize = CGSizeMake(self.pagerView.frame.size.height*0.75, self.pagerView.frame.size.height*0.75);
   
    self.pagerView.decelerationDistance = FSPagerViewAutomaticDistance;
       [self.pagerView registerClass:[FSPagerViewCell class] forCellWithReuseIdentifier:@"cell"];
       self.pagerView.isInfinite = YES;
       self.typeIndex = 0;
    NSLog(@"%@",appLinksArr);
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.typeIndex = self.typeIndex;
}
#pragma mark - Button Action

- (IBAction)btn_BackPress:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - FSPagerViewDataSource

- (NSInteger)numberOfItemsInPagerView:(FSPagerView *)pagerView
{
    return [appLinksArr count];
}

- (FSPagerViewCell *)pagerView:(FSPagerView *)pagerView cellForItemAtIndex:(NSInteger)index
{
    FSPagerViewCell * cell = [pagerView dequeueReusableCellWithReuseIdentifier:@"cell" atIndex:index];
    cell.imageView.image = [UIImage imageNamed:appImagesArr[index]];
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    cell.imageView.layer.cornerRadius = 8.0;
    cell.imageView.clipsToBounds = YES;
    cell.textLabel.text = appNamesArr[index];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.font = [UIFont fontWithName:@"Noteworthy-Bold" size:18.0];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}

#pragma mark - FSPagerViewDelegate
- (void)pagerViewDidScroll:(FSPagerView *)pagerView{
    seletedAppIndex = pagerView.currentIndex;
}
- (void)pagerView:(FSPagerView *)pagerView didSelectItemAtIndex:(NSInteger)index
{
    [pagerView deselectItemAtIndex:index animated:YES];
    [pagerView scrollToItemAtIndex:index animated:YES];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[appLinksArr objectAtIndex:index]] options:@{} completionHandler:nil];
}

-(void)btnLeftClicked
{
    [self slideView];
}

@end
