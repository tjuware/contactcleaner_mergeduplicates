//
//  MoreAppsVC.h
//  Pedometer_Walk
//
//  Created by Ios_Team on 22/02/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSPagerViewObjcCompat.h"
@import FSPagerView;
#import "MasterVC.h"

@interface MoreAppsVC : MasterVC

@property (strong, nonatomic) IBOutlet FSPagerView *pagerView;

@end
