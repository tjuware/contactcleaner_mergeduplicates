//
//  PopUpVC.h
//  SmartContactManager_ScanDuplicate
//
//  Created by 1_Somnath on 11/12/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol PopUpVCDelegate<NSObject>

-(void)didSucess :(NSDictionary *)dict;

@end
@interface PopUpVC : UIViewController
@property (strong, nonatomic) NSDictionary *info_dict;
@property(weak,nonatomic) id <PopUpVCDelegate> popUpDelegate;
@end

NS_ASSUME_NONNULL_END
