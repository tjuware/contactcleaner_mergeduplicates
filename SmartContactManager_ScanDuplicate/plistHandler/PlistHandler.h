//
//  PlistHandler.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 07/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlistHandler : NSObject
@property(nonatomic,retain)NSMutableDictionary *pList;

+ (id) sharedObject;
-(void) savePlistDataWithData:(NSDictionary *)userdata;
  -(NSMutableDictionary *) getPlistData;
-(void) removeDataFromPlist;
-(BOOL)isUserLogin;
-(void)setBackupCount:(NSInteger)count;
-(NSInteger)getBackupCount;
-(NSString *)getEmailID;
-(NSString *)getUserID;
-(NSString *)getValueFromPlistByKey:(NSString *)key;
-(void)setValueFromPlistByvalue:(NSString *)value andKey:(NSString *)key;

@end
