//
//  MasterView.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 08/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "viewModification.h"
#import "AppDelegate.h"
#import "Toast+UIView.h"
#import "MasterView.h"
#define FRAMEWIDTH [[UIScreen mainScreen] bounds].size.width
#define FRAMEHEIGHT [[UIScreen mainScreen] bounds].size.height
@interface MasterView : UIView
{
    AppDelegate *app;
}
@property (nonatomic, strong) UIViewController *parentVC;
@end
