//
//  MasterVC.m
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 11/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "MasterVC.h"
#import "ViewController.h"
#import "AddContact.h"
#import "ContactMemberList.h"
#import "MoreAppVC.h"
#import "LoginVC.h"
#import "SettingVC.h"
#import "InfoVC.h"
#import "BackUpHistory.h"
#import "PreviewContactVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "MoreAppsVC.h"
@interface MasterVC ()<SliderMenuDelegate, HttpHelperDelegate, MFMailComposeViewControllerDelegate, GADInterstitialDelegate, GADBannerViewDelegate>
{
    UIViewController *previousVC;
    UIView *_activityView;
    UIActivityIndicatorView *_indicator;
}
@property (strong, nonatomic)SliderMenuView *sliderview;
@property(nonatomic, strong) GADBannerView *bannerView;
@end

@implementation MasterVC
-(void)setBackApiResponse:(NSMutableDictionary *)response api_number:(int)api_number
{
    if([[response objectForKey:@"success"] boolValue])
    {
        //        if(api_number == registerUserAPI || api_number == savePasswordAPI)
        //        {
        //            NSDictionary *dict = [[response objectForKey:@"user_details"] firstObject];
        //            [[PlistHandler sharedObject] savePlistDataWithData:[self removeNullValuefromDictionary:dict]];
        //        }
        if(api_number == registerUserAPI || api_number == savePasswordAPI)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
            NSDictionary *dict = [[response objectForKey:@"user_details"] firstObject];
             NSLog(@"user_details Dict In MasterVC %@",dict);
            [[PlistHandler sharedObject] savePlistDataWithData:[self removeNullValuefromDictionary:dict]];
            NSString *pass = [dict objectForKey:@"password"];
            if(api_number != savePasswordAPI){
                if(pass.length <= 0 || pass == nil)
                {
                    [popup viewWithPassword: [dict objectForKey:@"registered_email"]];
                }
            }
            
            [self btn_close:1];
        }else if(api_number == addRewardAPI){
            NSLog(@"%@", response);
        }
    }
}

-(void)responseError:(NSError *)error api_number:(int)api_number
{
    [self showErrorWithStatus:@"Something Went Wrong, Check Internet"];
    
}
- (void)setServerTimeOut:(int)api_number{
    [self showErrorWithStatus:@"Something Went Wrong, Check Internet"];
}
-(void)showErrorWithStatus:(NSString*)string{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showErrorWithStatus:string];
        [SVProgressHUD dismissWithDelay:2.0];
        
    });
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.sliderview removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [viewModification sharedViews];
    [SVProgressHUD setMinimumDismissTimeInterval:2.0];
    [SVProgressHUD setMaximumDismissTimeInterval:2.5];
    self.sliderview = [[SliderMenuView alloc]initWithFrame:CGRectMake(-500, FRAMEHEIGHT/10, FRAMEWIDTH, FRAMEHEIGHT -((FRAMEHEIGHT/10)*1))];
    self.sliderview.delegate = self;
    [self.view insertSubview:self.sliderview atIndex:[self.view subviews].count + 1];
    previousVC = (app.navController.viewControllers.count >= 2) ? [app.navController.viewControllers objectAtIndex:[app.navController.viewControllers count]-2] : nil;
    arr_color = [[NSArray alloc]initWithObjects:[UIColor blackColor], UIColor.blueColor, UIColor.brownColor, UIColor.cyanColor, UIColor.greenColor, UIColor.magentaColor, UIColor.orangeColor,UIColor.purpleColor, UIColor.redColor, UIColor.yellowColor,UIColor.grayColor, nil];
    dict_plist = [[PlistHandler sharedObject]getPlistData];
}
-(void)secondMethod
{
    self.sliderview = [[SliderMenuView alloc]initWithFrame:CGRectMake(-500, FRAMEHEIGHT/10, FRAMEWIDTH, FRAMEHEIGHT -((FRAMEHEIGHT/10)*1))];
    self.sliderview.delegate = self;
    [self.view insertSubview:self.sliderview atIndex:[self.view subviews].count + 1];
  
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if(dict_plist == nil || dict_plist.count == 0)
        dict_plist = [[PlistHandler sharedObject]getPlistData];

    email = [dict_plist objectForKey:@"registered_email"];
    
    if(!app.navController.navigationBar.hidden)
        [app.navController setNavigationBarHidden:YES animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
+ (id)sharedInstance { //Shared instance method
    
    static viewModification *sharedMyViews = nil; //create contactsList Object
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //for first time create shared instance object
        sharedMyViews = [[self alloc] init];
    });
    
    return sharedMyViews;
}
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - admob

-(void)returnbannerView :(UIView *)adsview :(UIViewController *)VC
{
    _bannerView = [[GADBannerView alloc]
                   initWithAdSize:kGADAdSizeBanner];
    _bannerView.delegate = VC;
    
//        self.bannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716"; // demo
    self.bannerView.adUnitID = @"ca-app-pub-5601451829724206/3418015423";// live
    self.bannerView.rootViewController = VC;
    GADRequest *request = [GADRequest request];
//    request.testDevices = @[ @"b97567422af9f5420f8a24f761586017" ];
     [adsview addSubview:self.bannerView];
    [self.bannerView loadRequest:request];

}
- (GADInterstitial *)createAndLoadInterstitiala {
//             GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-3940256099942544/4411468910"]; // demo

   GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-5601451829724206/6937801495"]; // live
    interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
//    request.testDevices = @[ @"b97567422af9f5420f8a24f761586017" ];
    [interstitial loadRequest:request];
    return interstitial;
}

-(void)doSomethingInterstitial
{
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    } else {
        NSLog(@"Ad wasn't ready");
    }
}

//- (void)createAndLoadInterstitial:(UIViewController *)views {
//    self.interstitial =
//    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-3940256099942544/4411468910"];
//    
//    GADRequest *request = [GADRequest request];
//    // Request test ads on devices you specify. Your test device ID is printed to the console when
//    //    // an ad request is made.
//    //    request.testDevices = @[ kGADSimulatorID, @"2077ef9a63d2b398840261c8221a0c9a" ];
//    [self.interstitial loadRequest:request];
//    [self.interstitial presentFromRootViewController:views];
//}
#pragma mark - slideview
-(void)slideView
{
//    if(![[PlistHandler sharedObject] isUserLogin]){
//    [self.sliderview hideEmail];
//    }else{
//        [self.sliderview setEmail:[NSString stringWithFormat:@"%@", [dict_plist objectForKey:@"registered_email"]]];
//    }
    [self sliderMenuList];
    if(self.sliderview != nil)
    {
        if(_sliderview.frame.origin.x < 0)
            [self showSideMenu];
        else
            [self.sliderview hideMenu];
    }
    else
        [self showSideMenu];
}
-(void)showSideMenu{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.sliderview = [[SliderMenuView alloc] initWithFrame:CGRectMake(-500, FRAMEHEIGHT/10, FRAMEWIDTH, FRAMEHEIGHT -((FRAMEHEIGHT/10)*1))];
        self.sliderview.delegate =self;
        [self.sliderview setoption:[self menuList]];
        NSLog(@"self.slider  ... %@", self.sliderview.array_title);
        //[[NSMutableArray alloc]initWithObjects:@"Home", @"Backup History", @"Add Contact", @"Edit Contact",  @"Delete contact", @"Recovery", @"More App", @"Setting", @"Rate us",  @"Info", nil];
        //        self.sliderview.array_icon = [[NSMutableArray alloc]initWithObjects:@"icon_homeW", @"icon_historyW", @"icon_addContactW", @"icon_editContactW", @"icon_deleteContactW", @"icon_recoveryW", @"icon_moreAppW", @"icon_settingW",@"icon_ratestar", @"icon_infoW", nil];
        //        if(![[PlistHandler sharedObject] isUserLogin]){
        //            [self.sliderview.array_title addObject:@"Login"];
        //            [self.sliderview.array_icon addObject:@"login"];
        //        }
        [self.sliderview.view_top setBackgroundColor:Bg_color];
        [self.view insertSubview:self.sliderview atIndex:[self.view subviews].count + 1];
        [self.sliderview.tableview reloadData];
        [UIView animateWithDuration:0.3f animations:^{
            [self.sliderview setFrame:CGRectMake(0, FRAMEHEIGHT/10, FRAMEWIDTH, FRAMEHEIGHT -((FRAMEHEIGHT/10)*1))];
        }];
    });
}

-(void)sliderMenuList
{
    self.sliderview.array_title = [self menuList];/*[[NSMutableArray alloc]initWithObjects:@"Home", @"Backup History", @"Add Contact", @"Edit Contact",  @"Delete contact", @"Recovery", @"More App", @"Setting", @"Info", nil];
    self.sliderview.array_icon = [[NSMutableArray alloc]initWithObjects:@"icon_homeW", @"icon_historyW", @"icon_addContactW", @"icon_editContactW", @"icon_deleteContactW", @"icon_recoveryW", @"icon_more", @"icon_settingW", @"icon_infoW", nil];
    if(![[PlistHandler sharedObject] isUserLogin]){
        [self.sliderview.array_title addObject:@"Login"];
        [self.sliderview.array_icon addObject:@"icon_loginW"];
    }
                                                   */
}

-(NSMutableArray *)menuList
{
    NSMutableArray *arr_list , *arr_sub= [[NSMutableArray alloc]init];
    NSMutableDictionary *dict , *subIndex = [[NSMutableDictionary alloc]init];
    arr_list= [[NSMutableArray alloc]init];;
    //1 Home
    dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"Home" forKey:@"Name"];
    [dict setObject:@"icon_homeW" forKey:@"Image"];
    [dict setObject:@"1" forKey:@"id"];
    [dict setObject:@"0" forKey:@"isSelect"];
    [arr_list addObject:dict];
    
    //2 Backup History
    dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"Backup History" forKey:@"Name"];
    [dict setObject:@"icon_historyW" forKey:@"Image"];
    [dict setObject:@"2" forKey:@"id"];
    [dict setObject:@"0" forKey:@"isSelect"];
    [arr_list addObject:dict];
    
    //3 Services
    dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"Contacts" forKey:@"Name"];
    [dict setObject:@"icon_addContactW" forKey:@"Image"];
    [dict setObject:@"3" forKey:@"id"];
    [dict setObject:@"0" forKey:@"isSelect"];
    
    //4 Services
    [subIndex setObject:@"Add Contact" forKey:@"Name"];
    [subIndex setObject:@"icon_addContactW" forKey:@"Image"];
    [subIndex setObject:@"4" forKey:@"id"];
    [subIndex setObject:@"0" forKey:@"isSelect"];
    [arr_sub addObject:subIndex];
    
    //4 Services
    subIndex = [[NSMutableDictionary alloc]init];
    [subIndex setObject:@"Edit Contact" forKey:@"Name"];
    [subIndex setObject:@"icon_editContactW" forKey:@"Image"];
    [subIndex setObject:@"5" forKey:@"id"];
    [subIndex setObject:@"0" forKey:@"isSelect"];
    [arr_sub addObject:subIndex];
    
    //4 Services
    subIndex = [[NSMutableDictionary alloc]init];
    [subIndex setObject:@"Delete contact" forKey:@"Name"];
    [subIndex setObject:@"icon_deleteContactW" forKey:@"Image"];
    [subIndex setObject:@"6" forKey:@"id"];
    [subIndex setObject:@"0" forKey:@"isSelect"];
    [arr_sub addObject:subIndex];
    [dict setObject:arr_sub forKey:@"SubIndex"];
//    [arr_list addObject:dict];
    
    //7 Recovery
//    dict = [[NSMutableDictionary alloc]init];
//    [dict setObject:@"Recovery" forKey:@"Name"];
//    [dict setObject:@"icon_recoveryW" forKey:@"Image"];
//    [dict setObject:@"7" forKey:@"id"];
//    [dict setObject:@"0" forKey:@"isSelect"];
//    [arr_list addObject:dict];
    subIndex = [[NSMutableDictionary alloc]init];
    [subIndex setObject:@"Recovery" forKey:@"Name"];
    [subIndex setObject:@"icon_recoveryW" forKey:@"Image"];
    [subIndex setObject:@"7" forKey:@"id"];
    [subIndex setObject:@"0" forKey:@"isSelect"];
    [arr_sub addObject:subIndex];
    [dict setObject:arr_sub forKey:@"SubIndex"];
    [arr_list addObject:dict];
    
    //8 Recovery
    dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"More App" forKey:@"Name"];
    [dict setObject:@"icon_more" forKey:@"Image"];
    [dict setObject:@"8" forKey:@"id"];
    [dict setObject:@"0" forKey:@"isSelect"];
    [arr_list addObject:dict];
    
    //9 Settings
    dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"Setting" forKey:@"Name"];
    [dict setObject:@"icon_settingW" forKey:@"Image"];
    [dict setObject:@"9" forKey:@"id"];
    [dict setObject:@"0" forKey:@"isSelect"];
    [arr_list addObject:dict];
    
    //10 Rate US
    dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"Rate us" forKey:@"Name"];
    [dict setObject:@"icon_ratestar" forKey:@"Image"];
    [dict setObject:@"10" forKey:@"id"];
    [dict setObject:@"0" forKey:@"isSelect"];
    [arr_list addObject:dict];
    
    //11 FeedBack
    dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"LetsDo Bussiness" forKey:@"Name"];
    [dict setObject:@"icon_feedBackW" forKey:@"Image"];
    [dict setObject:@"11" forKey:@"id"];
    [dict setObject:@"0" forKey:@"isSelect"];
    [arr_list addObject:dict];
    
    //12 Logout
    if(![[PlistHandler sharedObject] isUserLogin]){
        dict = [[NSMutableDictionary alloc]init];
        [dict setObject:@"Login" forKey:@"Name"];
        [dict setObject:@"login" forKey:@"Image"];
        [dict setObject:@"12" forKey:@"id"];
        [dict setObject:@"0" forKey:@"isSelect"];
        [arr_list addObject:dict];
    }else{
        dict = [[NSMutableDictionary alloc]init];
        [dict setObject:@"Logout" forKey:@"Name"];
        [dict setObject:@"icon_pReset" forKey:@"Image"];
        [dict setObject:@"13" forKey:@"id"];
        [dict setObject:@"0" forKey:@"isSelect"];
        [arr_list addObject:dict];
    }
    return arr_list;
}
#pragma mark - SliderViewDelegate
-(void)handleMenuSelection:(NSInteger)tag
{
    [SVProgressHUD dismiss];
    [self.sliderview hideMenu];
    NSTimeInterval interval = 0.3;
    switch (tag) {
        case 1://home
        {
            [self performSelector:@selector(redirectToHome) withObject:nil afterDelay:interval];
        }
            break;
        case 2://backup history
        {
            [self performSelector:@selector(redirectToBackupHistory) withObject:nil afterDelay:interval];
        }
            break;
        case 4://add contact
        {
            [self performSelector:@selector(redirectToAddContact) withObject:nil afterDelay:interval];
        }
            break;
        case 5://edit contact
        {
            [self performSelector:@selector(redirectToEditContact:) withObject:@"Edit Contact" afterDelay:interval];
        }
            break;
        case 6://delete contact
        {
            [self performSelector:@selector(redirectToEditContact:) withObject:@"Delete Contact" afterDelay:interval];
        }
            break;
        case 7://recovery
        {
            SettingVC *morevc = [[SettingVC alloc]init];
            morevc.isForSetting = NO;
            [app.navController pushViewController:morevc animated:YES];
        }
            break;
        case 8://more app
        {
//            MoreAppVC *morevc = [[MoreAppVC alloc]init];
//            [app.navController pushViewController:morevc animated:YES];
            MoreAppsVC *morevc = [[MoreAppsVC alloc]init];
            [app.navController pushViewController:morevc animated:YES];
        }
            break;
        case 9:// setting
        {
            //            [self showShareView];
            //            [self performSelector:@selector(showShareView) withObject:nil afterDelay:interval];
            SettingVC *morevc = [[SettingVC alloc]init];
            morevc.isForSetting = YES;
            [app.navController pushViewController:morevc animated:YES];
            //            [[PlistHandler sharedObject]removeDataFromPlist];
            //            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
            //            content.contentURL = [NSURL URLWithString:@"https://itunes.apple.com/us/app/id1032947867"];
            //            [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
        }
            break;
        case 10:// rateus
        {
            NSString *reviewURL = @"https://itunes.apple.com/us/app/id1444870653";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL] options:@{} completionHandler:^(BOOL success) {
                
            }];
        }
            break;
            case 11:
        {
            if([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
                mailCont.mailComposeDelegate = self;
                // NSString *url=@"https://itunes.apple.com/us/app/id1444870653";
                [mailCont setSubject:[NSString stringWithFormat:@"FeedBack - from %@",[[PlistHandler sharedObject]getValueFromPlistByKey:@"registered_email"]]];
                [mailCont setToRecipients:[NSArray arrayWithObject:@"kush.thakkar@techathalon.com"]];
//                [mailCont setMessageBody:[NSString stringWithFormat:@"We are specialized into Web Development and Mobile development (IOS & ANDROID)"]isHTML:NO];
                [mailCont setMessageBody:[NSString stringWithFormat:@"We are specialized into Web Development and Mobile development"]isHTML:NO];
                
                [app.navController presentViewController:mailCont animated:YES completion:nil];
            }
        }
            break;
//        {
//            InfoVC *info = [[InfoVC alloc]init];
//            [app.navController pushViewController:info animated:YES];
//        }
//            break;
        case 12:// login
        {
            LoginVC *login = [[LoginVC alloc]init];
            [app.navController pushViewController:login animated:YES];
        }
            break;
        case 13:// logout
        { // "Reset"
            [[AlertControllerDesign sharedObject] alertControllerForLogoutWithCompletionHandler:^(BOOL completed) {
                if(completed){
                    [self.view makeToast:@"Account Reset Successfully"];
                    [[PlistHandler sharedObject]removeDataFromPlist];
                    [self performSelector: @selector (handleMenuSelection:) withObject:0 afterDelay:0.5];
                    [self redirectToHome];
                }
            }];
        }
            break;
        default:
            break;
    }
}
#pragma mark - shareviewDelegate
-(void)shareSuccess{
    NSLog(@"share suceess");
}

-(void)shareFailed{
    NSLog(@"shareFailed");
    
}

-(void)showShareView{
    dispatch_async(dispatch_get_main_queue(), ^{
        CGFloat width = FRAMEWIDTH * 0.90;
        CGFloat height = FRAMEHEIGHT * 0.25;
        UIView *view = [[UIView alloc]initWithFrame:self.view.frame];
        view.backgroundColor = [UIColor clearColor];
        view.tag = 143;
        [self.view addSubview:view];
        self.shareview = [[ShareDialogView alloc] initWithFrame:CGRectMake((FRAMEWIDTH - width)/2, -250, width, height)];
        self.shareview.parentView =view;
        self.shareview.parentVC = self;
        self.shareview.delegate = self;
        NSLog(@"sharevue %@", self.shareview.delegate);
        [self.shareview showSideMenu:self.shareview];
    });
}
#pragma mark - helper method
-(void)startActivityIndicator:(NSString *)message withCompletion:(void(^)(BOOL completed))completionHandler
{
    _activityView=[[UIView alloc]initWithFrame:CGRectMake(0,  0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
    _activityView.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];;
    _activityView.alpha=0.8;
    UIView *childFirst=[[UIView alloc]initWithFrame:CGRectMake(80, 210, 170, 130)];
    
    childFirst.layer.cornerRadius=9;
    childFirst.layer.masksToBounds=true;
    childFirst.backgroundColor=[UIColor whiteColor];
    
    UIView *childView=[[UIView alloc]initWithFrame:CGRectMake(1, 1, childFirst.frame.size.width-2, childFirst.frame.size.height-2)];
    childFirst.center=CGPointMake(self.view.center.x, self.view.center.y);
    
    // _activityView.center=self.view.center;
    childView.layer.cornerRadius=9;
    childView.layer.masksToBounds=true;
    childView.backgroundColor=[UIColor blackColor];
    childView.alpha=1;
    
    _indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(8,5, 150, 150)];
    _indicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhiteLarge;
    _indicator.color = [UIColor whiteColor];
    _indicator.hidden=false;
    
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(20 , 20, 150, 30)];
    label.font = [UIFont fontWithName:@"TrebuchetMS" size:20];
    label.text=message;
    label.textAlignment= NSTextAlignmentCenter;
    label.tag = 7368;
    label.textColor=[UIColor whiteColor];
    
    [childView addSubview:_indicator];
    [childView addSubview:label];
    [childFirst addSubview:childView];
    [_activityView addSubview:childFirst];
    [_indicator startAnimating];
    [self.view addSubview:_activityView];
    completionHandler(YES);
}

-(void)stopActivityIndicator
{
    [_indicator stopAnimating];
    [_activityView removeFromSuperview];
}
-(void)updatePlistDictioryByKey:(NSString *)key andValue:(NSString *)value
{
    [[PlistHandler sharedObject] setValueFromPlistByvalue:value andKey:key];
    dict_plist = [[PlistHandler sharedObject]getPlistData];
    NSLog(@"plistdata %@", dict_plist);
}

-(void)redirectToHome
{
    if(![app.navController.topViewController isKindOfClass:[ViewController class]])
    {
        if([previousVC isKindOfClass:[ViewController class]])
            [app.navController popViewControllerAnimated:YES];
        else{
            [app.navController popToRootViewControllerAnimated:YES];
        }
    }
}
-(void)redirectToAddContact
{
    AddContact *bcakup = [[AddContact alloc]init];
    [app.navController pushViewController:bcakup animated:YES];
}
-(void)redirectToEditContact:(NSString *)fromPage
{
    ContactMemberList *newConvVC = [[ContactMemberList alloc] initWithNibName:@"ContactMemberList" bundle:nil];
    newConvVC.arr_contact = [[ContactList sharedContacts]totalPhoneNumberArray];
    newConvVC.pageCameFrom = fromPage;
    [app.navController pushViewController:newConvVC animated:YES];
}
-(NSString *)handleNullValueOfString:(NSString *)value
{
    value = (value == (id)[NSNull null] || value.length == 0 || [value isEqualToString:@"(null)"]) ?@"":value;
    value = [value stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    return value;
}
-(NSDictionary *)removeNullValuefromDictionary:(NSDictionary *)dict
{
    NSMutableDictionary *dictM = [dict mutableCopy];
    for(NSString *key in [dictM allKeys]){
        NSString *value = [self handleNullValueOfString:[dictM objectForKey:key]];
        if(value.length == 0)
            [dictM setObject:value forKey:key];
    }
    return [dictM copy];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)loginAlertController
{
    [[AlertControllerDesign sharedObject]alertControllerForLoginWithCompletionHandler:^(NSMutableDictionary *dict, BOOL completed) {
        if(completed){
            self->httpHelper = [[HttpHelper alloc]initWithMUrl:registerUserURL Dictionary:[dict copy] isloading:YES api_number:registerUserAPI message:@"Loading all backup"];
            self->httpHelper.delegate = self;
        }
    }];
}
-(void)redirectToBackupHistory
{
    BackUpHistory *bcakup = [[BackUpHistory alloc]init];
    [app.navController pushViewController:bcakup animated:YES];
}
-(void)redirectToPreviewContactVC:(NSString *)filename
{
    PreviewContactVC *preview = [[PreviewContactVC alloc]init];
    preview.filename = filename;
    [app.navController pushViewController:preview animated:YES];
}
-(void)updateCheckPasswordAPI :(NSString *)password WithCompletionHandler:(void(^)(NSMutableDictionary *dict, BOOL completed))completionHandler
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSURL *url = [NSURL URLWithString:checkPasswordURL];
    [request setURL:url];
    request.timeoutInterval = 60.0f;
    [request setHTTPMethod:@"POST"];
    
    NSString *email = [self handleNullValueOfString:[[PlistHandler sharedObject]getValueFromPlistByKey:@"registered_email"]];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"registered_email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[email dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[password dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 60.0;
    sessionConfig.timeoutIntervalForResource = 60.0;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error)
        {
            if(data != nil)
            {
                NSString * dataInString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"dataInString %@", dataInString);
                NSMutableDictionary *editInfo=(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSLog(@" update loaction... %@",editInfo);
                NSLog(@"^location is updated");
                completionHandler(editInfo, YES);
            }else
            {
                NSLog(@"error %@", error.localizedDescription);
                completionHandler(nil, NO);
            }
        }else{
            NSLog(@"error %@", error.localizedDescription);
            completionHandler(nil, NO);
        }
    }]resume];
}
-(NSArray *)downloadVCFandDisplay:(NSString *)filename
{
    NSString *_vCardString;
    
    NSString *path = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:filename];
    //_vCardString = [path lastPathComponent];
    NSLog(@"path : %@",path);
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    NSURL *url = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"%@%@",uploadPath,filename]];
    _vCardString = @"";
    if(!fileExists) {
        
        _vCardString = [[NSString alloc]initWithContentsOfURL:url encoding:NSStringEncodingConversionAllowLossy error:nil];
        if(_vCardString==nil)
        {
            [[AlertControllerDesign sharedObject] alertController:@"Some error occurred while retriving file please try again" withMessage:@"" andButtonOne:@"Do Later" andButtontwo:@"" WithCompletionHandler:^(BOOL completed) {
                if(completed)
                    [self dismissViewControllerAnimated:YES completion:nil];
            }];
        }
        NSLog(@"vCardString : %@",_vCardString);
    } else {
        _vCardString = [[NSString alloc]initWithContentsOfFile:path encoding:NSStringEncodingConversionAllowLossy error:nil];
        if(_vCardString==nil)
        {
            [[AlertControllerDesign sharedObject] alertController:@"Some error occurred while retriving file please try again" withMessage:@"" andButtonOne:@"Do Later" andButtontwo:@"" WithCompletionHandler:^(BOOL completed) {
                if(completed)
                    [self dismissViewControllerAnimated:YES completion:nil];
            }];
        }
        
        NSLog(@"vCardString : %@",_vCardString);
    }
    NSArray * vcfarray  = [_vCardString componentsSeparatedByString:@"END:VCARD"];
    
    
    NSMutableArray *arrTable = [[vcfarray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
    [arrTable enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *newstr = [self handleNullValueOfString:obj];
        newstr = [newstr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if(newstr.length == 0)
            [arrTable removeObjectAtIndex:idx];
    }];
    return [arrTable copy];
}

- (void)setBackgroundGradient:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha
{
    
    [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.bounds;

    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:colorR1/255.0 green:colorG1/255.0 blue:colorB1/255.0 alpha:alpha] CGColor], (id)[[UIColor colorWithRed:colorR2/255.0 green:colorG2/255.0 blue:colorB2/255.0 alpha:alpha] CGColor], nil];
    grad.startPoint = CGPointMake(0.0, 0.5);
    grad.endPoint = CGPointMake(1.0, 0.5);
    [mainView.layer insertSublayer:grad atIndex:0];
    [mainView setClipsToBounds:YES];
}

- (void)setBackgroundGradient:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha withName:(NSString *)name
{
    
    [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.bounds;
    grad.name = name;
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:colorR1/255.0 green:colorG1/255.0 blue:colorB1/255.0 alpha:alpha] CGColor], (id)[[UIColor colorWithRed:colorR2/255.0 green:colorG2/255.0 blue:colorB2/255.0 alpha:alpha] CGColor], nil];
    grad.startPoint = CGPointMake(0.0, 0.5);
    grad.endPoint = CGPointMake(1.0, 0.5);
    [mainView.layer insertSublayer:grad atIndex:0];
    [mainView setClipsToBounds:YES];
}
-(void)shadowHeaderView:(UIView *)view
{
    [[viewModification sharedViews] setBackgroundGradient:view color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];
}
-(void)shadowHeaderViewWithAplha:(UIView *)view
{
    [[viewModification sharedViews] setBackgroundGradient:view color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:0.70];
}

#pragma mark - GIDSignInDelegate

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error) {
        NSLog(@"Status: Authentication error: %@", error);
        return;
    }else
    {
        NSDictionary *nameValuePairs=[NSDictionary dictionaryWithObjectsAndKeys:user.profile.email,@"email_id",@"",@"password",@"",@"phoneNO",@"",@"gender",user.profile.givenName,@"u_firstname",user.profile.familyName,@"u_lastname", nil];
        httpHelper = [[HttpHelper alloc]initWithMUrl:registerUserURL Dictionary:nameValuePairs isloading:YES api_number:registerUserAPI message:@"Loading all backup"];
        httpHelper.delegate= self;
    }
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error) {
        NSLog(@"Status: Failed to disconnect: %@", error);
    } else {
        NSLog(@"Status: Disconnected");
    }
}

- (void)presentSignInViewController:(UIViewController *)viewController {
    [app.navController pushViewController:viewController animated:YES];
}

-(void)googleLogin
{
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];
}
#pragma mark - Login Pop up

-(void)showLoginPopUp
{
    if(popup == nil){
        [self.view makeToast:@"Rescaning...."];
        UIView *view = [[UIView alloc]initWithFrame:self.view.frame];
        view.tag = 123;
        [view setBackgroundColor:[[UIColor blackColor]colorWithAlphaComponent:0.25]];
        popup = [[LoginPopUpView alloc]initWithFrame:CGRectMake(self.view.center.x - 150, self.view.center.y - 187, 300, 272)];
        popup.parentVC = self;
        [popup.layer setCornerRadius:20];
        [popup viewWithOutPassword];
        [popup setClipsToBounds:YES];
        [popup setDelegate:self];
        
        [view addSubview:popup];
        [self.view addSubview:view];
    }
}

-(void)btn_close:(int)sender{
    [UIView animateWithDuration:0.5 animations:^{
        self->popup.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self->popup removeFromSuperview];
        self->popup = nil;
        UIView *view = [self.view viewWithTag:123];
        [view removeFromSuperview];
    }];
}

-(void)signInClick:(NSString *)txtEmail
{
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:txtEmail,@"email_id",@"",@"password",nil];
    httpHelper = [[HttpHelper alloc]initWithMUrl:registerUserURL Dictionary:[dict copy] isloading:YES api_number:registerUserAPI message:@"Loading all backup"];
    httpHelper.delegate= self;
}

-(void)signInWithPassword:(NSString *)txt_pass withEmail:(NSString *)txtEmail
{
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:txtEmail,@"email_id", txt_pass, @"password", nil];
    httpHelper = [[HttpHelper alloc]initWithMUrl:savePasswordURL Dictionary:[dict copy] isloading:YES api_number:savePasswordAPI message:@"Loading all backup"];
    httpHelper.delegate= self;
}

-(void)btn_skip
{
    [self btn_close:0];
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [app.navController dismissViewControllerAnimated:controller completion:nil];
}

-(void)fetchStuff:(NSString*)title message:(NSString*)message style:(UIAlertControllerStyle)style buttNames:(NSArray*)buttArray completion:(void (^)( NSString *))completion{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    for (NSString* key in buttArray){
        [alert addAction:[UIAlertAction actionWithTitle:key style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            completion(key);
        }]];
    }
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - API Caller
-(void)addRewardApiCall{
    NSString * userID = [[PlistHandler sharedObject] getUserID];
    NSDictionary * dict = [[NSDictionary alloc]initWithObjectsAndKeys: userID, @"user_id", @"50", @"points", nil];
    httpHelper = [[HttpHelper alloc]initWithMUrl:addRewardURL Dictionary:dict isloading:NO api_number:addRewardAPI message:@"add reward"];
    httpHelper.delegate = self;
    
}

-(void)claimRewardApiCall{
    NSString * userID = [[PlistHandler sharedObject] getUserID];
    NSDictionary * dict = [[NSDictionary alloc]initWithObjectsAndKeys: userID, @"user_id", @"100", @"points", nil];
    httpHelper = [[HttpHelper alloc]initWithMUrl:claimRewardURL Dictionary:dict isloading:YES api_number:claimRewardAPI message:@"Claiming..."];
    httpHelper.delegate = self;
}

#pragma mark getBACKUPCOUNT
-(void)updateBackupCount:(NSInteger)count{
    [[NSUserDefaults standardUserDefaults] setInteger:count forKey:@"HighScore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(int)getBackUpCount{
    NSString * pointUsed = [[PlistHandler sharedObject]getValueFromPlistByKey:REWARD_USED];
    int rewardUsed = [[self handleNullValueOfString:pointUsed] intValue];
    return rewardUsed;
}

#pragma mark - Change Image Color
-(UIImage *)ipMaskedImageNamed:(UIImage *)image color:(UIColor *)color {
    //    UIImage *image = [UIImage imageNamed:name];
    if (color == nil){
        return image;
    }
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, image.scale);
    CGContextRef c = UIGraphicsGetCurrentContext(); [image drawInRect:rect];
    CGContextSetFillColorWithColor(c, [color CGColor]);
    CGContextSetBlendMode(c, kCGBlendModeSourceAtop);
    CGContextFillRect(c, rect);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

#pragma mark - GADBANNerDelegate
/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"adViewDidReceiveAd");
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}
@end
