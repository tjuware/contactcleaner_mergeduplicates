//
//  MasterVC.h
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 11/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"
#import "viewModification.h"
#import "Constant.h"
#import "ContactAction.h"
#import "SVProgressHUD.h"
#import "Toast+UIView.h"
#import "SliderMenuView.h"
#import "AppDelegate.h"
#import "PlistHandler.h"
#import "HttpHelper.h"
#import "FileManipulation.h"
#import "LocalNotifications.h"
#import "AlertControllerDesign.h"
#import "ShareDialogView.h"
#import "ContactList.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "LoginPopUpView.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

#define FRAMEWIDTH [[UIScreen mainScreen] bounds].size.width
#define FRAMEHEIGHT [[UIScreen mainScreen] bounds].size.height
#define Bg_color [UIColor colorWithRed:16.0/255.0 green:25.0/255.0 blue:139.0/255.0 alpha:0.8]
#define CONTACTCELL_ID  @"ContactCellID"
#define RECOVER  @"SCM_Scan_Recover_Contact.json"
#define SCMCONTACT  @"SCM_Scan_Contact.json"
#define REWARD_POINT @"reward_points"
#define REWARD_USED @"reward_count_used"

//Other Company Apps Links

#define appStoreBaseURL @"https://apps.apple.com/in/app/"

static NSString * ballsHitAppURL = appStoreBaseURL @"balls-hit-smash/id1083462882";
static NSString * bmqAppURL = appStoreBaseURL @"bollywood-movies-quiz-guess-so/id708263591";
static NSString * bsqAppURL = appStoreBaseURL @"bollywood-songs-quiz/id695445842";
static NSString * hbCelebrityAppURL = appStoreBaseURL @"bollywood-hollywood-star-quiz/id6804607467";
static NSString * boxOfficeAppURL = appStoreBaseURL @"hollywood-bollywood-quizup/id1121590226";
static NSString * housieAppURL = appStoreBaseURL @"housie-tambola-number-picker/id1448156725";
static NSString * mathsAppURL = appStoreBaseURL @"math-learner-learning-maths/id1484812590";
static NSString * waterAppURL = appStoreBaseURL @"drink-water-reminders-tracker/id1453280308";
static NSString * notesAppURL = appStoreBaseURL @"sticky-notes-to-do-list/id1534095481";
static NSString * pedoAppURL = appStoreBaseURL @"pedometer-walking-steptracker/id1465881182";
static NSString * scanDuplicateAppURL = appStoreBaseURL @"contact-cleaner-merge-email/id1444870653";
static NSString * scmAppURL = appStoreBaseURL @"contacts-backup-transfer-easy/id1032947867";
static NSString * scmBackUpAppURL = appStoreBaseURL @"smart-contact-manager/id1443756011";
static NSString * bdayContactUpAppURL = appStoreBaseURL @"birthday-contact-backup/id827642824";


@interface MasterVC : UIViewController<ShareDialogViewDelegate, LoginPopUpDelegate, HttpHelperDelegate>
{
    AppDelegate *app;
    HttpHelper *httpHelper;
    NSArray *arr_color;
    NSDictionary *dict_plist;
    LoginPopUpView *popup;
    NSString *email;
}
@property (strong, nonatomic)ShareDialogView *shareview;
@property(nonatomic, strong) GADInterstitial *interstitial;

+ (id)sharedInstance;
- (NSURL *)applicationDocumentsDirectory;
-(void)slideView;
-(void)redirectToHome;
-(NSDictionary *)removeNullValuefromDictionary:(NSDictionary *)dict;
-(NSString *)handleNullValueOfString:(NSString *)value;
-(BOOL) NSStringIsValidEmail:(NSString *)checkString;
-(void)sliderMenuList;
-(void)loginAlertController;
-(void)setBackApiResponse:(NSMutableDictionary *)response api_number:(int)api_number;
-(void)redirectToPreviewContactVC:(NSString *)filename;
-(void)updateCheckPasswordAPI :(NSString *)password WithCompletionHandler:(void(^)(NSMutableDictionary *dict, BOOL completed))completionHandler;
-(NSArray *)downloadVCFandDisplay:(NSString *)filename;
-(void)showShareView;
-(void)redirectToBackupHistory;
- (void)setBackgroundGradient:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha;
-(void)shadowHeaderView:(UIView *)view;
-(void)googleLogin;
-(void)shadowHeaderViewWithAplha:(UIView *)view;
-(void)showLoginPopUp;
-(void)updatePlistDictioryByKey:(NSString *)key andValue:(NSString *)value;
-(void)startActivityIndicator:(NSString *)message withCompletion:(void(^)(BOOL completed))completionHandler;
-(void)stopActivityIndicator;
-(void)secondMethod;
-(void)returnbannerView :(UIView *)adsview :(UIViewController *)VC;
- (void)createAndLoadInterstitial:(UIViewController *)views;
- (void)setBackgroundGradient:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha withName:(NSString *)name;
- (GADInterstitial *)createAndLoadInterstitiala;
-(void)doSomethingInterstitial;
-(void)addRewardApiCall;
-(void)updateBackupCount:(NSInteger)count;
-(void)claimRewardApiCall;
-(int)getBackUpCount;
-(void)showErrorWithStatus:(NSString*)string;
-(void)fetchStuff:(NSString*)title message:(NSString*)message style:(UIAlertControllerStyle)style buttNames:(NSArray*)buttArray completion:(void (^)( NSString *))completion;
-(UIImage *)ipMaskedImageNamed:(UIImage *)image color:(UIColor *)color;
@end
