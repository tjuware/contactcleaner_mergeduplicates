//
//  ContactCell.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 28/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@protocol ContactCellDelegate <NSObject>
@optional
-(void)btn_cellSelect:(id)sender;
@end
@interface ContactCell : SWTableViewCell
@property(weak,nonatomic)id <ContactCellDelegate> delegate_cell;
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_left;
@property (strong, nonatomic) IBOutlet UIImageView *img_check;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_right;
-(void)showIMGCheck:(BOOL)ishide;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_img_checkWidth;
-(void)showIMGCheck:(BOOL)ishide forCell:(ContactCell *)cell;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_rightToParentLabelName;

@end
