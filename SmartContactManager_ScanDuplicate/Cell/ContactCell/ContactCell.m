//
//  ContactCell.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 28/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)showIMGCheck:(BOOL)ishide forCell:(ContactCell *)cell
{
    if(ishide)
    {
        self.cnst_img_checkWidth.constant = 25;
        [self.img_check setHidden:NO];
    }else
    {
        self.cnst_img_checkWidth.constant = 0;
        [self.img_check setHidden:YES];
    }
    [self layoutIfNeeded];
}
@end
