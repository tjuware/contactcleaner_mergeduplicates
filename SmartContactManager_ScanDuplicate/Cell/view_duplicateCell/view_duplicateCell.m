//
//  view_duplicateCell.m
//  Smart_Contact_Manager
//
//  Created by 3__smitayevalkar on 12/04/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "view_duplicateCell.h"

@implementation view_duplicateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    _contactProfilePic.layer.cornerRadius=_contactProfilePic.frame.size.width/2;
    _contactProfilePic.layer.masksToBounds=YES;
}

@end
