//
//  view_duplicateCell.h
//  Smart_Contact_Manager
//
//  Created by 3__smitayevalkar on 12/04/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface view_duplicateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *contactProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UILabel *contactInfo;
@property (strong, nonatomic) IBOutlet UIImageView *imgSelected;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contactNameTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contactNameHeight;
@property (weak, nonatomic) IBOutlet UITextView *tv_ContactNumber;
@property (strong, nonatomic) IBOutlet UIView *view_inner;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contactInfoBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contactInfoHeight;
@end
