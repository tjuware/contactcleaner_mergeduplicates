//
//  PopUpVC.m
//  SmartContactManager_ScanDuplicate
//
//  Created by 1_Somnath on 11/12/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "PopUpVC.h"
#import "DTCustomColoredAccessory.h"
@interface PopUpVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableIndexSet *expandedSections;
    NSMutableArray *email_array;
    NSMutableArray *phone_array;
     NSMutableArray *merge_array;
    NSMutableDictionary *dictionary;
}
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UIView *content_view;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_heighttable_email;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_heighttable_number;
@property (weak, nonatomic) IBOutlet UITableView *tableView_email;
@property (weak, nonatomic) IBOutlet UITableView *tableView_phone;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_viewheight;
@end
@implementation PopUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    _tableView_email.delegate = self;
    _tableView_email.dataSource = self;
    _tableView_phone.delegate = self;
    _tableView_phone.dataSource = self;
    dictionary = [_info_dict mutableCopy];
    [_lbl_name setText:[dictionary objectForKey:@"name"]];
    phone_array =[[dictionary objectForKey:@"phoneNumbers"] mutableCopy];
    email_array = [[dictionary objectForKey:@"emailAddresses"] mutableCopy];
//    if(email_array.count == 0)
    [_cnst_heighttable_email setConstant:0];
    if(phone_array.count == 0)
    [_cnst_heighttable_number setConstant:0];
    [self setShadowEffecttoView:_content_view];
    merge_array = [[phone_array arrayByAddingObjectsFromArray:email_array] mutableCopy];
    NSLog(@"array for merge %@",merge_array);
    
    
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)btn_close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)setShadowEffecttoView :(UIView *)viewCheck
{
    viewCheck.layer.shadowRadius  = 1.5f;
    viewCheck.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    viewCheck.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    viewCheck.layer.shadowOpacity = 0.9f;
    viewCheck.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(viewCheck.bounds, shadowInsets)];
    viewCheck.layer.shadowPath    = shadowPath.CGPath;
}

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if (section>=0) return YES;
    
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
           if(merge_array.count >0)
//            return email_array.count;
//           else if(tableView ==_tableView_phone && phone_array.count >0)
//               return phone_array.count;
            return merge_array.count +1;
           else
               return 0;
        }
        
        return 1; // only top row showing
    }
    
    // Return the number of rows in the section.
    return 1;
}
- (IBAction)recoverbtnsuccess:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.popUpDelegate didSucess:dictionary];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    // Configure the cell...
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        NSArray *array = [merge_array copy];
//        if(tableView == _tableView_email && email_array.count >0)
//            array =[email_array copy];
//        else if(tableView == _tableView_phone && phone_array.count >0)
//            array= [phone_array copy];
//        else
//            return cell;
        if (!indexPath.row)
        {
            
            // first row
//            cell.textLabel.text = [array objectAtIndex:0]; // only top row showing
            cell.textLabel.text = [NSString stringWithFormat:@"you have %lu contacts with %lu emails", phone_array.count,email_array.count];
            if ([expandedSections containsIndex:indexPath.section])
            {
                cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeUp];
            }
            else
            {
                cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeDown];
            }
        }
        else
        {
            // all other rows
            cell.textLabel.text = [array objectAtIndex:indexPath.row -1];
            cell.accessoryView = nil;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    else
    {
        cell.accessoryView = nil;
        cell.textLabel.text = @"Normal Cell";
        
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if ([self tableView:tableView canCollapseSection:indexPath.section] && merge_array.count >0)
    {
        if (!indexPath.row)
        {
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            float cellcount_phone = merge_array.count * 40;
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [expandedSections removeIndex:section];
                [UIView animateWithDuration:1.5f animations:^{
                    [self->_cnst_heighttable_number setConstant:self.tableView_phone.frame.size.height - cellcount_phone];
                    [self->_cnst_viewheight setConstant:self.content_view.frame.size.height - 80];
                }];
                
                
            }
            else
            {
                [expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [UIView animateWithDuration:1.5f animations:^{
                    [self->_cnst_heighttable_number setConstant:self.tableView_phone.frame.size.height + cellcount_phone];
                    [self->_cnst_viewheight setConstant:self.content_view.frame.size.height + 80];
                }];
                
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            if (currentlyExpanded)
            {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                
                cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeDown];
                
            }
            else
            {
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                cell.accessoryView =  [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeUp];
                
            }
        }
    }
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if([expandedSections containsIndex:indexPath.section] && indexPath.row != 0){
        return YES;
    }
    return NO;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( editingStyle == UITableViewCellEditingStyleDelete) {
        NSString *valueofinde = [merge_array objectAtIndex:indexPath.row -1];
        if([email_array containsObject:valueofinde])
            [email_array removeObject:valueofinde];
        else if ([phone_array containsObject:valueofinde])
            [phone_array  removeObject:valueofinde];
        
        [merge_array removeObjectAtIndex:indexPath.row -1 ];
        // tell table to refresh now
    }
    [dictionary removeObjectForKey:@"phoneNumbers"];
    [dictionary removeObjectForKey:@"emailAddresses"];
    
    [dictionary setObject:phone_array forKey:@"phoneNumbers"];
    [dictionary setObject:email_array forKey:@"emailAddresses"];
    [tableView reloadData];
}

@end
