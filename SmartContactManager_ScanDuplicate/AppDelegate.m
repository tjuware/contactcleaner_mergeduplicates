//
//  AppDelegate.m
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 05/11/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//
// TJ_Uploaded 7.2.0 07/02/2022
#import "AppDelegate.h"
#import <Contacts/Contacts.h>
//#import "ViewController.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <UserNotifications/UserNotifications.h>
#import "TutorialVC.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "InitialVC.h"
@import Firebase;
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [FIRApp configure];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userContactsChange:) name:CNContactStoreDidChangeNotification object:nil];
    //    [[GADMobileAds sharedInstance] startWithCompletionHandler:^( * _Nonnull status) {
    //        NSLog(@"%@", status);
    //    }];
    NSLog(@"Version 3.1.0 By VJ");
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    InitialVC *vc = [[InitialVC alloc] initWithNibName:@"InitialVC" bundle:nil];
    self.navController=[[UINavigationController alloc]initWithRootViewController:vc];
    [self.navController setNavigationBarHidden:YES];
    self.window.rootViewController=self.navController;
    [self.window makeKeyAndVisible];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self dismissSplashController];
    });
    
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound + UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (!granted) {
                //Show alert asking to go to settings and allow permission
            }
        }];
    }
    
    application.applicationIconBadgeNumber = 0;
    
    return YES;
}

-(void)dismissSplashController{
   
        NSString *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"isDemoSeen"];
        if(data==nil)
        {
            [[PlistHandler sharedObject]removeDataFromPlist];
            TutorialVC *viewController = [[TutorialVC alloc]init];
            self.navController=[[UINavigationController alloc]initWithRootViewController:viewController];
            [self.navController setNavigationBarHidden:YES];
            self.window.rootViewController=self.navController;
            [self.window makeKeyAndVisible];
        }else{
            NSDictionary *dict_wholeContact = [[FileManipulation sharedContacts] readWholePhoneContactDictionaryFromJsonFile:SCMCONTACT];
            [[ContactList sharedContacts] fetchAllContacts:NO WithCompletionHandler:^(int result, NSMutableArray *arr) {
                if(result == 2 && dict_wholeContact.count <= 0){
                    [[FileManipulation sharedContacts]writeWholePhoneContactToJsonFile:arr andFileName:SCMCONTACT];
                }
            }];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            self.navController=[[UINavigationController alloc]initWithRootViewController:viewController];
            [self.navController setNavigationBarHidden:YES];
            self.window.rootViewController=self.navController;
            [self.window makeKeyAndVisible];
        }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}
-(void)userContactsChange:(NSNotification *)note
{
    [self performSelector:@selector(callDelay) withObject:nil afterDelay:2.0];
}

-(void)callDelay
{
    [[ContactAction sharedContacts] getUpdatedContactAndGoForApiCallWithCompletionHandler:^(NSMutableArray *arr_contact, BOOL completed) {
        if(completed){
            NSDictionary *dict = [[FileManipulation sharedContacts] readWholePhoneContactDictionaryFromJsonFile: RECOVER];
            NSMutableArray *arr = [[dict objectForKey:@"PhoneContacts"] mutableCopy];
            if(arr.count <= 0 || arr == nil)
                arr = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in arr_contact)
            {
                [arr addObject:temp];
            }
            [[FileManipulation sharedContacts] writeRecoverPhoneContactToJsonFile:arr andFileName: RECOVER];
        }
    }];
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    
    return [[GIDSignIn sharedInstance] handleURL:url                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]] || [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]                                                                             annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [self callDelay];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"SmartContactManager_ScanDuplicate"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
