//
//  ShareDialogView.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 08/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "ShareDialogView.h"
#import <MessageUI/MessageUI.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
@interface ShareDialogView()<MFMessageComposeViewControllerDelegate, FBSDKSharingDelegate>
{
}
@property (strong, nonatomic) IBOutlet UIView *view_fb;
@property (strong, nonatomic) IBOutlet UIView *view_messenger;
@property (strong, nonatomic) IBOutlet UIView *view_wa;
@property (strong, nonatomic) IBOutlet UIView *topview;
@property (strong, nonatomic) IBOutlet UILabel *line1;
@property (strong, nonatomic) IBOutlet UILabel *line2;

@end
@implementation ShareDialogView
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self customInit];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self customInit];
    }
    return self;
}

-(void)customInit
{
    UIView *view=[[[NSBundle bundleForClass:[self class]] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    [self addSubview:view];
    CGRect frame=self.bounds;
    view.frame=frame;
    [self layoutIfNeeded];
    [self setNeedsLayout];
    [[viewModification sharedViews] changeViewtoCornerRadius:10 AndBorder:0.5 andBorderColor:[UIColor darkGrayColor] toView:self.view_fb];
    [[viewModification sharedViews] changeViewtoCornerRadius:10 AndBorder:0.5 andBorderColor:[UIColor darkGrayColor] toView:self.view_wa];
    [[viewModification sharedViews] changeViewtoCornerRadius:10 AndBorder:0.5 andBorderColor:[UIColor darkGrayColor] toView:self.view_messenger];
    [[viewModification sharedViews] changeViewtoCornerRadius:10 AndBorder:1.00 andBorderColor:[UIColor whiteColor] toView:self.topview];
    [self.line1 setFrame:CGRectMake(1000, _line1.frame.origin.y, _line1.frame.size.width, _line1.frame.size.height)];
    [self.line1 setFrame:CGRectMake(1000, _line2.frame.origin.y, _line2.frame.size.width, _line2.frame.size.height)];
}
- (IBAction)shareViaMessage:(id)sender{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertController *whatsapp_alert = [UIAlertController alertControllerWithTitle:@"Info" message:@"Your device doesn't support SMS!" preferredStyle:UIAlertControllerStyleAlert] ;
        [whatsapp_alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self->app.navController dismissViewControllerAnimated:YES completion:nil];
        } ]];
        return;
    }
    
    NSString *message= @"Good News.!!! Smart Contact Manager - Backup. It FREE for few days.Download it. https://itunes.apple.com/us/app/id1032947867";
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    // [messageController setRecipients:recipents];
    [messageController setBody:message];
    // Present message view controller on screen
    [app.navController presentViewController:messageController animated:YES completion:nil];
}

- (IBAction)shareViaWhatsapp:(id)sender{
    NSString *msg= @"Good News.!!! Smart Contact Manager - Backup. It FREE for few days.Download it. https://itunes.apple.com/us/app/id1032947867";
    msg=   [msg stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"whatsapp://send?text=%@",msg]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL options:@{} completionHandler:^(BOOL success) {
            [self hideSideMenu:self];
            if([self.delegate respondsToSelector:@selector(shareSuccess)])
                [self.delegate shareSuccess];
        }];
    }
    else
    {
        UIAlertController *whatsapp_alert = [UIAlertController alertControllerWithTitle:@"Info" message:@"Whatsapp not installed" preferredStyle:UIAlertControllerStyleAlert] ;
        [whatsapp_alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self->app.navController dismissViewControllerAnimated:YES completion:nil];
        } ]];
        [app.navController presentViewController:whatsapp_alert animated:YES completion:nil];
    }
}

- (IBAction)btn_fbClicked:(id)sender {
    [self hideSideMenu:self];
   
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:@"https://itunes.apple.com/us/app/id1032947867"];
    [FBSDKShareDialog showFromViewController:self.parentVC withContent:content delegate:self];
    NSLog(@"parent %@",self.parentVC);
    
}
-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    if([_delegate respondsToSelector:@selector(shareSuccess)])
        [self.delegate shareSuccess];
}

-(void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    if([_delegate respondsToSelector:@selector(shareSuccess)])
        [self.delegate shareSuccess];
}
-(void)showSideMenu:(ShareDialogView *)view{
    CGFloat width = FRAMEWIDTH * 0.85;
    CGFloat height = FRAMEHEIGHT * 0.25;
    [view setFrame:CGRectMake(-width, (FRAMEHEIGHT - height)/2, width, height)];
    [self.parentView insertSubview:view atIndex:[self.parentView subviews].count + 1];
    view.alpha = 0.0;
    [UIView animateWithDuration:1.0f animations:^{
        [view setFrame:CGRectMake((FRAMEWIDTH - width)/2, (FRAMEHEIGHT - height)/2, width, height)];
        view.alpha = 1.0;
    }];
}

-(void)hideSideMenu:(ShareDialogView *)view{
    CGFloat width = FRAMEWIDTH * 0.85;
    CGFloat height = FRAMEHEIGHT * 0.25;
    UIView *vi = [self.parentVC.view viewWithTag:143];

    [UIView animateWithDuration:1.0f animations:^{
        [view setFrame:CGRectMake(-width, (FRAMEHEIGHT - height)/2, width, height)];
        view.alpha = 0.0;
        vi.alpha = 0.0;
    }completion:^(BOOL finished) {
        [view removeFromSuperview];
        [vi removeFromSuperview];
    }];
}
#pragma mark - mfmessageComposeVC delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    [self hideSideMenu:self];

    switch (result) {
        case MessageComposeResultCancelled:
        {
            [self makeToast:@"You cancelled the SMS/text"];
            if([self.delegate respondsToSelector:@selector(shareFailed)])
                [self.delegate shareFailed];
        }
            break;
            
        case MessageComposeResultFailed:
        {
            [self makeToast:@"SMS/text failed to send"];
            if([self.delegate respondsToSelector:@selector(shareSuccess)])
                [self.delegate shareSuccess];
        }
            break;
            
        case MessageComposeResultSent:
        {
            [self makeToast:@"SMS/text sent successfully"];
            if([self.delegate respondsToSelector:@selector(shareSuccess)])
                [self.delegate shareSuccess];
        }
            break;
    }
    
    [app.navController dismissViewControllerAnimated:YES completion:nil];
}
@end
