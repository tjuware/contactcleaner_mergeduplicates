//
//  ContactList.h
//  ContactsList
//
//  Created by ndot on 11/01/16.
//  Copyright © 2016 Ktr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h> //AddressBook.framework for below iOS 9
#import <Contacts/Contacts.h> //Contacts.framework for above iOS 9

@interface ContactList : NSObject{
    
    NSMutableArray *totalPhoneNumberArray; //Total Mobile Contacts from access from this variable
    
    NSMutableArray *groupsOfContact; //Collection of contacts by using contacts.framework
    NSArray *arrayOfAllPeople; //Collection of contacts by using  AddressBook.framework
    
    //ABAddressBookRef addressBook; //Address Book Object
    
    CNContactStore *contactStore; //ContactStore Object
}

@property (nonatomic,retain) NSMutableArray *totalPhoneNumberArray; //Total Mobile Contacts access from this variable property
@property (nonatomic,retain) NSString *fileName;
//fetch Contact shared instance method
+(id)sharedContacts; //Singleton method


//- (void)fetchAllContacts:(BOOL)isLoad WithCompletionHandler:(void(^)(NSMutableArray *arr, BOOL completed))completionHandler;

-(void) deleteAllContacts ;
-(void)generateAndSaveVCF:(NSMutableArray *)arr_contact WithUSerID:(NSString *)user_id andCompletionHandler:(void(^)(NSString *filename, BOOL completed))completionHandler;
-(void) uploadFile:(NSString *)email_id WithUserID:(NSString*)user_id;
-(void) uploadFile:(NSString *)email_id WithCompletionHandler:(void (^)(int result, NSMutableDictionary * response))completion;
- (void)fetchAllContacts:(BOOL)isLoad WithCompletionHandler:(void(^)(int result, NSMutableArray *arr))completionHandler ;

@end



