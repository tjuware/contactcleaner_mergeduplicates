//
//  ContactAction.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 03/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "ContactAction.h"
#import <ContactsUI/ContactsUI.h>
#import "AppDelegate.h"
#import "ContactList.h"
#import "FileManipulation.h"

#define RECOVER  @"SCM_Scan_Recover_Contact.json"
#define SCMCONTACT  @"SCM_Scan_Contact.json"
@interface ContactAction()<CNContactViewControllerDelegate>
{
    CNContactStore *contactStore;
    NSMutableArray *arr_dupName, *arr_dupNum, *arr_dupEmail, *Arr_device_cnt;
}
@property(strong,nonatomic)AppDelegate* appD;

@end

@implementation ContactAction

#pragma mark - Singleton Methods
+ (id)sharedContacts { //Shared instance method
    
    static ContactAction *sharedMyContacts = nil; //create contactsList Object
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //for first time create shared instance object
        sharedMyContacts = [[self alloc] init];
    });
    
    return sharedMyContacts;
}

- (id)init { //init method
    if (self = [super init]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.appD =(AppDelegate*)[[UIApplication sharedApplication] delegate];
            self->contactStore = [[CNContactStore alloc] init]; //init a contactStore object
            
        });
    }
    return self;
}
#pragma mark - contactView
-(void)viewOrEditContact:(CNContact *)contact andIsForEdit:(BOOL)isEdit
{
    CNContactViewController *controller = (isEdit) ? [CNContactViewController viewControllerForNewContact:contact] :[CNContactViewController viewControllerForContact:contact];
    CNContactStore *contactStore = [[CNContactStore alloc] init];
    controller.contactStore = contactStore;
    controller.allowsEditing = YES;
    controller.allowsActions = YES;
    controller.modalPresentationStyle = UIModalPresentationFormSheet;
    controller.delegate =self;
    
    //                UINavigationController *wrapper = [[UINavigationController alloc] initWithRootViewController:controller];
    //                [self presentViewController:wrapper animated:YES completion:nil];
    [_appD.navController setNavigationBarHidden:YES animated:YES];
    [_appD.navController pushViewController:controller animated:YES];
}

-(void)redirectToAddContact:(CNContact *)contact
{
    CNContactViewController *controller = [CNContactViewController viewControllerForNewContact:[contact copy]];
    CNContactStore *contactStore = [[CNContactStore alloc] init];
    controller.contactStore = contactStore;
    controller.allowsEditing = YES;
    controller.allowsActions = YES;
    controller.modalPresentationStyle = UIModalPresentationFormSheet;
    controller.delegate =self;
    
    //                UINavigationController *wrapper = [[UINavigationController alloc] initWithRootViewController:controller];
    //                [self presentViewController:wrapper animated:YES completion:nil];
    [_appD.navController setNavigationBarHidden:YES animated:YES];
    [_appD.navController pushViewController:controller animated:YES];
}

-(void)addNewContactToDevice:(NSDictionary *)dict_contact andSaveDirectly:(BOOL)isDirectly
{
    CNMutableContact *contact = [[CNMutableContact alloc]init];
    contact.givenName = [NSString stringWithFormat:@"%@",[dict_contact objectForKey:@"givenName"]];
    contact.middleName = [NSString stringWithFormat:@"%@",[dict_contact objectForKey:@"middleName"]];
    contact.familyName = [NSString stringWithFormat:@"%@",[dict_contact objectForKey:@"familyName"]];
    
    NSArray *arr_phn = [dict_contact objectForKey:@"phoneNumbers"];
    NSMutableArray *arr_newPhn = [[NSMutableArray alloc]init];
    
    for (NSString* key in arr_phn) {
        CNPhoneNumber *phn = [[CNPhoneNumber alloc]initWithStringValue: key];
        CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:phn];
        [arr_newPhn addObject:homePhone];
    }
    contact.phoneNumbers = [arr_newPhn copy];
    
    NSArray *arr_email = [dict_contact objectForKey:@"emailAddresses"];
    NSMutableArray *arr_newEmail = [[NSMutableArray alloc]init];
    for (NSString* key in arr_email) {
        CNLabeledValue *homeEmail = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:key];
        [arr_newEmail addObject:homeEmail];
    }
    contact.emailAddresses = [arr_newEmail copy];
    
    CNMutablePostalAddress *postalAddr = [[CNMutablePostalAddress alloc] init];
    postalAddr.street = [NSString stringWithFormat:@"%@",[dict_contact objectForKey:@"street"]];
    postalAddr.city = [NSString stringWithFormat:@"%@",[dict_contact objectForKey:@"city"]];
    postalAddr.postalCode = [NSString stringWithFormat:@"%@",[dict_contact objectForKey: @"postalCode"]];
    postalAddr.state = [NSString stringWithFormat:@"%@",[dict_contact objectForKey:@"state"]];
    postalAddr.country = [NSString stringWithFormat:@"%@",[dict_contact objectForKey:@"country"]];
    CNLabeledValue *homeAddress = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:postalAddr];
    
    contact.postalAddresses = @[homeAddress];
    if(!isDirectly)
        [self redirectToAddContact:[contact copy]];
    else
        [self saveContact:contact];
}

-(void) deleteAllContacts {
    NSArray *keys = @[CNContactPhoneNumbersKey,CNContactGivenNameKey];
    NSString *containerId = contactStore.defaultContainerIdentifier;
    NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
    NSError *error;
    NSArray *cnContactsa = [contactStore unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
    
    if (error) {
        NSLog(@"error fetching contacts %@", error);
    } else {
        CNSaveRequest *saveRequest = [[CNSaveRequest alloc] init];
        
        for (CNContact *contact in cnContactsa) {
            if([contact.givenName isEqualToString:@"Akdam"])
            {
                [saveRequest deleteContact:[contact mutableCopy]];
                
            }
        }
        [contactStore executeSaveRequest:saveRequest error:nil];
    }
}
-(void) deleteSingleContactFile:(CNContact *)contact{
    CNSaveRequest *saveRequest = [[CNSaveRequest alloc] init];
    [saveRequest deleteContact:[contact mutableCopy]];
    [contactStore executeSaveRequest:saveRequest error:nil];
}
-(void) deleteSingleContact:(CNContact *)contact WithCompletionHandler:(void(^)(CNContact *contact, BOOL completed))completionHandler{
    [self deleteSingleContactFile:contact];
    completionHandler(contact, YES);
}

-(void)addSingleVCFContact:(NSString *)vcf_str
{
    NSData *vCardData =[vcf_str dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSMutableArray *trial =(NSMutableArray *) ([CNContactVCardSerialization contactsWithData:vCardData error:&err]);  //(__bridge CNMutableContact
    CNMutableContact *contact =[[trial objectAtIndex:0]mutableCopy] ;
    [self saveContact:contact];
}

-(void)saveContact:(CNMutableContact *)contact
{
    CNSaveRequest *request = [[CNSaveRequest alloc] init];
    [request addContact:contact toContainerWithIdentifier:nil];
    NSError *saveError;
    if (![contactStore executeSaveRequest:request error:&saveError]) {
        NSLog(@"error = %@", saveError);
    }else{
        NSLog(@"contact Added succesfully");
    }
}

-(void)updateContact:(CNMutableContact*)contact
{
    @try {
        CNContactStore *contactStore =[[CNContactStore alloc]init];
        NSError *errror;
        [contactStore containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[contactStore.defaultContainerIdentifier]] error:&errror];
        CNSaveRequest *saveReq = [CNSaveRequest new];
        [saveReq updateContact:contact];
        NSError *deleteContactError;
        [contactStore executeSaveRequest:saveReq error:&deleteContactError];
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    }
    
}
#pragma mark - CNContactViewControllerDelegate
-(void)contactViewController:(CNContactViewController *)viewController didCompleteWithContact:(CNContact *)contact
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)contactViewController:(CNContactViewController *)viewController shouldPerformDefaultActionForContactProperty:(CNContactProperty *)property
{
    return YES;
}

-(void)getUpdatedContactAndGoForApiCallWithCompletionHandler:(void(^)(NSMutableArray *arr_contact, BOOL completed))completionHandler
{
    [[ContactList sharedContacts] fetchAllContacts:NO WithCompletionHandler:^(int result, NSMutableArray *arr) {
        if(result == 2){
            NSMutableArray *arr_temp = arr;//[[ContactList sharedContacts]totalPhoneNumberArray];
            
            [arr_temp enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSMutableDictionary *newdict = [[arr_temp objectAtIndex:idx] mutableCopy];
                [newdict removeObjectForKey:@"contact"];
                [arr_temp replaceObjectAtIndex:idx withObject:newdict];
            }];
            NSMutableArray *array_deletedContact=[[NSMutableArray alloc] init];
            NSArray *array_contacts = [arr_temp copy];
            NSDictionary *dict_wholeContact = [[FileManipulation sharedContacts] readWholePhoneContactDictionaryFromJsonFile:SCMCONTACT];
            NSArray *array_wholeContacts = [dict_wholeContact objectForKey:@"PhoneContacts"];
            if(array_wholeContacts.count >0)
            {
                for(NSDictionary *dict_contact in array_wholeContacts){
                    if(![[NSArray arrayWithArray:array_contacts] containsObject:dict_contact])
//                    if(![array_contacts containsObject:dict_contact])
                        [array_deletedContact addObject:dict_contact];
                }
                
                if(array_wholeContacts.count >0)
                    [[FileManipulation sharedContacts] clearWholePhoneContactToJsonFile:SCMCONTACT];
                
                if(arr != nil)
                    [[FileManipulation sharedContacts] writeWholePhoneContactToJsonFile:arr andFileName:SCMCONTACT];
            }
            completionHandler(array_deletedContact, YES);
        }else{
            completionHandler(nil, NO);
        }
        
    }];
}
-(void) scanContactWithCompletionHandler:(void(^)(NSMutableArray *arr_name, NSMutableArray *arr_number, NSMutableArray *arr_email))completionHandler
{
    NSMutableArray *cnts=[[NSMutableArray alloc]init];
    NSMutableArray *num_cnts =[[NSMutableArray alloc]init];
    NSMutableArray *email_cnts =[[NSMutableArray alloc]init];
    arr_dupName =[[NSMutableArray alloc]init];
    arr_dupNum =[[NSMutableArray alloc]init];
    arr_dupEmail =[[NSMutableArray alloc]init];
    
    [[ContactList sharedContacts]fetchAllContacts:NO WithCompletionHandler:^(int result, NSMutableArray *arr) {
        if(result == 2){
            
            self->Arr_device_cnt =arr;
            for(int i = 0; i< self->Arr_device_cnt.count; i++)
            {
                NSString *name =[self->Arr_device_cnt valueForKey:@"name"][i];
                NSArray *arr_number =[self->Arr_device_cnt valueForKey:@"phoneNumbers"][i];
                NSArray *arr_email =[self->Arr_device_cnt valueForKey:@"emailAddresses"][i];
                
                @autoreleasepool {
                    if(![cnts containsObject:name])
                    {
                        [cnts addObject:name];
                        
                    } else{
                        if(![name isEqualToString:@" "])
                        {
                            if(![[self->arr_dupName valueForKey:@"name"] containsObject:[self->Arr_device_cnt[i] valueForKey:@"name"]])
                                [self->arr_dupName addObject:self->Arr_device_cnt[i]];
                        }
                    }
                    
                    for(NSString *number in arr_number){
                        if(![num_cnts containsObject:number])
                            [num_cnts addObject:number];
                        else
                            if(![number isEqualToString:@""])
                            {
                                if(![[self->arr_dupNum valueForKey:@"phoneNumbers"] containsObject:[self->Arr_device_cnt[i]valueForKey:@"phoneNumbers"]])
                                    [self->arr_dupNum addObject:self->Arr_device_cnt[i]];
                            }
                    }
                    
                    for(NSString *email in arr_email){
                        if(![email_cnts containsObject:email])
                            [email_cnts addObject:email];
                        else
                            if(![email isEqualToString:@""])
                            {
                                if(![[self->arr_dupEmail valueForKey:@"emailAddresses"] containsObject:[self->Arr_device_cnt[i]valueForKey:@"emailAddresses"]])
                                    [self->arr_dupEmail addObject:self->Arr_device_cnt[i]];
                            }
                    }
                }
            }
            
            //        NSLog(@"duplicate Name %@",self->arr_dupName);
            //        NSLog(@"duplicate Number %@",self->arr_dupNum);
            [self duplicateContactDetailWithCompletionHandler:^(NSMutableArray *arr_name, NSMutableArray *arr_number, NSMutableArray *arr_email) {
                completionHandler(arr_name,arr_number,arr_email);
            }];
        }else{
            
        }
        
    }];
}

-(void) duplicateContactDetailWithCompletionHandler:(void(^)(NSMutableArray *arr_name, NSMutableArray *arr_number, NSMutableArray *arr_email))completionHandler
{
    NSMutableArray *name_inner ;
    NSMutableArray *num_inner, *email_inner;
    
    NSMutableArray *name_master =[[NSMutableArray alloc]init];
    NSMutableArray *num_master =[[NSMutableArray alloc]init];
    NSMutableArray *email_master =[[NSMutableArray alloc]init];
    
    //get detail for name
    for(int i = 0; i< arr_dupName.count; i++)
    {
        name_inner = [[NSMutableArray alloc]init];
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"givenName LIKE %@",[arr_dupName[i] valueForKey:@"givenName"]];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"familyName == %@",[arr_dupName[i] valueForKey:@"familyName"]];
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
        
        name_inner = [[Arr_device_cnt filteredArrayUsingPredicate:predicate]mutableCopy];
        
        [name_master addObject:name_inner];
    }
    
    // get detail for number
    
    for(int i = 0; i< arr_dupNum.count; i++)
    {
        NSDictionary *dict_number = [arr_dupNum objectAtIndex:i];
        NSArray *arr_num = [dict_number valueForKey:@"phoneNumbers"];
        for(NSString *number in arr_num){
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"phoneNumbers CONTAINS %@",number];
            
            num_inner = [[Arr_device_cnt filteredArrayUsingPredicate:predicate2]mutableCopy];
            if(num_inner.count>1){
                num_inner = [self createNewArr:num_inner number:number andKey:@"number"];
                if(![num_master containsObject:num_inner])
                    [num_master addObject: num_inner];
                //            NSLog(@"All Duplicate number %@",num_master);
            }
        }
    }
    
    for(int i = 0; i< arr_dupEmail.count; i++)
    {
        NSDictionary *dict_email = [arr_dupEmail objectAtIndex:i];
        NSArray *arr_email = [dict_email valueForKey:@"emailAddresses"];
        for(NSString *email in arr_email){
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"emailAddresses CONTAINS %@",email];
            
            email_inner = [[Arr_device_cnt filteredArrayUsingPredicate:predicate2]mutableCopy];
            if(email_inner.count>1){
                email_inner = [self createNewArr:email_inner number:email andKey:@"email"];
                if(![email_master containsObject:email_inner])
                    [email_master addObject: email_inner];
                //            NSLog(@"All Duplicate number %@",num_master);
            }
        }
    }
    completionHandler(name_master, num_master, email_master);
}

-(NSMutableArray *)createNewArr :(NSMutableArray *)arr number:(NSString*)num andKey:(NSString *)key
{
    NSMutableArray *arrNew = [[NSMutableArray alloc]init];
    for(int i = 0 ;i< arr.count; i++)
    {
        NSDictionary *dict_cont = [arr objectAtIndex:i];
        NSMutableDictionary *dicc = [[NSMutableDictionary alloc]initWithDictionary:dict_cont];
        [dicc setObject:num forKey:key];
        //        NSDictionary *dict =[[NSMutableDictionary alloc]initWithObjectsAndKeys:num,@"number",dict_cont,@"Dictionary",nil];
        [arrNew addObject:dicc];
    }
    return arrNew;
}

-(void)newScanWithCompletionHandler:(void(^)(NSMutableArray *arr_name, NSMutableArray *arr_number, NSMutableArray *arr_email, NSMutableArray *arr_contact, int result))completionHandler;
{
    
    NSMutableArray * __block arr_DupName = [[NSMutableArray alloc]init];
    NSMutableArray * __block arr_DupNum = [[NSMutableArray alloc]init];
    NSMutableArray * __block arr_DupEmail = [[NSMutableArray alloc]init];
    
    NSMutableArray * __block practice;// = [self fetchContacts];
    [[ContactList sharedContacts]fetchAllContacts:NO WithCompletionHandler:^(int result, NSMutableArray *arr) {
        if(result == 2){
            practice =arr;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                [practice enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    // Name
                    NSString *name =[practice valueForKey:@"name"][idx];
                    
                    NSPredicate *predName = [NSPredicate predicateWithFormat:@"name == %@ && name != %@", name, @" "];
                    NSArray *arr = [practice filteredArrayUsingPredicate:predName];
                    if(arr.count > 1)
                    {
                        
                        NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:arr, @"name", name, @"dup_name",  nil];
                        [arr_DupName addObject:dictionary];
                    }
                    
                    // Number
                    NSArray *arr_phn = [[[practice objectAtIndex:idx] valueForKey:@"phoneNumbers"] copy];
                    [arr_phn enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSString * phnNum = [arr_phn objectAtIndex:idx];
                        NSArray *filteredData = [practice filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(phoneNumbers contains[c] %@)", phnNum]];
                        if(filteredData.count > 1)
                        {
                            NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:filteredData, @"number", phnNum, @"dup_num", nil];
                            [arr_DupNum addObject:dictionary];
                        }
                    }];
                    
                    NSArray *arr_email = [[[practice objectAtIndex:idx] valueForKey:@"emailAddresses"] copy];
                    [arr_email enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSString * phnNum = [arr_email objectAtIndex:idx];
                        NSArray *filteredData = [practice filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(emailAddresses contains[c] %@)", phnNum]];
                        if(filteredData.count > 1)
                        {
                            NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:filteredData, @"email", phnNum, @"dup_email", nil];
                            if(![arr_DupEmail containsObject:dictionary])
                                [arr_DupEmail addObject:dictionary];
                        }
                    }]  ;
                }];
                if(arr_DupName.count > 0){
                    NSOrderedSet *orderedSetP = [NSOrderedSet orderedSetWithArray:arr_DupName];
                    arr_DupName = [[orderedSetP array] mutableCopy];
                    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"dup_name" ascending:NO];
                    practice = [[[practice copy] sortedArrayUsingDescriptors:@[sortDescriptor]]mutableCopy];
                    
                }
                
                if(arr_DupNum.count > 0)
                {
                    NSOrderedSet *orderedSetE = [NSOrderedSet orderedSetWithArray:arr_DupNum];
                    arr_DupNum = [[orderedSetE array] mutableCopy];
                }
                //        NSLog(@"DUpNAME  %@", arr_DupName);
                //        NSLog(@"DUpNUm  %@", arr_DupNum);
                completionHandler(arr_DupName, arr_DupNum, arr_DupEmail,arr, 2);
            });
        }else{
            completionHandler(nil, nil, nil, nil, 1);
        }
    }];
}

-(void)mergeDuplicateContactByName:(NSDictionary *)dict WithCompletionHandler:(void(^)(CNContact *contact, BOOL completed))completionHandler
{
    CNMutableContact *newContact = [[CNMutableContact alloc] init];
    NSMutableArray __block *arr_phn = [[NSMutableArray alloc]init];
    NSMutableArray __block *arr_email = [[NSMutableArray alloc]init];
    NSArray *arr = [dict objectForKey:@"name"];
    NSDictionary *firstObject = [arr firstObject];
    CNContact *first_contact = [firstObject objectForKey:@"contact"];
    newContact.givenName = first_contact.givenName;
    newContact.middleName = first_contact.middleName;
    newContact.familyName = first_contact.familyName;
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dict = [arr objectAtIndex:idx];
        CNContact *temp_contact = [dict objectForKey:@"contact"];
        NSArray *arr_tempPhn = [[temp_contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
        NSArray *arr_tempemail = [temp_contact.emailAddresses valueForKey:@"value"];
        [arr_phn addObjectsFromArray:arr_tempPhn];
        [arr_email addObjectsFromArray:arr_tempemail];
        if(newContact.birthday ==nil)
            newContact.birthday = temp_contact.birthday;
        NSOrderedSet *orderedSetP = [NSOrderedSet orderedSetWithArray:arr_phn];
        arr_phn = [[orderedSetP array] mutableCopy];
        
        NSOrderedSet *orderedSetE = [NSOrderedSet orderedSetWithArray:arr_email];
        arr_email = [[orderedSetE array] mutableCopy];
        [[ContactAction sharedContacts] deleteSingleContact:temp_contact WithCompletionHandler:^(CNContact *contact, BOOL completed) {
            NSLog(@"Contact Deleted Successfully");
        }];
    }];
    
    NSMutableArray *arrP = [[NSMutableArray alloc]init];
    [arr_phn enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CNPhoneNumber *phn = [[CNPhoneNumber alloc]initWithStringValue:arr_phn[idx]];
        CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:phn];
        [arrP addObject:homePhone];
    }];
    newContact.phoneNumbers = [arrP copy];
    NSMutableArray *arrE = [[NSMutableArray alloc]init];
    [arr_email enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CNLabeledValue *homeEmail = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:arr_email[idx]];
        [arrE addObject:homeEmail];
    }];
    newContact.emailAddresses = [arrE copy];
    [[ContactAction sharedContacts]saveContact:newContact];
    completionHandler([newContact copy], YES);
}

-(void)mergeDuplicateContactByNumber:(NSDictionary *)dict WithCompletionHandler:(void(^)(BOOL completed))completionHandler
{
    NSArray *arr = [dict objectForKey:@"number"];
    NSString *number = [dict objectForKey:@"dup_num"];
    
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dict = [arr objectAtIndex:idx];
        if(idx != 0)
        {
            CNContact *contact = [dict objectForKey:@"contact"];
            CNMutableContact * tempContact = [contact mutableCopy];
            if(tempContact.phoneNumbers.count >1)
            {
                NSMutableArray *arr_phn = [[dict objectForKey:@"phoneNumbers"] mutableCopy];
                
                if([arr_phn containsObject:number])
                    [arr_phn removeObject:number];
                NSMutableArray *arrP = [[NSMutableArray alloc]init];
                
                [arr_phn enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    CNPhoneNumber *phn = [[CNPhoneNumber alloc]initWithStringValue:arr_phn[idx]];
                    CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:phn];
                    [arrP addObject:homePhone];
                }];
                tempContact.phoneNumbers = [arrP copy];
                [[ContactAction sharedContacts] updateContact:tempContact];
            }else{
                //                if(tempContact.emailAddresses.count == 0 && tempContact.note.length <= 0 && tempContact.birthday == nil)
                if(tempContact.emailAddresses.count == 0 && tempContact.birthday == nil)
                    [[ContactAction sharedContacts] deleteSingleContact:contact WithCompletionHandler:^(CNContact *contact, BOOL completed) {
                    }];
                else{
                    tempContact.phoneNumbers = @[];
                    [[ContactAction sharedContacts] updateContact:tempContact];
                }
            }
        }
    }];
    completionHandler(YES);
}

-(void)mergeDuplicateContactByEmail:(NSDictionary *)dict WithCompletionHandler:(void(^)(BOOL completed))completionHandler
{
    NSArray *arr = [dict objectForKey:@"email"];
    NSString *number = [dict objectForKey:@"dup_email"];
    
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dict = [arr objectAtIndex:idx];
        if(idx != 0)
        {
            CNContact *contact = [dict objectForKey:@"contact"];
            CNMutableContact * tempContact = [contact mutableCopy];
            if(tempContact.emailAddresses.count >1)
            {
                NSMutableArray *arr_phn = [[dict objectForKey:@"emailAddresses"] mutableCopy];
                
                if([arr_phn containsObject:number])
                    [arr_phn removeObject:number];
                NSMutableArray *arrP = [[NSMutableArray alloc]init];
                
                [arr_phn enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    //                    CNPhoneNumber *phn = [[CNPhoneNumber alloc]initWithStringValue:arr_phn[idx]];
                    CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:arr_phn[idx]];
                    [arrP addObject:homePhone];
                }];
                tempContact.emailAddresses = [arrP copy];
                [[ContactAction sharedContacts] updateContact:tempContact];
            }else{
                //                if(tempContact.phoneNumbers.count == 0 && tempContact.note.length <= 0 && tempContact.birthday == nil)
                if(tempContact.phoneNumbers.count == 0 && tempContact.birthday == nil)
                    [[ContactAction sharedContacts] deleteSingleContact:contact WithCompletionHandler:^(CNContact *contact, BOOL completed) {
                    }];
                else{
                    tempContact.phoneNumbers = @[];
                    [[ContactAction sharedContacts] updateContact:tempContact];
                }
            }
        }
    }];
    completionHandler(YES);
}
@end
