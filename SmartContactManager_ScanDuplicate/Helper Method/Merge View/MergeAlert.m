//
//  MergeAlert.m
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 29/12/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "MergeAlert.h"
#import "DuplicateCell.h"
@interface MergeAlert()<UITableViewDelegate, UITableViewDataSource>
{
    NSArray *arr_data;
    NSString *common ;
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_msg;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *btn_Yes;
@property (strong, nonatomic) IBOutlet UIButton *btn_No;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_tableHeight;

@end


@implementation MergeAlert

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self customInit];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self customInit];
    }
    return self;
}

-(void)customInit
{
    UIView *view=[[[NSBundle bundleForClass:[self class]] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    [self addSubview:view];
    CGRect frame=self.bounds;
    view.frame=frame;
    [self layoutIfNeeded];
    [self setNeedsLayout];
    [self.tableView registerNib:[UINib nibWithNibName:@"DuplicateCell" bundle:nil] forCellReuseIdentifier:@"CellID"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

#pragma mark - tableview method
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arr_data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DuplicateCell *cell = (DuplicateCell *)[tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if(!cell)
        cell = [[DuplicateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
    NSDictionary *dict = [arr_data objectAtIndex:indexPath.row];
    cell.lbl_name.text = [dict objectForKey:@"name"];
    NSArray *phone = [[dict objectForKey:@"phoneNumbers"] copy];
    NSArray *email = [[dict objectForKey:@"emailAddresses"] copy];
    cell.lbl_detail.text = (phone.count > 0) ? [phone firstObject] : (email.count > 0) ? [email firstObject] : @"No data";
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

#pragma mark - IBACTIN

- (IBAction)btn_yes:(id)sender {
//    [self removeFromSuperview];
    if([self.delegate respondsToSelector:@selector(btn_YesAlert:)])
        [_delegate btn_YesAlert:_index];
}

- (IBAction)btn_No:(id)sender {
    if([self.delegate respondsToSelector:@selector(btn_CancelAlert)])
        [_delegate btn_CancelAlert];
    
}

#pragma mark - Helper Method
-(void)callReloadCell:(NSDictionary*)dict andKey:(NSString *)key
{
    self.dict_contact = dict;
    self.key = key;
    NSLog(@"[dict allKeys] %@",[dict allKeys]);
    NSLog(@"[[dict allKeys]firstObject] %@",[[dict allKeys]lastObject]);
    NSString *  str = [[dict allKeys]lastObject];
    
    common= [NSString stringWithFormat:@"%@",[dict objectForKey:str]];
    arr_data = [[dict objectForKey:key] copy];
    [self.tableView reloadData];
}
@end
