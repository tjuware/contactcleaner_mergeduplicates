//
//  HttpHelper.m
//  JSonDemo
//
//  Copyright (c) 2012 dharni. All rights reserved.
//

#import "HttpHelper.h"
#import "SVProgressHUD.h"


int reloadUrlCount;

@implementation HttpHelper

@synthesize dict;
@synthesize data;
@synthesize startloading;
@synthesize xmldata;
@synthesize delegate;

-(id) initWithMUrl:(NSString *) serverurl Dictionary:(NSDictionary *) valuesPairs isloading:(bool)loading api_number:(int)request_api_number message:(NSString *)message{
    self=[super init];
    if(self){
        @autoreleasepool {
            self.dict = [[NSMutableDictionary alloc] init];
//            app =(AppDelegate *) [[UIApplication sharedApplication]delegate];
            self.startloading=loading;
            api_number=request_api_number;
            
            if (loading) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
                    [SVProgressHUD showWithStatus:message];
                });
            }
            
            NSMutableString *values;
            int i=0;
            
            for (id key in valuesPairs) {
                if(i==0){
                    values=[NSMutableString stringWithFormat:@"%@=%@",key,[valuesPairs objectForKey:key]];
                }else{
                    [values appendString:[NSMutableString stringWithFormat:@"&%@=%@",key,[valuesPairs objectForKey:key] ]];
                }
                i++;
            }
            
            
            
            NSLog(@"value pairs %@",values);
            [UIApplication sharedApplication]. networkActivityIndicatorVisible=YES;
            NSURL *url=[NSURL URLWithString:serverurl];
            // NSURLRequest *request=[NSURLRequest requestWithURL:url];
            NSString *post = values;
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            [request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            urlConnection=[[NSURLConnection alloc]initWithRequest:request delegate:self];
            
            
           /* NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
            
            NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (!error) {
                    self.data = [data mutableCopy];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                    });
                    self.dict=(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:self.data options:0 error:nil];
                    
                    self.startloading=false;
                    xmldata = [[NSMutableString alloc] initWithData:self.data encoding:NSASCIIStringEncoding];
                    [self.delegate setBackApiResponse:self.dict api_number:api_number];
                    NSLog(@"inside connection %@",xmldata);
                    //[SVProgressHUD dismiss];
                }else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                      //  [SVProgressHUD dismiss];
                        [SVProgressHUD showErrorWithStatus:@"No internet Connection."];
                        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                        if (api_number==7 || api_number==8) {
                            [self.delegate setServerTimeOut:api_number];
                        }
                    });
                }
            }];
            [dataTask resume];*/
        }
    }
    return  self;
}
-(id) initWithMUrl:(NSString *) serverurl Dictionary:(NSDictionary *) valuesPairs isloading:(bool)loading {
   // //NSLog(@"serverurl : %@",serverurl);
    self=[super init];
    if(self){
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
            [SVProgressHUD showWithStatus:@"Loading..."];
        });
        
        self.dict = [[NSMutableDictionary alloc] init];
        // mainView=(MainViewController *)[[UIApplication  sharedApplication]delegate];
//        app =(AppDelegate *) [[UIApplication sharedApplication]delegate];
       self.startloading=loading;
        api_number=0;
        NSMutableString *values;
        int i=0;
        
        for (id key in valuesPairs) {
            if(i==0){
                values=[NSMutableString stringWithFormat:@"%@=%@",key,[valuesPairs objectForKey:key]];
            }else{
                [values appendString:[NSMutableString stringWithFormat:@"&%@=%@",key,[valuesPairs objectForKey:key] ]];
            }
            i++;
        }
      //  //NSLog(@"value pairs %@",values);
        [UIApplication sharedApplication]. networkActivityIndicatorVisible=YES;
        NSURL *url=[NSURL URLWithString:serverurl];
        // NSURLRequest *request=[NSURLRequest requestWithURL:url];
        NSString *post = values;
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        urlConnection=[[NSURLConnection alloc]initWithRequest:request delegate:self];
        
        /*[NSURLConnection sendAsynchronousRequest:request queue:[[[NSOperationQueue alloc] init] autorelease] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
         //NSLog(@"response %@",response);
         }];*/
    }
    return  self;

}
-(id) initWithMOnlyUrl:(NSString *)serverurl isloading:(bool)loading{
    self=[super init];
    if(self){
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
            [SVProgressHUD showWithStatus:@"Loading..."];
//            [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
        });
//        app =(AppDelegate *) [[UIApplication sharedApplication]delegate];
        self.startloading=loading;
        [UIApplication sharedApplication]. networkActivityIndicatorVisible=YES;
        NSURL *url=[NSURL URLWithString:serverurl];
        // NSURLRequest *request=[NSURLRequest requestWithURL:url];
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"GET"];
        [request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
       urlConnection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
        
        
    }
    return  self;

}
-(id) initWithMOnlyUrl:(NSString *)serverurl isloading:(bool)loading api_number:(int)request_api_number message:(NSString *)message{
    self=[super init];
    if(self){
    //    [SVProgressHUD showWithStatus:message maskType:SVProgressHUDMaskTypeGradient];//commented on 26-06-18
//        app =(AppDelegate *) [[UIApplication sharedApplication]delegate];
        self.startloading=loading;
        [UIApplication sharedApplication]. networkActivityIndicatorVisible=YES;
        NSURL *url=[NSURL URLWithString:serverurl];
        api_number=request_api_number;
        NSString *post = @"";
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"GET"];
        [request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        urlConnection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
        
        
    }
    return  self;
}
-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
   // //NSLog(@"didReceiveResponse");
    self.data=[[NSMutableData alloc]init];
    
}
-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)thedata{
   // //NSLog(@"data %@",data);
    [self.data appendData:thedata];
   // [self.data retain];
}
-(void) connectionDidFinishLoading:(NSURLConnection *)connectio{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    self.dict=(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:self.data options:0 error:nil];
    
    self.startloading=false;
    xmldata = [[NSMutableString alloc] initWithData:self.data encoding:NSASCIIStringEncoding];
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
    
    NSLog(@"inside connection %@",xmldata);
    if([self.delegate respondsToSelector:@selector(setBackApiResponse:api_number:)])
        [self.delegate setBackApiResponse:self.dict api_number:api_number];
}
-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    //NSLog(@"serverError : %@",error);
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
    if (reloadUrlCount<=2) {
        reloadUrlCount++;
        NSMutableURLRequest *request=[[connection currentRequest] mutableCopy];
        NSURLConnection *conn=[[NSURLConnection alloc]initWithRequest:request delegate:self startImmediately:YES];
        NSLog(@"connection %@",conn);
    }else{
        if ([self.delegate respondsToSelector:@selector(responseError:api_number:)]) {
            [self.delegate responseError:error api_number:api_number];
        }else
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
            if (api_number==7 || api_number==8) {
                [self.delegate setServerTimeOut:api_number];
            }
        }
    }
}
-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
    NSMutableDictionary *mutableUserInfo = [[cachedResponse userInfo] mutableCopy];
    NSMutableData *mutableData = [[cachedResponse data] mutableCopy];
    NSURLCacheStoragePolicy storagePolicy = NSURLCacheStorageAllowedInMemoryOnly;
    
    // ...
    //NSLog(@"Cached data %@",cachedResponse);
    return [[NSCachedURLResponse alloc] initWithResponse:[cachedResponse response]
                                                    data:mutableData
                                                userInfo:mutableUserInfo
                                           storagePolicy:storagePolicy];
}
-(void) dealloc{
    [self setData:nil];
    [self setDict:nil];
    [self setStartloading:nil];
    [self setXmldata:nil];
   // [data release];
   // [dict release];
   // [xmldata release];
    
   // [super dealloc];
}
@end
