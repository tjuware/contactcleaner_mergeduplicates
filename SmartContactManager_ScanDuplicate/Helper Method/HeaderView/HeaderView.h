//
//  HeaderView.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 27/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HeaderViewDelegate <NSObject>
@optional
-(void)btnLeftClicked;
-(void)btnRigtClicked;
@end
//IB_DESIGNABLE
@interface HeaderView : UIView
@property(weak,nonatomic)id <HeaderViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_right;

//@property(assign,nonatomic)IBInspectable NSString * title;
//
//@property(assign,nonatomic)IBInspectable UIColor * rightColor;

-(void)changeRightIconImage:(UIImage *)image;
-(void)changeLeftIconImage:(UIImage *)image;
-(void)changeBackgroundColor:(UIColor *)color;
-(void)setTitle:(NSString *)title;
-(void)changeRightTitle:(NSString *)str withColor:(UIColor *)color;
-(void)setTitleColor:(UIColor *)color;
-(void)changeRightTitle:(NSString *)str;
-(void)hideViewLeft;
@end
