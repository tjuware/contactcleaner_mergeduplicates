//
//  Constant.h
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 14/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <Foundation/Foundation.h>
#define FamilyBudBaseURL @"http://www.familybuds.com/cellphonemanager/" //FamilyBuds
#define prodBaseURL @"https://www.techathalon.com/cellphonemanager/" //Techathalon(Abdul bhai server)
#define prodPrivacyBaseURL @"https://techathalon.com/privacypolicy/"

static NSString *privacyPath = prodPrivacyBaseURL @"?app_id=6";

static NSString *uploadPath = prodBaseURL @"public/uploads/contacts/";
static NSString *faqPath = prodBaseURL @"ApiHomeNewApp/faq";

static int uploadTextAPI = 1;
static NSString *uploadTextURL = prodBaseURL @"ApiHomeNewApp/do_contact_backup_upload_oc/2/ios/2";

static int getBackupAPI = 2;
static NSString *getBackupURL = prodBaseURL @"ApiHomeNewApp/get_previous_contact_backup/2/ios/2";

static int deleteBackupAPI = 3;
static NSString *deleteBackupURL = prodBaseURL @"ApiHomeNewApp/deleteBackedUpdata/2/ios/2";

static int sendEmailAPI = 4;
static NSString *sendEmailURL = prodBaseURL @"ApiHomeNewApp/sendBackupAgain_oc/2/ios/2";

static int registerUserAPI = 5;
static NSString *registerUserURL = prodBaseURL @"ApiHomeNewApp/registerUser/2/ios/2";

static int checkPasswordAPI = 6;
static NSString *checkPasswordURL = prodBaseURL @"ApiHomeNewApp/checkPassword/2/ios/2";

static int changePasswordAPI = 7;
static NSString *changePasswordURL = prodBaseURL @"ApiHomeNewApp/changePassword/2/ios/2";

static int sendPasswordAPI = 8;
static NSString *sendPasswordURL = prodBaseURL @"ApiHomeNewApp/sendPassword_oc/2/ios/2";

static int savePasswordAPI = 9;
static NSString *savePasswordURL = prodBaseURL @"ApiHomeNewApp/savePassword/2/ios/2";

static int addRewardAPI = 15;
static NSString *addRewardURL = prodBaseURL @"ApiHomeNewApp/addReward/2/ios/2";

static int claimRewardAPI = 16;
static NSString *claimRewardURL = prodBaseURL @"ApiHomeNewApp/subtractReward/2/ios/2";

//static NSString *uploadPath = @"http://www.familybuds.com/cellphonemanager/public/uploads/contacts/";
//static NSString *uploadPath = @"https://gathademo.com/cellphonemanager/public/uploads/contacts/";

//static NSString *privacyPath = @"https://gathademo.com/privacypolicy/?app_id=6";


//static NSString *faqPath = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/faq";
//static NSString *faqPath = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/faq";

//static int uploadTextAPI = 1;
//static NSString *uploadTextURL=@"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/do_contact_backup_upload_oc/2/ios/2";
//static NSString *uploadTextURL=@"https://techathalon.com/cellphonemanager/ApiHomeNewApp/do_contact_backup_upload_oc/2/ios/2";

//static int getBackupAPI = 2;
//static NSString *getBackupURL = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/get_previous_contact_backup/2/ios/2";
//static NSString *getBackupURL = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/get_previous_contact_backup/2/ios/2";

//static int deleteBackupAPI = 3;
//static NSString *deleteBackupURL = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/deleteBackedUpdata/2/ios/2";
//static NSString *deleteBackupURL = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/deleteBackedUpdata/2/ios/2";

//static int sendEmailAPI = 4;
//static NSString *sendEmailURL = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/sendBackupAgain_oc/2/ios/2";
//static NSString *sendEmailURL = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/sendBackupAgain_oc/2/ios/2";

//static int registerUserAPI = 5;
//static NSString *registerUserURL = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/registerUser/2/ios/2";
//static NSString *registerUserURL = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/registerUser/2/ios/2";

//static int checkPasswordAPI = 6;
//static NSString *checkPasswordURL = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/checkPassword/2/ios/2";
//static NSString *checkPasswordURL = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/checkPassword/2/ios/2";

//static int changePasswordAPI = 7;
//static NSString *changePasswordURL = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/changePassword/2/ios/2";
//static NSString *changePasswordURL = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/changePassword/2/ios/2";

//static int sendPasswordAPI = 8;
//static NSString *sendPasswordURL = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/sendPassword_oc/2/ios/2";
//static NSString *sendPasswordURL = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/sendPassword_oc/2/ios/2";

//static int savePasswordAPI = 9;
//static NSString *savePasswordURL = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/savePassword/2/ios/2";
//static NSString *savePasswordURL = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/savePassword/2/ios/2";


//static int addRewardAPI = 15;
//static NSString *addRewardURL = @"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/addReward/2/ios/2";
//static NSString *addRewardURL = @"https://techathalon.com/cellphonemanager/ApiHomeNewApp/addReward/2/ios/2";

//static int claimRewardAPI = 16;
//static NSString *claimRewardURL =@"http://www.familybuds.com/cellphonemanager/ApiHomeNewApp/subtractReward/2/ios/2";
//static NSString *claimRewardURL =@"https://techathalon.com/cellphonemanager/ApiHomeNewApp/subtractReward/2/ios/2";

@interface Constant : NSObject


@end
