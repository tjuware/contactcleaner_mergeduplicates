//
//  SliderMenuView.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 29/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SliderMenuDelegate <NSObject>
@optional
-(void)btn_closeMenu:(id)sender;
-(void)handleMenuSelection:(NSInteger)tag;
@end

@interface SliderMenuView : UIView
@property(weak,nonatomic)id <SliderMenuDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) NSMutableArray *array_icon;
@property (strong, nonatomic) NSMutableArray *array_title;
@property (strong, nonatomic) IBOutlet UIView *view_top;
-(void)hideMenu;
-(void)newShow:(SliderMenuView *)view;
-(void)setoption:(NSMutableArray *)arr;
-(void)hideEmail;
-(void)setEmail:(NSString *)email;

@end
