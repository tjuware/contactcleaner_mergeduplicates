//
//  SliderMenuView.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 29/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "SliderMenuView.h"
#import "ContactCell.h"
#import "MasterVC.h"
//#import "PlistHandler.h"
#define CONTACTCELL_ID  @"ContactCellID"
#define Bg_color [UIColor colorWithRed:16.0/255.0 green:25.0/255.0 blue:139.0/255.0 alpha:0.8]
#define FRAMEWIDTH [[UIScreen mainScreen] bounds].size.width
#define FRAMEHEIGHT [[UIScreen mainScreen] bounds].size.height
@interface SliderMenuView ()<UITableViewDelegate, UITableViewDataSource, ContactCellDelegate>
@property (strong, nonatomic) IBOutlet UIView *greyView;
@property (strong, nonatomic) IBOutlet UIView *view_tableview;
@property (strong, nonatomic) IBOutlet UILabel *lbl_email;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_emailHeight;



@end

@implementation SliderMenuView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self customInit];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self customInit];
    }
    return self;
}

-(void)customInit
{
    UIView *view=[[[NSBundle bundleForClass:[self class]] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    [self addSubview:view];
    CGRect frame=self.bounds;
    view.frame=frame;
    [self layoutIfNeeded];
    [self setNeedsLayout];
    [self.tableview registerNib:[UINib nibWithNibName:@"ContactCell" bundle:nil]
        forCellReuseIdentifier:CONTACTCELL_ID];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self.greyView setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.2]];
    [[MasterVC sharedInstance] performSelector:@selector(shadowHeaderViewWithAplha:) withObject:self.view_tableview afterDelay:0.5];

}
-(void)setoption:(NSMutableArray *)arr
{
    self.array_title = arr;
    [self.tableview reloadData];
}

- (IBAction)btn_sideSpace:(id)sender {
//    [self.delegate btn_closeMenu:sender];
    [self hideMenu];
}
#pragma mark - TableviewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.array_title.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *dict = [[_array_title objectAtIndex:section] copy];
    NSArray *arr = [[dict objectForKey:@"SubIndex"] copy];
    if(section == 2 && [[dict objectForKey:@"isSelect"] isEqualToString:@"1"])
    {
        return arr.count+1;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactCell *cell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:CONTACTCELL_ID];
    if(!cell)
        cell = [[ContactCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CONTACTCELL_ID];
    
//    cell.delegate_cell = self;
    cell.backgroundColor = [UIColor clearColor];
    NSDictionary *dict = [_array_title objectAtIndex:indexPath.section];
    NSString *name = [dict objectForKey:@"Name"];
    //    NSString *img_name = [_array_icon objectAtIndex:indexPath.row];
    
    NSString *img_name = [dict objectForKey:@"Image"];
    cell.image.image = [UIImage imageNamed:img_name];
    cell.image.contentMode = UIViewContentModeScaleAspectFit;
    cell.lbl_name.text = name;
    cell.lbl_name.textColor = [UIColor whiteColor];
    cell.image.hidden = NO;
    [cell showIMGCheck:(indexPath.section == 2) ? YES : NO forCell:cell];
    cell.cnst_left.constant = 16;
    
    //    cell.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:1.0];
    cell.cnst_right.constant = 0;
    NSArray *subIndex =  [[dict objectForKey:@"SubIndex"] copy];
    if(subIndex.count != 0 && indexPath.row !=0 && [[dict objectForKey:@"isSelect"] integerValue]== 1)
    {
        NSDictionary *dict1 = [subIndex objectAtIndex:indexPath.row -1];
        NSString *name1 = [dict1 objectForKey:@"Name"];
        NSString *img_name1 = [dict1 objectForKey:@"Image"];
        
        cell.lbl_name.text = name1;
        cell.image.image = [UIImage imageNamed:img_name1];
        cell.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.55];
        cell.cnst_left.constant = 32;
    }else if(indexPath.section == 2){
        [cell showIMGCheck:YES forCell:cell];
        [ cell.img_check setImage:[UIImage imageNamed:([[dict objectForKey:@"isSelect"] integerValue] == 1) ? @"UpArrow"  : @"DownArrow"]];
        cell.cnst_right.constant = 15;
        
    }
    
    [cell setNeedsLayout];
    [self layoutSubviews];
    cell.tag = indexPath.section;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    ContactCell *cell = [self.tableview cellForRowAtIndexPath:indexPath];
//    cell.index = indexPath.section;
//    if(indexPath.section == 2){
//        ContactCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//        [cell showIMGCheck:YES];
//        NSDictionary *dict = [_array_title objectAtIndex:indexPath.section];
//        [ cell.img_check setImage:[UIImage imageNamed:([[dict objectForKey:@"isSelect"] integerValue] == 1) ? @"UpArrow"  : @"DownArrow"]];
//        cell.cnst_right.constant = 15;
//    }
    NSMutableDictionary *dict = [_array_title objectAtIndex:indexPath.section];
    if([self.delegate respondsToSelector:@selector(handleMenuSelection:)] && !([[dict objectForKey:@"id"] isEqualToString:@"3"]))
        [self.delegate handleMenuSelection:[[dict objectForKey:@"id"] integerValue]];
    else{
        if(indexPath.row != 0){
            NSDictionary *temp = [[[dict objectForKey:@"SubIndex"] copy]objectAtIndex:indexPath.row-1];
            [self.delegate handleMenuSelection:[[temp objectForKey:@"id"] integerValue]];
        }else{
            [dict setObject:([[dict objectForKey:@"isSelect"] integerValue] == 1) ? @"0" : @"1" forKey:@"isSelect"];
            [self.array_title replaceObjectAtIndex:indexPath.section withObject:dict];
            [tableView reloadData];
        }
    }
}
-(void)hideMenu
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setFrame:CGRectMake(-500, FRAMEHEIGHT/10, FRAMEWIDTH, FRAMEHEIGHT - ((FRAMEHEIGHT/10)*1))];

        [UIView animateWithDuration:0.5f animations:^{
            [self layoutIfNeeded];
            [self layoutSubviews];
//            [self setFrame:CGRectMake(-500, FRAMEHEIGHT/10, FRAMEWIDTH, FRAMEHEIGHT - ((FRAMEHEIGHT/10)*1))];
        } completion:^(BOOL finished) {
//            [self removeFromSuperview];
        }];
    });
}

-(void)newShow:(SliderMenuView *)view
{
    dispatch_async(dispatch_get_main_queue(), ^{
//        [view setFrame:CGRectMake(-500, FRAMEHEIGHT/10, FRAMEWIDTH, FRAMEHEIGHT -((FRAMEHEIGHT/10)*1))];
        [self.view_top setBackgroundColor:Bg_color];
        [self.tableview reloadData];
        NSLog(@"%@", [self subviews]);
        [self setFrame:CGRectMake(0, FRAMEHEIGHT/10, FRAMEWIDTH, FRAMEHEIGHT -((FRAMEHEIGHT/10)*1))];

        [UIView animateWithDuration:0.3f animations:^{
            [self layoutIfNeeded];
            [self layoutSubviews];
        }];
    });
    
}

-(void)hideEmail
{
    _cnst_emailHeight.constant = 0;
    _lbl_email.hidden = YES;
    [self layoutIfNeeded];
}

-(void)setEmail:(NSString *)email
{
    _lbl_email.text = email;
    _lbl_email.hidden = NO;
    _cnst_emailHeight.constant = 50;
    [self layoutIfNeeded];
}
@end
