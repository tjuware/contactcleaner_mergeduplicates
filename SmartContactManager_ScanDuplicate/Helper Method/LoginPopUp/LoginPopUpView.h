//
//  LoginPopUpView.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 17/11/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterView.h"
#import "ACFloatingTextField.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@protocol LoginPopUpDelegate <NSObject>
@optional
-(void)btn_close:(int)sender;
-(void)signInClick:(NSString *)txtEmail;
-(void)signInWithPassword:(NSString *)txt_pass withEmail:(NSString *)txtEmail;
-(void)btn_skip;
-(void)btn_GoogleLogin;

@end

@interface LoginPopUpView : MasterView<UITextFieldDelegate>
@property(weak,nonatomic)id <LoginPopUpDelegate> delegate;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_email;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_newPass;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_cnfpass;

@property (strong, nonatomic) IBOutlet UIView *view_fb;
@property (strong, nonatomic) IBOutlet UIView *view_gp;
@property (strong, nonatomic) IBOutlet UIButton *btn_gp;
@property (strong, nonatomic) IBOutlet UIButton *btn_signIn;
@property (strong, nonatomic) IBOutlet UIView *view_signIn;
@property (strong, nonatomic) IBOutlet UIView *view_newPass;
@property (strong, nonatomic) IBOutlet UIView *view_cnfPass;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_passHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_cnfHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_newHeifht;
@property (strong, nonatomic) UIViewController *parentVC;
@property (assign) BOOL isUserRegister;
- (IBAction)btn_fb:(id)sender;
- (IBAction)btn_SignIn:(id)sender;
- (IBAction)btn_gp:(id)sender;


-(void)viewWithOutPassword;
-(void)viewWithPassword;
-(void)viewWithPassword:(NSString *)email;

@end
