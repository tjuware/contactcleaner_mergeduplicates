//
//  AlertControllerDesign.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 11/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "AlertControllerDesign.h"
#import "AppDelegate.h"
#import "MasterVC.h"
//#import "PlistHandler.h"

@interface AlertControllerDesign()<UITextFieldDelegate>
{
    UIAlertController *loginAlertController;
    AppDelegate *app;
}

@end
@implementation AlertControllerDesign
+ (id)sharedObject {
    static AlertControllerDesign *sharedMyViews = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //for first time create shared instance object
        sharedMyViews = [[self alloc] init];
    });
    
    
    return sharedMyViews;
}

-(void)presentActivityController
{
//    NSString *url=@"https://itunes.apple.com/us/app/id1032947867";
    NSString *url=@"https://apps.apple.com/in/app/contact-cleaner-merge-email/id1444870653";
    NSString * title =[NSString stringWithFormat:@"The most secure contact backup. Download Smart Contact Manager - Backup app %@",url];
   
    NSArray *objectsToShare = @[title];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    [activityVC setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed,  NSArray *returnedItems, NSError *activityError) {
    }];
    
    NSArray * excludeActivities = @[
                                    UIActivityTypeAirDrop,
                                    UIActivityTypePrint,
                                    UIActivityTypeAssignToContact,
                                    UIActivityTypeSaveToCameraRoll,
                                    UIActivityTypeAddToReadingList,
                                    UIActivityTypePostToFlickr,
                                    UIActivityTypePostToVimeo,
                                    UIActivityTypePostToWeibo,
                                    UIActivityTypePostToTencentWeibo,
                                    UIActivityTypeOpenInIBooks];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
//    [app.navController presentViewController:activityVC animated:YES completion:nil];
    [self presentController:activityVC animated:YES];
}


-(void)alertControllerForLoginWithCompletionHandler:(void(^)(NSMutableDictionary *dict, BOOL completed))completionHandler{
//    app =(AppDelegate *) [[UIApplication sharedApplication]delegate];

    loginAlertController = [UIAlertController alertControllerWithTitle: @"Login" message: @"Please login to take backup.\n By entering EmailId." preferredStyle: UIAlertControllerStyleAlert];
    [loginAlertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Email id";
        textField.textColor = [UIColor darkGrayColor];
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        [textField addTarget:self action:@selector(usernameTextfield:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    [loginAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *txt = [self->loginAlertController.textFields firstObject];
        NSDictionary *nameValuePairs=[NSDictionary dictionaryWithObjectsAndKeys: txt.text, @"email_id", @"", @"phoneNO", @"", @"gender", nil];
        completionHandler([nameValuePairs mutableCopy], YES);
    }]];
    
    [loginAlertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler(nil, NO);
    }]];
    UIAlertAction *action = [loginAlertController.actions firstObject];
    [action setEnabled:NO];
    [self presentController:loginAlertController animated:YES];
//    [app.navController presentViewController:loginAlertController animated:YES completion:nil];
}
-(void)usernameTextfield:(UITextField*)txt
{
    UIAlertAction *action = [loginAlertController.actions firstObject];
    if([[MasterVC sharedInstance] NSStringIsValidEmail:txt.text])
    {
        [action setEnabled:YES];
    }else{
        [action setEnabled:NO];
    }
    NSLog(@"%@",txt.text);
}

-(void)alertControllerForChangePasswordWithCompletionHandler:(void(^)(NSMutableDictionary *dict, BOOL completed))completionHandler{
//    app =(AppDelegate *) [[UIApplication sharedApplication]delegate];
    __weak typeof(self) weakSelf = self;
    
    loginAlertController = [UIAlertController alertControllerWithTitle: @"Change Password" message: @"User can change password" preferredStyle: UIAlertControllerStyleAlert];
    [loginAlertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Old Password";
        textField.textColor = [UIColor darkGrayColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.delegate = weakSelf;
        textField.textContentType = UITextContentTypePassword;
        [textField addTarget:weakSelf action:@selector(changePasswordHelper:) forControlEvents:UIControlEventEditingChanged];
    }];
    [loginAlertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"New Password";
        textField.textColor = [UIColor darkGrayColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.textContentType = UITextContentTypePassword;
        
        [textField addTarget:weakSelf action:@selector(changePasswordHelper:) forControlEvents:UIControlEventEditingChanged];
        textField.delegate = weakSelf;
    }];
    [loginAlertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Re-Enter New Password";
        textField.textColor = [UIColor darkGrayColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.textContentType = UITextContentTypePassword;
        
        [textField addTarget:weakSelf action:@selector(changePasswordHelper:) forControlEvents:UIControlEventEditingChanged];
        textField.delegate = weakSelf;
    }];
    [loginAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *txt_old = [self->loginAlertController.textFields firstObject];
        UITextField *txt_new = [self->loginAlertController.textFields objectAtIndex:1];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys: txt_old.text,@"old_pswd",txt_new.text,@"new_pswd", nil];
        completionHandler(dict, YES);
        
    }]];
    [loginAlertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
    }]];
    UIAlertAction *action = [loginAlertController.actions firstObject];
    [action setEnabled:NO];
    
//    [app.navController presentViewController:loginAlertController animated:YES completion:nil];
    [self presentController:loginAlertController animated:YES];
}

-(void)changePasswordHelper:(UITextField*)txt
{
    UIAlertAction *action = [loginAlertController.actions firstObject];
    UITextField *txt_oldPass =[loginAlertController.textFields objectAtIndex:0];
    UITextField *txt_newPass = [loginAlertController.textFields objectAtIndex:1];
    UITextField *txt_confPass = [loginAlertController.textFields objectAtIndex:2];
    [action setEnabled:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![txt_newPass.text isEqualToString:txt_confPass.text] && txt_confPass.text.length == 4 && txt_newPass.text.length == 4 ) {
            [SVProgressHUD showErrorWithStatus:@"Pin does not match"];
        }else if ([txt_newPass.text isEqualToString:txt_oldPass.text]) {
            [SVProgressHUD showErrorWithStatus:@"New pin matches the old pin"];
        }
        else{
            [action setEnabled:YES];
        }
    });
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSInteger length = [currentString length];
    if (length > 4) {
        return NO;
    }
    return YES;
}

-(void)alertControllerForLogoutWithCompletionHandler:(void(^)(BOOL completed))completionHandler{
//    app =(AppDelegate *) [[UIApplication sharedApplication]delegate];
//    __weak typeof(self) weakSelf = self;
    
    loginAlertController = [UIAlertController alertControllerWithTitle: @"Logout" message: @"Are you sure you want to logout?" preferredStyle: UIAlertControllerStyleAlert];
    [loginAlertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler(YES);
    }]];
    [loginAlertController addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler(NO);
    }]];
//    [app.navController presentViewController:loginAlertController animated:YES completion:nil];
    [self presentController:loginAlertController animated:YES];
}

-(void)alertController:(NSString *)title withMessage:(NSString *)message andButtonOne:(NSString *)btn1 andButtontwo:(NSString *)btn2 WithCompletionHandler:(void(^)(BOOL completed))completionHandler
{
//    app =(AppDelegate *) [[UIApplication sharedApplication]delegate];
    loginAlertController = [UIAlertController alertControllerWithTitle: title message: message preferredStyle: UIAlertControllerStyleAlert];
    [loginAlertController addAction:[UIAlertAction actionWithTitle:(btn1.length > 0) ? btn1 : @"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler(YES);
    }]];
    if(btn2.length > 0){
        [loginAlertController addAction:[UIAlertAction actionWithTitle:btn2 style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            completionHandler(NO);
        }]];
    }
    [self presentController:loginAlertController animated:YES];

//    [app.navController presentViewController:loginAlertController animated:YES completion:nil];
}

-(void)alertControllerForSavePasswordWithCompletionHandler:(void(^)(NSMutableDictionary* dict, BOOL completed))completionHandler{
//    app =(AppDelegate *) [[UIApplication sharedApplication]delegate];
    __weak typeof(self) weakSelf = self;
    loginAlertController = [UIAlertController alertControllerWithTitle: @"Info" message: @"You have not saved your password yet" preferredStyle: UIAlertControllerStyleAlert];
    [loginAlertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter New Password";
        textField.textColor = [UIColor darkGrayColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.textContentType = UITextContentTypePassword;
        textField.delegate = weakSelf;
    }];
    
    [loginAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *txt = [self->loginAlertController.textFields firstObject];
        NSMutableDictionary *valuepairs=[NSMutableDictionary dictionaryWithObjectsAndKeys:txt.text,@"password",@"",@"registered_email",nil];
        completionHandler(valuepairs, YES);
    }]];
    
    [loginAlertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler(nil, NO);
    }]];
    UIAlertAction *action = [loginAlertController.actions firstObject];
    [action setEnabled:NO];
    [self presentController:loginAlertController animated:YES];
//    [app.navController presentViewController:loginAlertController animated:YES completion:nil];
}

-(void)presentController:(UIAlertController *)controller animated:(BOOL)animated{
    dispatch_async(dispatch_get_main_queue(), ^{
        self->app =(AppDelegate *) [[UIApplication sharedApplication]delegate];
        [self->app.navController presentViewController:controller animated:animated completion:nil];
    });
}
-(void)showAlertControllerWithTitle:(NSString*)title withMessage:(NSString*)message withButtons:(NSArray*)buttArray completionHandler:(void(^)(NSString* string))completionhandler{
    loginAlertController = [UIAlertController alertControllerWithTitle: title message: message preferredStyle: UIAlertControllerStyleAlert];
    for (NSString* key in buttArray){
        [loginAlertController addAction:[UIAlertAction actionWithTitle:key style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            completionhandler(key);
        }]];
    }
    [self presentController:loginAlertController animated:YES];
}

@end
