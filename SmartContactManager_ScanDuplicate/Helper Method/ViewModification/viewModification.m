//
//  viewModification.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 27/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "viewModification.h"
@implementation viewModification
+ (id)sharedViews { //Shared instance method
    
    static viewModification *sharedMyViews = nil; //create contactsList Object
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //for first time create shared instance object
        sharedMyViews = [[self alloc] init];
    });
    
    return sharedMyViews;
}

-(id)init
{
    self = [super init];
    return self;
}

-(void)changeViewtoCornerRadius:(int)radius AndBorder:(float)border_width andBorderColor:(UIColor *)color toView:(UIView *)view
{
    view.layer.borderWidth = border_width;
    view.layer.borderColor = [color CGColor];
    [self setCornerRadius:radius andView:view];
}

-(void) setCornerRadius:(int)radius andView:(UIView *)view
{
    view.layer.cornerRadius = radius;
    view.clipsToBounds = YES;
}
-(void)changeViewtoCornerRadiusWithShadow:(int)radius AndBorder:(float)border_width andBorderColor:(UIColor *)color toView:(UIView *)v andShadowRadius:(int)sRadius andShadowOpacity:(float)sLength
{
    [v.layer setCornerRadius:radius];
    
    // border
    [v.layer setBorderColor:color.CGColor];
    [v.layer setBorderWidth:border_width];
    
    // drop shadow
    [v.layer setShadowColor:[UIColor blackColor].CGColor];
    [v.layer setShadowOpacity:sLength];
    [v.layer setShadowRadius:sRadius];
    [v.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

-(void)changeViewtoCornerRadiusWithShadow:(int)radius AndBorder:(float)border_width andBorderColor:(UIColor *)color toView:(UIView *)v andShadowRadius:(int)sRadius andShadowOpacity:(float)sLength andsetShadowOffset:(CGSize)size
{
    [v.layer setCornerRadius:radius];
    [v setClipsToBounds:YES];
    // border
    [v.layer setBorderColor:color.CGColor];
    [v.layer setBorderWidth:border_width];
    
    // drop shadow
    [v.layer setShadowColor:[UIColor blackColor].CGColor];
    [v.layer setShadowOpacity:sLength];
    [v.layer setShadowRadius:sRadius];
    [v.layer setShadowOffset:CGSizeMake(10.0, 10.0)];
}

-(void)changeViewtoCornerRadiusInArray:(NSArray *)arr_radius AndBorder:(float)border_width andBorderColor:(UIColor *)color toView:(NSArray *)arr_view
{
    [arr_view enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        int radius = [[arr_radius objectAtIndex:idx] intValue];
        UIView *view = (UIView *)[arr_view objectAtIndex:idx];
        
        [self changeViewtoCornerRadius:radius AndBorder:border_width andBorderColor:color toView:view];
    }];
}

-(void)custom:(UIView *)outView
{
    outView.layer.cornerRadius = 25;
    outView.clipsToBounds = YES;
    outView.layer.shadowPath =[UIBezierPath bezierPathWithRoundedRect:outView.bounds cornerRadius:outView.layer.cornerRadius].CGPath;
    outView.layer.shadowColor = UIColor.lightGrayColor.CGColor;
    outView.layer.shadowOpacity = 0.5;
    outView.layer.shadowOffset = CGSizeMake(3,3);
    outView.layer.shadowRadius = 1;
    outView.layer.masksToBounds = NO;
}

- (void)setBackgroundGradient:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha
{
    
    [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.frame;
    
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:colorR1/255.0 green:colorG1/255.0 blue:colorB1/255.0 alpha:alpha] CGColor], (id)[[UIColor colorWithRed:colorR2/255.0 green:colorG2/255.0 blue:colorB2/255.0 alpha:alpha] CGColor], nil];
    grad.startPoint = CGPointMake(0.0, 0.5);
    grad.endPoint = CGPointMake(1.0, 0.5);
    [mainView.layer insertSublayer:grad atIndex:0];
    [mainView setClipsToBounds:YES];
}
@end
