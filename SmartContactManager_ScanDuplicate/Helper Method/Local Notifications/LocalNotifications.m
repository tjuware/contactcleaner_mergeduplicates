//
//  LocalNotifications.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 14/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "LocalNotifications.h"
#import <UserNotifications/UserNotifications.h>
#import "PlistHandler.h"
@implementation LocalNotifications
+ (id)sharedObject {
    static LocalNotifications *sharedMyViews = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //for first time create shared instance object
        sharedMyViews = [[self alloc] init];
    });
    return sharedMyViews;
}

-(void)resetLocalNotification
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//    [center removeAllPendingNotificationRequests];
    NSString *reminderSelected = [[PlistHandler sharedObject]getValueFromPlistByKey:@"reminderSelect"];
    UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
    objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"Notification!" arguments:nil];
    objNotificationContent.body = [NSString localizedUserNotificationStringForKey:@"Hey Backup time !"
                                                                        arguments:nil];
    objNotificationContent.sound = [UNNotificationSound defaultSound];
    objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    UNTimeIntervalNotificationTrigger *trigger;
    /*
    switch ([reminderSelected integerValue]) {
        case 1: // 5   432000.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:432000.f repeats:YES];
            break;
        case 2: // 10   864000.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:864000.f repeats:YES];
            break;
        case 3: // 15   1296000.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1296000.f repeats:YES];
            break;
        case 4: // 30  2592000.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:2592000.f repeats:YES];
            break;
        default:
            break;
    }
    */
    switch ([reminderSelected integerValue]) {
        case 1: // 3   259200.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:259200.f repeats:YES];
            break;
        case 2: // 6   518400.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:518400.f repeats:YES];
            break;
        case 3: // 9   777600.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:777600.f repeats:YES];
            break;
        case 4: // 12  1036800.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1036800.f repeats:YES];
            break;
        default:
            break;
    }
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"ten" content:objNotificationContent trigger:trigger];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Local Notification succeeded");
        }
        else {
            NSLog(@"Local Notification failed");
        }
    }];
    [self addOneNotificationWithTitle:@"Contact Backup!" withSubtitle:@"Scan contacts and take backup!" withInterval:16 withIdent:@"Evening4"];
}
-(void)addOneNotificationWithTitle:(NSString*)title withSubtitle:(NSString*)subTitle withInterval:(NSInteger)hour withIdent:(NSString*)identifier{
//    [[NSDate date] dateByAddingTimeInterval:86400.f]
    NSDateComponents *date = [self getDateComponentshour:hour];
//     NSDateComponents *date = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    UNMutableNotificationContent *notifContent = [[UNMutableNotificationContent alloc] init];
    notifContent.title = [NSString localizedUserNotificationStringForKey:title arguments:nil];
    notifContent.body = [NSString localizedUserNotificationStringForKey:subTitle
                                                                        arguments:nil];
    notifContent.sound = [UNNotificationSound defaultSound];
    notifContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:date repeats:YES];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:notifContent trigger:trigger];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"New NotificationRequest succeeded");
        }else{
            NSLog(@"Error In New Notification");
        }
    }];
}
-(NSDateComponents*)getDateComponentshour:(NSInteger)hour{
//    NSDate *now = [NSDate date];
    NSDate *now = [[NSDate date] dateByAddingTimeInterval:86400.f];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:[NSTimeZone localTimeZone]];
    NSDateComponents* dateComponent = [[NSDateComponents alloc] init];
    dateComponent = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond|NSCalendarUnitTimeZone fromDate:now];
    dateComponent.hour = hour;
    return dateComponent;
}
@end
