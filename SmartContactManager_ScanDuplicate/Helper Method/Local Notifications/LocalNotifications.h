//
//  LocalNotifications.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 14/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalNotifications : NSObject
+ (id) sharedObject;
-(void)resetLocalNotification;

@end
