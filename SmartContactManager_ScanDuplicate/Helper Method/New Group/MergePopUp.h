//
//  MergePopUp.h
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 21/11/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol MergePopUpDelegate <NSObject>
@optional
-(void)btn_OkClick;
-(void)hideMergePopup;
@end

@interface MergePopUp : UIView
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UIButton *btn_Ok;
@property (strong, nonatomic) NSMutableArray *arr_display;
-(void)reloadCell:(NSMutableArray  *)arr;
@property (nonatomic, assign) NSInteger barItemIndex;

- (IBAction)btn_Ok:(id)sender;
@property(weak,nonatomic)id <MergePopUpDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
